﻿using System;
using System.Collections.Generic;
using System.Text;
//using Mogre;

namespace SampleGame
{
	class FSMPlayer
	{
		private static FSMPlayer mThis = null;
		private bool mPlaying = false;
		private Object mPlayerObject = null;

		public static FSMPlayer This
		{
			get
			{
				if (mThis == null)
				{
					mThis = new FSMPlayer();
				}

				return mThis;
			}
		}

		public bool Playing
		{
			get { return mPlaying; }
		}

		public FSMPlayer()
		{
		}

		public void Play()
		{
			mPlaying = true;

			// 플레이어 오브젝트가 존재하면 카메라 위치를 해당 오브젝트 시야 위치로 설정
			if (mPlayerObject != null)
			{
				OgreManager.This.AttachCameraToObject(mPlayerObject);
				OgreManager.This.SetVisibleObject(mPlayerObject, false);	// 플레이어 안 보이도록 함
			}

			// 모든 오브젝트를 순회하면서 FSM 상태를 초기화함
			var enumerator = SNFDocument.This.GetObjectEnumerator();
			while (enumerator.MoveNext())
			{
				Object obj = enumerator.Current.Value;	// 오브젝트
				if (obj.AssignedFSM == null || obj.Player)
				{
					continue;
				}

				obj.FSMStatus.Initialize(obj.AssignedFSM.InitialState);	// 오브젝트의 현재 상태를 초기화.
				obj.AssignedFSM.Initialize();
				OgreManager.This.PlayAnimation(obj, obj.FSMStatus.CurrentState.AnimationName);	// 현재 상태의 애니메이션 재생
			}
		}

		public void Stop()
		{
			mPlaying = false;

			if (mPlayerObject != null)
			{
				OgreManager.This.RestoreCamera();
				OgreManager.This.SetVisibleObject(mPlayerObject, true);	// 플레이어 보이도록 함
			}

			// 모든 오브젝트의 위치, 회전을 돌려놓고 사라졌으면 다시 보이게 하고 애니메이션도 중지
			var enumerator = SNFDocument.This.GetObjectEnumerator();
			while (enumerator.MoveNext())
			{
				Object obj = enumerator.Current.Value;	// 오브젝트
				OgreManager.This.RestoreObject(obj);
				OgreManager.This.StopAllAnimation(obj);
				OgreManager.This.StopAllObjectMove();
			}
		}

		public void Update()
		{
			if (!mPlaying)
			{
				return;
			}

			// 모든 오브젝트를 순회하면서 FSM 상태 업데이트 해줌
			var enumerator = SNFDocument.This.GetObjectEnumerator();
			while (enumerator.MoveNext())
			{
				Object obj = enumerator.Current.Value;	// 오브젝트
				// 오브젝트에 FSM이 할당되지 않았거나 플레이어 오브젝트면 FSM 업데이트 하지 않음
				if (obj.AssignedFSM == null || obj.Player)
				{
					continue;
				}

				FSMState currentState = obj.FSMStatus.CurrentState;	// 현재 오브젝트의 FSM 상태

				// 현재 상태에 TargetObject 속성이 null이 아니면 해당 오브젝트를 바라보도록 함
				if (currentState.LookAtObject != null)
				{
					OgreManager.This.LookAtObject(obj, currentState.LookAtObject);
				}

				// 이벤트 발생 조건 중 만족되는 것이 있는지 체크
				for (int i = 0; i < currentState.EventTriggerCount; ++i)
				{
					FSMEventTrigger trigger = currentState.GetEventTrigger(i);
					if(trigger.CheckCondition())
					{
						currentState.RunEvents(trigger);
					}
				}
			}
		}

		public void SetPlayerObject(Object obj)
		{
			mPlayerObject = obj;
		}

		public Object GetPlayerObject()
		{
			return mPlayerObject;
		}
	}
}

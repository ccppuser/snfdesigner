﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Mogre;
using RecastDetourDotNet;

namespace SampleGame
{
	public enum ObjectType
	{
		Agent,
		Terrain,
	}

	public class Object
	{
		private string mObjectName;
		private string mMeshName;
		private ObjectType mObjectType;
		private Vector3 mObjectPosition;
		private Quaternion mObjectRotation;
		private float mObjectScale;
		private bool mHasSight;
		private Camera mSightCamera = null;
		private Vector3 mSightPosition;
		private Quaternion mSightRotation;
		private float mSightRange;
		private float mSightWidth;
		private float mSightHeight;

		private FSM mAssignedFSM;
		private FSMStatus mFSMStatus = new FSMStatus();

		private bool mPlayer;

		private Pathfinder mPathFinder = null;

		[CategoryAttribute("오브젝트 속성")]
		public string ObjectName
		{
			get { return mObjectName; }
			set { mObjectName = value; }
		}
		[CategoryAttribute("오브젝트 속성"), ReadOnlyAttribute(true)]
		public string MeshName
		{
			get { return mMeshName; }
			set { mMeshName = value; }
		}
		[CategoryAttribute("오브젝트 속성"),
		DefaultValueAttribute(ObjectType.Agent)]
		public ObjectType ObjectType
		{
			get { return mObjectType; }
			set
			{
				mObjectType = value;

				// 오브젝트를 동작 주체로 설정
				if (mObjectType == ObjectType.Agent)
				{
					if (mPathFinder != null)
					{
						mPathFinder.Cleanup();
					}

					SceneNode objectNode = OgreManager.This.RootSceneNode.GetChild(ObjectName) as SceneNode;
					Entity entity = objectNode.GetAttachedObject(0) as Entity;
					entity.QueryFlags = QueryFlags.ENTITY;
					entity.CastShadows = true;	// 그림자 생기도록
				}
				// 오브젝트를 지형으로 설정
				else if (mObjectType == ObjectType.Terrain)
				{
					BuildNavigationMesh();

					SceneNode objectNode = OgreManager.This.RootSceneNode.GetChild(ObjectName) as SceneNode;
					Entity entity = objectNode.GetAttachedObject(0) as Entity;
					entity.QueryFlags = QueryFlags.TERRAIN;
					entity.CastShadows = false;	// 그림자 안 생기도록
				}
			}
		}
		[BrowsableAttribute(false)]
		public Vector3 ObjectPosition
		{
			get { return mObjectPosition; }
			set { mObjectPosition = value; }
		}
		[BrowsableAttribute(false)]
		public Quaternion ObjectRotation
		{
			get { return mObjectRotation; }
			set { mObjectRotation = value; }
		}
		[CategoryAttribute("오브젝트 속성")]
		public float ObjectScale
		{
			get { return mObjectScale; }
			set { mObjectScale = value; }
		}
		[CategoryAttribute("시야 속성")]
		public bool HasSight
		{
			get { return mHasSight; }
			set
			{
				mHasSight = value;

				if (mHasSight)
				{
					SceneNode objectNode = OgreManager.This.RootSceneNode.GetChild(mObjectName) as SceneNode;	// 오브젝트 노드
					// 시야 노드는 해당 오브젝트 노드의 자식으로 넣는다.
					SceneNode sightNode = objectNode.CreateChildSceneNode("SightNode_" + mObjectName);	// 시야 노드
					Entity sightEntity = OgreManager.This.SceneManager.CreateEntity("sight.mesh");// 시야 모델 생성
					sightEntity.QueryFlags = QueryFlags.EXCEPT;	// 마우스 피킹 안되도록 체크
					sightEntity.RenderQueueGroup = (byte)RenderQueueGroupID.RENDER_QUEUE_9;	// 제대로 투명하게
					sightNode.AttachObject(sightEntity);

					// 시야 모델 transform 설정
					sightNode.Position = mSightPosition;
					sightNode.Orientation = mSightRotation;
					sightNode.Scale(mSightWidth, mSightHeight, mSightRange);

					// 현재 오브젝트 선택 모드이면
					if (OgreManager.This.ObjectControlMode == ObjectControlMode.SELECT)
					{
						sightNode.SetVisible(false);	// 시야도 숨겨 놓음
					}

					// 시야 절두체 생성
					if (mSightCamera != null)
					{
						mSightCamera.Dispose();
					}

					mSightCamera = OgreManager.This.SceneManager.CreateCamera("Camera_" + mObjectName);
					mSightCamera.AspectRatio = mSightWidth / mSightHeight;
					mSightCamera.FarClipDistance = mSightRange;
					mSightCamera.NearClipDistance = 0.1f;
					//mSightCamera.Position = objectNode.Position;
					//mSightCamera.Orientation = objectNode.Orientation;
					mSightCamera.Position = sightNode._getDerivedPosition();
					mSightCamera.Orientation = sightNode._getDerivedOrientation();

					// 시야 모델에 자식 노드로 추가
					//SceneNode frustumNode = OgreManager.This.SceneManager.CreateSceneNode("Frustum_" + mObjectName);
					//frustumNode.AttachObject(mSightCamera);
					//sightNode.AddChild(frustumNode);
				}
				else
				{
					SceneNode objectNode = OgreManager.This.RootSceneNode.GetChild(mObjectName) as SceneNode;
					SceneNode sightNode = OgreManager.This.GetSightNode(objectNode);

					// 시야 절두체 제거
					if (mSightCamera != null)
					{
						mSightCamera.Dispose();
						mSightCamera = null;
					}

					// 시야 절두체 노드 제거
					if (sightNode != null)
					{
						Entity sightEntity = sightNode.GetAttachedObject(0) as Entity;
						OgreManager.This.SceneManager.DestroyEntity(sightEntity);
						OgreManager.This.SceneManager.DestroySceneNode(sightNode);
					}
				}
			}
		}
		[BrowsableAttribute(false)]
		public Vector3 SightPosition
		{
			get { return mSightPosition; }
			set { mSightPosition = value; }
		}
		[BrowsableAttribute(false)]
		public Quaternion SightRotation
		{
			get { return mSightRotation; }
			set { mSightRotation = value; }
		}
		[CategoryAttribute("시야 속성")]
		public float SightRange
		{
			get { return mSightRange; }
			set
			{
				mSightRange = value;

				SceneNode objectNode = OgreManager.This.RootSceneNode.GetChild(mObjectName) as SceneNode;
				SceneNode sightNode = OgreManager.This.GetSightNode(objectNode);

				Vector3 scaleFactor = sightNode.GetScale();
				scaleFactor.z = mSightRange;
				sightNode.SetScale(scaleFactor);
			}
		}
		[CategoryAttribute("시야 속성")]
		public float SightWidth
		{
			get { return mSightWidth; }
			set
			{
				mSightWidth = value;

				SceneNode objectNode = OgreManager.This.RootSceneNode.GetChild(mObjectName) as SceneNode;
				SceneNode sightNode = OgreManager.This.GetSightNode(objectNode);

				Vector3 scaleFactor = sightNode.GetScale();
				scaleFactor.x = mSightWidth;
				sightNode.SetScale(scaleFactor);
			}
		}
		[CategoryAttribute("시야 속성")]
		public float SightHeight
		{
			get { return mSightHeight; }
			set
			{
				mSightHeight = value;

				SceneNode objectNode = OgreManager.This.RootSceneNode.GetChild(mObjectName) as SceneNode;
				SceneNode sightNode = OgreManager.This.GetSightNode(objectNode);

				Vector3 scaleFactor = sightNode.GetScale();
				scaleFactor.y = mSightHeight;
				sightNode.SetScale(scaleFactor);
			}
		}

		[CategoryAttribute("FSM 속성")]
		public FSM AssignedFSM
		{
			get { return mAssignedFSM; }
			set
			{
				mAssignedFSM = value;
				if (mAssignedFSM.AssignedObject != this)
				{
					mAssignedFSM.AssignedObject = this;
				}
			}
		}
		[BrowsableAttribute(false)]
		public FSMStatus FSMStatus
		{
			get { return mFSMStatus; }
		}

		[CategoryAttribute("FSM 속성"),
		DefaultValueAttribute(false)]
		public bool Player
		{
			get { return mPlayer; }
			set
			{
				mPlayer = value;

				if (mPlayer)
				{
					if (FSMPlayer.This.GetPlayerObject() != null)
					{
						FSMPlayer.This.GetPlayerObject().Player = false;
					}

					FSMPlayer.This.SetPlayerObject(this);
				}
				else
				{
					if (FSMPlayer.This.GetPlayerObject() == this)
					{
						FSMPlayer.This.SetPlayerObject(null);
					}
				}
			}
		}

		public Object(string objectName, string meshName, Vector3 objPos)
		{
			mObjectName = objectName;
			mMeshName = meshName;
			Set(ObjectType.Agent, objPos, Quaternion.IDENTITY, 1, false, Vector3.ZERO, Quaternion.IDENTITY, 50, 30, 30, false);
		}

		public Object(string objectName, string meshName, ObjectType objType, Vector3 objPos, Quaternion objRot, float objScale, bool hasSight, Vector3 sightPos, Quaternion sightRot, float sightRange, float sightWidth, float sightHeight, bool player)
		{
			mObjectName = objectName;
			mMeshName = meshName;
			Set(objType, objPos, objRot, objScale, hasSight, sightPos, sightRot, sightRange, sightWidth, sightHeight, player);
		}

		public void Set(ObjectType objType, Vector3 objPos, Quaternion objRot, float objScale, bool hasSight, Vector3 sightPos, Quaternion sightRot, float sightRange, float sightWidth, float sightHeight, bool player)
		{
			mObjectPosition = objPos;
			mObjectRotation = objRot;
			mObjectScale = objScale;
			mSightPosition = sightPos;
			mSightRotation = sightRot;
			mSightRange = sightRange;
			mSightWidth = sightWidth;
			mSightHeight = sightHeight;

			ObjectType = objType;
			HasSight = hasSight;

			mPlayer = player;
		}

		~Object()
		{
			if (mPathFinder != null)
			{
				mPathFinder.Cleanup();
			}
			//Pathfinder.This.SetDebugRenderer(null);

			if (mHasSight)
			{
				if (OgreManager.This != null)
				{
					HasSight = false;
				}
			}
		}

		public bool IsInSight(Object obj)
		{
			if (mSightCamera == null)
			{
				return false;
			}

			SceneNode objectNode = OgreManager.This.RootSceneNode.GetChild(obj.ObjectName) as SceneNode;
			AxisAlignedBox aabb = objectNode._getWorldAABB();
			return mSightCamera.IsVisible(aabb);
		}

		private void BuildNavigationMesh()
		{
			SceneNode objectNode = OgreManager.This.RootSceneNode.GetChild(mObjectName) as SceneNode;
			SceneNode.ObjectIterator entityItr = objectNode.GetAttachedObjectIterator();
			// 해당 오브젝트를 이루는 모든 메쉬의 버텍스 및 인덱스를 얻는다.
			Vector3[] vertices;
			int vertexCount;
			int[] indices;
			int indexCount;
			List<float> totalVertices = new List<float>();
			List<int> totalIndices = new List<int>();
			int lastIndexCount = 0;
			foreach (Entity entity in entityItr)
			{
				MeshPtr mesh = entity.GetMesh();
				MMOC.CollisionTools.GetMeshInformation(mesh, out vertexCount, out vertices, out indexCount, out indices,
					objectNode._getDerivedPosition(), objectNode._getDerivedOrientation(), objectNode._getDerivedScale());

				// 서로 다른 메쉬를 한꺼번에 처리하는 과정에서
				// 인덱스가 겹치지 않도록 함
				for (int i = 0; i < indexCount; ++i)
				{
					indices[i] += lastIndexCount;
				}

				float[] floatVertices = new float[vertices.Length * 3];
				for (int i = 0; i < vertices.Length; ++i)
				{
					floatVertices[i*3+0] = vertices[i][0];
					floatVertices[i*3+1] = vertices[i][1];
					floatVertices[i*3+2] = vertices[i][2];
				}

				totalVertices.AddRange(floatVertices);
				totalIndices.AddRange(indices);

				lastIndexCount = indexCount;
			}

			List<string> messages;
			if (mPathFinder == null)
			{
				mPathFinder = new Pathfinder();
			}
			//if (!Pathfinder.This.CreateNavigationMesh(totalVertices.ToArray(), totalIndices.ToArray(),
			//    10, 6, 50, 40, 12, 20, 30, 3, 8, 20, 6, 6, 1, true, out messages))
			if (!mPathFinder.CreateNavigationMesh(totalVertices.ToArray(), totalIndices.ToArray(),
				25, 15, 50, 100, 50, 40, 30, 3, 8, 20, 6, 6, 1, true, out messages))
			{
				throw new Exception("Failed: BuildNavigationMesh");
			}

			// TEST: 네비게이션 메쉬 가시 테스트
			//DebugRendererMogre debugRenderer = new DebugRendererMogre();
			//Pathfinder.This.SetDebugRenderer(debugRenderer);
			//Pathfinder.This.DebugDrawNavMesh();
		}

		public void UpdateSightCamera()
		{
			SceneNode objectNode = OgreManager.This.RootSceneNode.GetChild(mObjectName) as SceneNode;	// 오브젝트 노드
			SceneNode sightNode = OgreManager.This.GetSightNode(objectNode);

			mSightCamera.AspectRatio = mSightWidth / mSightHeight;
			mSightCamera.FarClipDistance = mSightRange;
			//mSightCamera.Position = objectNode.Position;
			//mSightCamera.Orientation = objectNode.Orientation;
			mSightCamera.Position = sightNode._getDerivedPosition();
			mSightCamera.Orientation = sightNode._getDerivedOrientation();
			//Quaternion turnBack = new Quaternion(new Radian(Mogre.Math.PI), Vector3.UNIT_Y);
			//mSightCamera.Orientation *= turnBack;
		}

		public Vector3[] FindPathOnTerrain(Vector3 startPos, Vector3 endPos, out List<string> messages)
		{
			if (mPathFinder == null)
			{
				messages = null;
				return null;
			}

			float[] spos = new float[] { startPos.x, startPos.y, startPos.z };
			float[] epos = new float[] { endPos.x, endPos.y, endPos.z };
			float[] floatPath = mPathFinder.FindPath(spos, epos, out messages);
			if (floatPath == null || floatPath.Length == 0)
			{
				return null;
			}

			Vector3[] path = new Vector3[floatPath.Length / 3];
			for (int i = 0; i < path.Length; ++i)
			{
				path[i] = new Vector3(floatPath[i*3+0], floatPath[i*3+1], floatPath[i*3+2]);
			}

			return path;
		}

		public bool IsOnNavMesh(Vector3 position)
		{
			float[] pos = new float[3] { position[0], position[1], position[2] };
			return mPathFinder.IsOnNavMesh(pos);
		}
	}
}

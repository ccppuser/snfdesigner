﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.ComponentModel;

namespace SampleGame
{
	public enum FSMEventType
	{
		ChangeState,	// 상태 변경
		MoveObject,	// 이동
		RemoveObject,	// 삭제
	}

	public static class FSMEventTypeDescription
	{
		public static string Description(FSMEventType eventType)
		{
			switch (eventType)
			{
				case FSMEventType.ChangeState: return "상태 변경";
				case FSMEventType.MoveObject: return "이동";
				case FSMEventType.RemoveObject: return "삭제";
				default: return "";
			}
		}
	}

	public abstract class FSMEvent
	{
		protected FSM mFSM;
		protected FSMEventTrigger mEventTrigger = null;
		protected FSMEventType mEventType;
		protected bool mRan = false;

		[BrowsableAttribute(false)]
		public FSMEventTrigger EventTrigger
		{
			get { return mEventTrigger; }
		}

		[CategoryAttribute("이벤트 속성")]
		public FSMEventType EventType
		{
			get { return mEventType; }
		}

		[BrowsableAttribute(false)]
		public Object AgentObject
		{
			get { return mFSM.AssignedObject; }
		}

		public FSMEvent(FSMEventTrigger eventTrigger, FSM fsm)
		{
			mFSM = fsm;
			mEventTrigger = eventTrigger;
		}

		public void Initialize()
		{
			mRan = false;
		}

		public void Run()
		{
			if (!mRan)
			{
				mRan = true;
				OnRun();
			}
		}

		protected abstract void OnRun();
	}

	// 대상의 상태를 변경하는 이벤트
	// 속성의 콤보박스를 누르면 대상이 갖고 있는 상태 목록이 나오고
	// 그 중에서 원하는 상태를 선택할 수 있다.
	public class FSMChangeStateEvent : FSMEvent
	{
		private FSMState mTargetState = null;

		public static event EventHandler TargetStateChanged;

		[CategoryAttribute("이벤트 속성")]
		public FSMState TargetState
		{
			get { return mTargetState; }
			set
			{
				mTargetState = value;
			}
		}

		public FSMChangeStateEvent(FSMEventTrigger eventTrigger, FSM fsm)
			: base(eventTrigger, fsm)
		{
			mEventType = FSMEventType.ChangeState;
		}

		protected override void OnRun()
		{
			// state바꾸기 전에 이전 state의 애니메이션 종료
			AgentObject.FSMStatus.CurrentState.StopAnimation();
			// 대상 오브젝트의 state를 바꿈
			AgentObject.FSMStatus.CurrentState = mTargetState;
			if (mTargetState != null)
			{
				AgentObject.FSMStatus.CurrentState.Initialize();
				OgreManager.This.PlayAnimation(AgentObject, AgentObject.FSMStatus.CurrentState.AnimationName);	// 전환된 상태의 애니메이션 재생
			}
		}
	}

	// 대상을 이동시키는 이벤트
	// 속성을 더블 클릭하면 뷰포트에서 웨이포인트를 클릭하는 모드로 변경된다.
	public class FSMMoveObjectEvent : FSMEvent
	{
		private Mogre.Vector3 mTargetPoint = Mogre.Vector3.ZERO;
		private float mSpeed = 1;

		[CategoryAttribute("이벤트 속성")]
		public Mogre.Vector3 TargetPoint
		{
			get { return mTargetPoint; }
			set { mTargetPoint = value; }
		}

		[CategoryAttribute("이벤트 속성")]
		public float Speed
		{
			get { return mSpeed; }
			set { mSpeed = value; }
		}

		public FSMMoveObjectEvent(FSMEventTrigger eventTrigger, FSM fsm)
			: base(eventTrigger, fsm)
		{
			mEventType = FSMEventType.MoveObject;
		}

		protected override void OnRun()
		{
			OgreManager.This.MoveObject(AgentObject, mTargetPoint, mSpeed);
		}
	}

	// 대상을 삭제하는 이벤트
	public class FSMRemoveObjectEvent : FSMEvent
	{
		public FSMRemoveObjectEvent(FSMEventTrigger eventTrigger, FSM fsm)
			: base(eventTrigger, fsm)
		{
			mEventType = FSMEventType.RemoveObject;
		}

		protected override void OnRun()
		{
			OgreManager.This.ShowObject(AgentObject, false);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SampleGame
{
	static class Program
	{
		/// <summary>
		/// 해당 응용 프로그램의 주 진입점입니다.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			try
			{
				MainForm mainForm = new MainForm();
				mainForm.Show();
				while (!mainForm.Created) { }

				if (!mainForm.Initialize())
				{
					return;
				}

				mainForm.Run();
			}
			catch (Exception e)
			{
				Mogre.OgreException oe = Mogre.OgreException.LastException;
				if (oe != null)
				{
					MessageBox.Show(oe.FullDescription, "예외 발생", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else
				{
					MessageBox.Show(e.InnerException + ": " + e.Message + "\n" + e.StackTrace, "예외 발생", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}
	}
}

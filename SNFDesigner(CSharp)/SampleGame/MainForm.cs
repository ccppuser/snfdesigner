﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SampleGame
{
	public partial class MainForm : Form
	{
		private static MainForm mThis = null;
		private Dictionary<Keys, bool> mKeyStates = new Dictionary<Keys, bool>();
		private Point mCenterPosition = Point.Empty;

		public static MainForm This { get { return mThis; } }
		public Point CenterPosition { get { return mCenterPosition; } }

		public MainForm()
		{
			if (mThis == null)
			{
				mThis = this;
			}

			InitializeComponent();
		}

		public bool Initialize()
		{
			new OgreManager(this.Handle, this.Width, this.Height);

			FileStream fileStream = new FileStream("ProjectPath.txt", FileMode.Open);
			StreamReader reader = new StreamReader(fileStream);
			string projectPath = reader.ReadLine();
			reader.Close();
			fileStream.Close();

			OgreManager.This.AddResourcePath(projectPath);
			if (!OgreManager.This.New())
			{
				MessageBox.Show("오우거 환경 설정 실패");
				return false;
			}

			if (!SNFDocument.This.Open(projectPath))
			{
				MessageBox.Show("프로젝트 정보가 잘못되었습니다.");
				return false;
			}

			OgreManager.This.ShowSightRegion(false);

			return true;
		}

		public void Run()
		{
			while (Render())
			{
				Application.DoEvents();
			}
		}

		private bool Render()
		{
			if (OgreManager.This != null)
			{
				OgreManager.This.Render();
				return true;
			}
			else
			{
				return false;
			}
		}

		private bool mShowSightRegion = false;

		private void MainForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				if (FSMPlayer.This.Playing)
				{
					FSMPlayer.This.Stop();

					Cursor.Show();
				}
				else
				{
					FSMPlayer.This.Play();

					if (FSMPlayer.This.GetPlayerObject() != null)
					{
						Cursor.Hide();
						Point viewportCenter = new Point(Size.Width/2, Size.Height/2);
						Cursor.Position = PointToScreen(viewportCenter);
						mCenterPosition = Cursor.Position;
					}
				}
			}
			else if (e.KeyCode == Keys.Space)
			{
				mShowSightRegion = !mShowSightRegion;
				OgreManager.This.ShowSightRegion(mShowSightRegion);
			}

			if (!mKeyStates.ContainsKey(e.KeyCode))
			{
				mKeyStates.Add(e.KeyCode, true);
			}
			else
			{
				mKeyStates[e.KeyCode] = true;
			}
		}

		private void MainForm_KeyUp(object sender, KeyEventArgs e)
		{
			if (!mKeyStates.ContainsKey(e.KeyCode))
			{
				mKeyStates.Add(e.KeyCode, false);
			}
			else
			{
				mKeyStates[e.KeyCode] = false;
			}
		}

		public bool IsKeyDown(Keys keyCode)
		{
			if (!mKeyStates.ContainsKey(keyCode))
			{
				mKeyStates.Add(keyCode, false);
			}

			return mKeyStates[keyCode];
		}

		private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			OgreManager.This.Dispose();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Mogre;

namespace SNFDesigner
{
	public class Region
	{
		private SceneNode mRegionNode;
		private Sphere mBoundingSphere;

		public string Name
		{
			get { return mRegionNode.Name; }
		}

		public float Diameter
		{
			get { return mRegionNode.GetScale().x; }
		}

		public Vector3 Position
		{
			get { return mRegionNode.Position; }
		}

		public Quaternion Orientation
		{
			get { return mRegionNode.Orientation; }
		}

		public Region(SceneNode regionNode)
		{
			mRegionNode = regionNode;
			mBoundingSphere = new Sphere(mRegionNode.Position, Diameter);
		}

		public bool Intersects(Object obj)
		{
			SceneNode objectNode = OgreManager.This.RootSceneNode.GetChild(obj.ObjectName) as SceneNode;
			AxisAlignedBox aabb = objectNode._getWorldAABB();
			if (mBoundingSphere.Intersects(aabb))
			{
				return true;
			}

			return false;
		}

		public void SetRegionVisible(bool visible)
		{
			mRegionNode.SetVisible(visible);
		}
	}

	class RegionManager
	{
		private static RegionManager mThis = null;
		private bool mDeploying = false;
		private SceneNode mTempRegionNode = null;
		private Vector3 mStartScalePos;

		public static RegionManager This
		{
			get
			{
				if (mThis == null)
				{
					mThis = new RegionManager();
				}

				return mThis;
			}
		}

		public bool Deploying
		{
			get { return mDeploying; }
			set
			{
				mDeploying = value;

				if (mDeploying)
				{
					MainForm.This.Cursor = Cursors.Cross;

					OgreManager.This.RootSceneNode.AddChild(mTempRegionNode);
				}
				else
				{
					MainForm.This.Cursor = Cursors.Arrow;

					OgreManager.This.RootSceneNode.RemoveChild(mTempRegionNode);
				}
			}
		}

		public RegionManager()
		{
			// 영역을 나타내는 메쉬 생성
			Entity region = OgreManager.This.SceneManager.CreateEntity("region.mesh");
			region.QueryFlags = QueryFlags.REGION;
			region.RenderQueueGroup = (byte)RenderQueueGroupID.RENDER_QUEUE_9;	// 제대로 투명하게
			mTempRegionNode = OgreManager.This.RootSceneNode.CreateChildSceneNode();
			mTempRegionNode.AttachObject(region);

			// 일단 안 보이게 함
			mTempRegionNode.Parent.RemoveChild(mTempRegionNode);
		}

		public void FollowMouse(float x, float y)
		{
			if (mDeploying)
			{
				Vector3 groundPos = OgreManager.This.CalculateGroundPosition(x, y);
				mTempRegionNode.Position = groundPos;
			}
		}

		public void StartScale(float x, float y)
		{
			mStartScalePos = OgreManager.This.CalculateGroundPosition(x, y);
		}

		public void Scale(float x, float y)
		{
			Vector3 currentScalePos = OgreManager.This.CalculateGroundPosition(x, y);
			float distance = (currentScalePos - mStartScalePos).Length;
			mTempRegionNode.SetScale(distance, distance, distance);
		}

		public void EndScale()
		{
			// 영역 이름 설정
			SetRegionNameDlg dlg = new SetRegionNameDlg();
			if (dlg.ShowDialog() == DialogResult.Cancel)
			{
				CancelScale();
				return;
			}

			if (dlg.RegionName == "(Auto)")
			{
				// 새 영역 추가
				SNFDocument.This.AddRegion(new Region(mTempRegionNode));

				// 다음 영역 생성을 위해 준비
				Entity newRegion = OgreManager.This.SceneManager.CreateEntity("region.mesh");
				newRegion.QueryFlags = QueryFlags.REGION;
				newRegion.RenderQueueGroup = (byte)RenderQueueGroupID.RENDER_QUEUE_9;	// 제대로 투명하게
				mTempRegionNode = OgreManager.This.RootSceneNode.CreateChildSceneNode();
				mTempRegionNode.AttachObject(newRegion);

				// 일단 숨김
				mTempRegionNode.Parent.RemoveChild(mTempRegionNode);
			}
			else
			{
				// 새 노드 만들어 거기에 영역 메쉬 집어넣음
				Entity newRegion = OgreManager.This.SceneManager.CreateEntity("region.mesh");
				newRegion.QueryFlags = QueryFlags.REGION;
				newRegion.RenderQueueGroup = (byte)RenderQueueGroupID.RENDER_QUEUE_9;	// 제대로 투명하게
				SceneNode newRegionNode = OgreManager.This.RootSceneNode.CreateChildSceneNode(dlg.RegionName);
				newRegionNode.AttachObject(newRegion);
				newRegionNode.Position = mTempRegionNode.Position;
				newRegionNode.SetScale(mTempRegionNode.GetScale());

				SNFDocument.This.AddRegion(new Region(newRegionNode));

				// 다음 영역 생성을 위해 준비
				// 숨김
				mTempRegionNode.Parent.RemoveChild(mTempRegionNode);
			}
		}

		public void CancelScale()
		{
			Deploying = false;
		}
	}
}

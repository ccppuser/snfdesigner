﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace SNFDesigner
{
	public partial class FSMDesignControl : DockContent
	{
		private static FSMDesignControl mThis = null;
		private string mDefaultControlName = "";
		private CanvasControl mCanvasControl = null;
        private Graphics mGraphics = null;
		private Pen mTransitionPen = new Pen(Color.Black, 3);
		private Pen mThinPen = new Pen(Color.Black, 3);
		private Pen mThickPen = new Pen(Color.Red, 6);
		private const int mMinCircleRadius = 50;
		private const int mMaxCircleRadius = 100;
		private Font mFont = new Font("맑은 고딕", 9);
		private FSM mSelectedFSM = null;
		private FSMState mSelectedState = null;
        private bool mLeftMouseDown = false;
		private Point mLastMousePosition = Point.Empty;

		public static FSMDesignControl This
		{
			get { return mThis; }
		}

		public FSM SelectedFSM
		{
			get { return mSelectedFSM; }
		}

		public FSMDesignControl()
		{
			if (mThis == null)
			{
				mThis = this;
			}

			InitializeComponent();

			//mTransitionPen.EndCap = LineCap.ArrowAnchor;
			GraphicsPath arrowPath = new GraphicsPath();
			arrowPath.AddLine(0, 0, -2, -5);
			arrowPath.AddLine(-2, -5, 0, -4);
			arrowPath.AddLine(0, -4, 2, -5);
			arrowPath.AddLine(2, -5, 0, 0);
			arrowPath.FillMode = FillMode.Winding;
			mTransitionPen.CustomEndCap = new CustomLineCap(null, arrowPath);

			mCanvasControl = new CanvasControl();
			this.Controls.Add(mCanvasControl);
			mCanvasControl.Show();
        }

        private void FSMDesignControl_Load(object sender, EventArgs e)
        {
			this.mDefaultControlName = this.Text;
			this.contextMenuStrip.Enabled = false;

			mCanvasControl.Size = new Size(2000, 2000);
			mCanvasControl.Paint += new PaintEventHandler(designPanel_Paint);
			mCanvasControl.MouseDown += new MouseEventHandler(designPanel_MouseDown);
			mCanvasControl.MouseMove += new MouseEventHandler(designPanel_MouseMove);
			mCanvasControl.MouseUp += new MouseEventHandler(designPanel_MouseUp);

			FSMState.StateNameChanged += new StateNameChangedEventHandler(FSMState_StateNameChanged);
			FSMChangeStateEvent.TargetStateChanged += new EventHandler(FSMChangeStateEvent_TargetStateChanged);

			mGraphics = mCanvasControl.CreateGraphics();
        }

		private void FSMState_StateNameChanged(object sender, StateNameChangedEventArgs e)
		{
			FSMState state = sender as FSMState;

			// 상태의 이름의 길이에 따라 원의 반지름을 계산
            SizeF textSize = mGraphics.MeasureString(state.Name, mFont, mMaxCircleRadius * 2);
			float circleRadius = 0;
			if (textSize.Width < mMinCircleRadius)
			{
				circleRadius = mMinCircleRadius;
			}
			else
			{
				circleRadius = textSize.Width / 2;
			}

			state.CircleRadius = circleRadius;
			state.TextSize = textSize;

			mCanvasControl.Invalidate();
		}

		private void FSMChangeStateEvent_TargetStateChanged(object sender, EventArgs e)
		{
			FSMDesignControl.This.InvalidateCanvasControl();
		}

		public void InvalidateCanvasControl()
		{
			mCanvasControl.Invalidate();
		}

		public void SelectFSM(string fsmName)
		{
			FSM fsm = SNFDocument.This.GetFSM(fsmName);
			mSelectedFSM = fsm;	// 해당 FSM 선택
			mSelectedState = null;

			// FSM 선택함에 따라 폼 형태 변경
			this.Text = mDefaultControlName + " - " + fsmName;	// 폼 이름에 FSM 이름 붙임
			this.contextMenuStrip.Enabled = true;	// 콘텍스트 메뉴 활성화
			mCanvasControl.BackColor = Color.White;

			// FSM이 연결된 오브젝트를 선택한 것으로 표시
			if (fsm.AssignedObject != null)
			{
				OgreManager.This.SelectObject(fsm.AssignedObject);
			}
		}

		public void DeselectFSM(string fsmName)
		{
			mSelectedFSM = null;	// 선택한 FSM이 없도록
			mSelectedState = null;

			this.Text = mDefaultControlName;	// 폼 이름 초기화
			this.contextMenuStrip.Enabled = false;	// 콘텍스트 메뉴 비활성화
		}

		private void AddNewStateToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (mSelectedFSM == null)
			{
				return;
			}

			// 마우스 위치에 새 상태 원 추가
			Point mousePosition = Control.MousePosition;
			mousePosition = mCanvasControl.PointToClient(mousePosition);

			FSMState newState = new FSMState(mSelectedFSM);
			newState.CenterPosition = mousePosition;

			mSelectedFSM.AddState(newState);
			FSMState_StateNameChanged(newState, null);

			// 이 state가 최초의 state이면 FSM에 초기 state로 등록
			if (mSelectedFSM.StateCount == 1)
			{
				mSelectedFSM.InitialState = newState;
				// FSMStorageControl의 FSM속성창 업데이트
				FSMStorageControl.This.UpdateFSMProperty();
			}

			mCanvasControl.Invalidate();
		}

		private void designPanel_Paint(object sender, PaintEventArgs e)
		{
			if (mSelectedFSM == null)
			{
				return;
			}

            // 이 함수에서는 mGraphics변수가 아닌 e.Graphics변수를 사용해야 한다.

			// 상태 전환 선분 그리기
			for (int i = 0; i < mSelectedFSM.TransitionCount; ++i)
			{
				FSMStateTransition transition = mSelectedFSM.GetTransition(i);

				// 선분 길이 계산
				Mogre.Vector2 fromStatePoint = new Mogre.Vector2(transition.FromState.CenterPosition.X, transition.FromState.CenterPosition.Y);
				Mogre.Vector2 toStatePoint = new Mogre.Vector2(transition.ToState.CenterPosition.X, transition.ToState.CenterPosition.Y);
				Mogre.Vector2 fromToVec = toStatePoint - fromStatePoint;
				float originalLength = fromToVec.Length;
				fromToVec.Normalise();
				fromToVec *= originalLength - transition.ToState.CircleRadius;
				toStatePoint = fromStatePoint + fromToVec;
				// 선분 그리기
				//e.Graphics.DrawLine(mTransitionPen, transition.FromState.CenterPosition, transition.ToState.CenterPosition);
				e.Graphics.DrawLine(mTransitionPen, fromStatePoint.x, fromStatePoint.y, toStatePoint.x, toStatePoint.y);
			}

			// 상태 그리기
			for(int i = 0; i < mSelectedFSM.StateCount; ++i)
			{
				FSMState state = mSelectedFSM.GetState(i);
				RectangleF circleBound = new RectangleF(state.CenterPosition.X - state.CircleRadius, state.CenterPosition.Y - state.CircleRadius, state.CircleRadius * 2, state.CircleRadius * 2);

				// 상태를 나타내는 원 그리기
				e.Graphics.DrawEllipse(state == mSelectedState ? mThickPen : mThinPen, circleBound);
				e.Graphics.FillEllipse(Brushes.White, circleBound);
				// 상태 이름 그리기
				e.Graphics.DrawString(state.Name, mFont, Brushes.Black, new RectangleF(state.CenterPosition.X - state.TextSize.Width/2, state.CenterPosition.Y - state.TextSize.Height/2, state.TextSize.Width, state.TextSize.Height));
			}
		}

		private void designPanel_MouseDown(object sender, MouseEventArgs e)
		{
			if (mSelectedFSM == null)
			{
				return;
			}

			Point mousePosition = Control.MousePosition;
			mousePosition = mCanvasControl.PointToClient(mousePosition);

			if (e.Button == MouseButtons.Left)
			{
                mLeftMouseDown = true;

				// 어느 상태를 마우스로 눌렀는지 알아냄
				float minDistance = float.MaxValue;
				float distance = 0;
				FSMState nearestState = null;
				for (int i = 0; i < mSelectedFSM.StateCount; ++i)
				{
					FSMState state = mSelectedFSM.GetState(i);

					distance = Helper.GetDistance(mousePosition, state.CenterPosition);
					if (distance < minDistance)
					{
						minDistance = distance;
						nearestState = state;
					}
				}

				// 아무것도 선택 안 했으면 무시
                // 그리고 선택되었던 상태를 선택 취소
                if (nearestState == null || minDistance > nearestState.CircleRadius)
                {
                    mSelectedState = null;
					FSMStatePropertyControl.This.SetState(null);	// 상태 속성창에서 아무 상태도 선택 안하도록 비움
                }
                else
                {
                    // 이 상태를 마우스로 선택했다!
                    mSelectedState = nearestState;
					FSMStatePropertyControl.This.SetState(mSelectedState);	// 해당 상태의 속성을 상태 속성창에 띄움
					FSMStatePropertyControl.This.Pane.ActiveContent = FSMStatePropertyControl.This;
                }

				mCanvasControl.Invalidate();
			}
        }

        private void designPanel_MouseMove(object sender, MouseEventArgs e)
        {
			if (mLastMousePosition == Point.Empty)
			{
				mLastMousePosition = e.Location;
			}

            if(mLeftMouseDown && mSelectedFSM != null && mSelectedState != null && mLastMousePosition != e.Location)
            {
                mSelectedState.CenterPosition = e.Location;

				mCanvasControl.Invalidate();
            }

			mLastMousePosition = e.Location;
        }

        private void designPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if(mLeftMouseDown)
            {
                mLeftMouseDown = false;
            }
        }
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Mogre;

namespace SNFDesigner
{
	public partial class FSMStatePropertyControl : DockContent
	{
		private static FSMStatePropertyControl mThis = null;
		private TreeNode mEventTriggerNodeOnMouse = null;
		private TreeNode mLastEventTriggerNodeOnMouse = null;
		private TreeNode mLastEventNodeOnMouse = null;
		private string mBlankTreeNodeText = "          ";
		private GridItem mSelectedGridItem = null;
		private bool mObjectSelectMode = false;
		private bool mPointSelectMode = false;
		private bool mRegionSelectMode = false;

		public static FSMStatePropertyControl This
		{
			get { return mThis; }
		}

		public bool ObjectSelectMode
		{
			get { return mObjectSelectMode; }
		}

		public bool PointSelectMode
		{
			get { return mPointSelectMode; }
		}

		public bool RegionSelectMode
		{
			get { return mRegionSelectMode; }
		}

		public FSMStatePropertyControl()
		{
			if (mThis == null)
			{
				mThis = this;
			}

			InitializeComponent();
		}

		private void FSMPropertyControl_Load(object sender, EventArgs e)
		{
			propState.SelectedObject = null;
			propState.LineColor = Color.Gainsboro;
			propState.ToolbarVisible = false;
			propState.GridDoubleClick += new EventHandler(propState_GridDoubleClick);

			cmbCreateEventTrigger.Items.Add(FSMEventTriggerTypeDescription.Description(FSMEventTriggerType.SightEnter));
			cmbCreateEventTrigger.Items.Add(FSMEventTriggerTypeDescription.Description(FSMEventTriggerType.SightExit));
			cmbCreateEventTrigger.Items.Add(FSMEventTriggerTypeDescription.Description(FSMEventTriggerType.ZoneEnter));
			cmbCreateEventTrigger.Items.Add(FSMEventTriggerTypeDescription.Description(FSMEventTriggerType.ZoneExit));
			cmbCreateEventTrigger.Items.Add(FSMEventTriggerTypeDescription.Description(FSMEventTriggerType.ElapsedTime));
			cmbCreateEventTrigger.Items.Add(FSMEventTriggerTypeDescription.Description(FSMEventTriggerType.AnimationEnded));
			cmbCreateEventTrigger.Items.Add(FSMEventTriggerTypeDescription.Description(FSMEventTriggerType.AnimationRepeated));

			cmbCreateEvent.Items.Add(FSMEventTypeDescription.Description(FSMEventType.ChangeState));
			cmbCreateEvent.Items.Add(FSMEventTypeDescription.Description(FSMEventType.MoveObject));
			cmbCreateEvent.Items.Add(FSMEventTypeDescription.Description(FSMEventType.RemoveObject));

			tvwEvent.Controls.Add(cmbCreateEventTrigger);
			tvwEvent.Controls.Add(cmbCreateEvent);

			propEvent.SelectedObject = null;
			propEvent.LineColor = Color.Gainsboro;
			propEvent.ToolbarVisible = false;
			propEvent.GridDoubleClick += new EventHandler(propEvent_GridDoubleClick);

			FSMChangeStateEvent.TargetStateChanged += new EventHandler(FSMChangeStateEvent_TargetStateChanged);
		}

		private void FSMChangeStateEvent_TargetStateChanged(object sender, EventArgs e)
		{
			FSMChangeStateEvent changeStateEvent = sender as FSMChangeStateEvent;

			if (FSMDesignControl.This.SelectedFSM != null)
			{
				FSM currentFSM = SNFDocument.This.GetFSM(FSMDesignControl.This.SelectedFSM.Name);
				FSMState fromState = propState.SelectedObject as FSMState;
				FSMState toState = changeStateEvent.TargetState;
				currentFSM.AddTransition(fromState, toState);
			}
		}

		public void SetState(FSMState state)
		{
			// null이 들어왔으면 상태 선택 취소 및 이벤트 목록 표시 취소
			if (state != propState.SelectedObject)
			{
				cmbCreateEventTrigger.Visible = false;
				cmbCreateEvent.Visible = false;

				propState.SelectedObject = null;
				tvwEvent.Nodes.Clear();
				propEvent.SelectedObject = null;

				propState.Refresh();
				tvwEvent.Refresh();
				propEvent.Refresh();

				if (state != null)
				{
					cmbCreateEventTrigger.Visible = true;
					cmbCreateEvent.Visible = true;

					propState.SelectedObject = state;

					// 트리 뷰에 상태의 모든 이벤트 발생 조건 및 이벤트 목록을 삽입한다.
					for (int i = 0; i < state.EventTriggerCount; ++i)
					{
						FSMEventTrigger trigger = state.GetEventTrigger(i);
						TreeNode triggerNode = tvwEvent.Nodes.Add(FSMEventTriggerTypeDescription.Description(trigger.TriggerType));
						triggerNode.Tag = trigger;

						for (int j = 0; j < state.GetEventCount(trigger); ++j)
						{
							FSMEvent evt = state.GetEvent(trigger, j);
							TreeNode eventNode = triggerNode.Nodes.Add(FSMEventTypeDescription.Description(evt.EventType));
							eventNode.Tag = evt;
						}

						triggerNode.ExpandAll();
						triggerNode.Nodes.Add(mBlankTreeNodeText);
					}

					mLastEventTriggerNodeOnMouse = tvwEvent.Nodes.Add(mBlankTreeNodeText);
					// 마지막 트리거 노드의 위치에 트리거 추가 버튼을 놓는다.
					cmbCreateEventTrigger.Location = mLastEventTriggerNodeOnMouse.Bounds.Location;

					propEvent.SelectedObject = null;

					propState.Refresh();
					tvwEvent.Refresh();
					propEvent.Refresh();

					this.Pane.ActiveContent = this;
				}
			}
		}

		public void SetPickedObject(Object obj)
		{
			if (mSelectedGridItem != null)
			{
				if (propState.SelectedObject is FSMState && mSelectedGridItem.Label == "LookAtObject")
				{
					FSMState state = propState.SelectedObject as FSMState;
					state.LookAtObject = obj;

					propState.Refresh();
				}
				else if (propEvent.SelectedObject is FSMEventTrigger)
				{
					if (mSelectedGridItem.Label == "TargetObject")
					{
						if (propEvent.SelectedObject.GetType() == typeof(FSMSightEnterTrigger))
						{
							FSMSightEnterTrigger trigger = propEvent.SelectedObject as FSMSightEnterTrigger;
							trigger.TargetObject = obj;
						}
						else if (propEvent.SelectedObject.GetType() == typeof(FSMSightExitTrigger))
						{
							FSMSightExitTrigger trigger = propEvent.SelectedObject as FSMSightExitTrigger;
							trigger.TargetObject = obj;
						}
						else if (propEvent.SelectedObject.GetType() == typeof(FSMZoneEnterTrigger))
						{
							FSMZoneEnterTrigger trigger = propEvent.SelectedObject as FSMZoneEnterTrigger;
							trigger.TargetObject = obj;
						}
						else if (propEvent.SelectedObject.GetType() == typeof(FSMZoneExitTrigger))
						{
							FSMZoneExitTrigger trigger = propEvent.SelectedObject as FSMZoneExitTrigger;
							trigger.TargetObject = obj;
						}
					}

					propEvent.Refresh();
				}

				mSelectedGridItem = null;
			}
		}

		public void SetPickedGroundPoint(Vector3 groundPoint)
		{
			if (mSelectedGridItem != null)
			{
				if (propEvent.SelectedObject.GetType() == typeof(FSMMoveObjectEvent))
				{
					if (mSelectedGridItem.Label == "TargetPoint")
					{
						FSMMoveObjectEvent evt = propEvent.SelectedObject as FSMMoveObjectEvent;
						evt.TargetPoint = groundPoint;
					}
				}

				mSelectedGridItem = null;

				propEvent.Refresh();
			}
		}

		public void SetPickedRegion(Region region)
		{
			if (mSelectedGridItem != null)
			{
				if (propEvent.SelectedObject is FSMZoneEnterTrigger)
				{
					FSMZoneEnterTrigger trigger = propEvent.SelectedObject as FSMZoneEnterTrigger;

					if (mSelectedGridItem.Label == "TargetRegion")
					{
						trigger.TargetRegion = region;
					}
				}
				else if (propEvent.SelectedObject is FSMZoneExitTrigger)
				{
					FSMZoneExitTrigger trigger = propEvent.SelectedObject as FSMZoneExitTrigger;

					if (mSelectedGridItem.Label == "TargetRegion")
					{
						trigger.TargetRegion = region;
					}
				}

				mSelectedGridItem = null;

				propEvent.Refresh();
			}
		}

		public void SetObjectSelectMode(bool start)
		{
			mObjectSelectMode = start;

			if (mObjectSelectMode)
			{
				MainForm.This.Cursor = Cursors.Cross;

				MainForm.This.EnableAllControls(false);
			}
			else
			{
				MainForm.This.Cursor = Cursors.Arrow;

				MainForm.This.EnableAllControls(true);
			}
		}

		public void SetPointSelectMode(bool start)
		{
			mPointSelectMode = start;

			if (mPointSelectMode)
			{
				MainForm.This.Cursor = Cursors.Cross;

				MainForm.This.EnableAllControls(false);

				TargetPointManager.This.MovingTargetPoint = true;
			}
			else
			{
				MainForm.This.Cursor = Cursors.Arrow;

				MainForm.This.EnableAllControls(true);

				TargetPointManager.This.MovingTargetPoint = false;
			}
		}

		public void SetRegionSelectMode(bool start)
		{
			mRegionSelectMode = start;

			if (mRegionSelectMode)
			{
				MainForm.This.Cursor = Cursors.Cross;

				MainForm.This.EnableAllControls(false);
			}
			else
			{
				MainForm.This.Cursor = Cursors.Arrow;

				MainForm.This.EnableAllControls(true);
			}
		}

		private void propState_SelectedGridItemChanged(object sender, SelectedGridItemChangedEventArgs e)
		{
			//if (e.NewSelection.Label != "LookAtObject")
			//{
			//    SetObjectSelectMode(false);
			//}
		}

		private void propState_GridDoubleClick(object sender, EventArgs e)
		{
			if (propState.SelectedObject is FSMState)
			{
				if (propState.SelectedGridItem.Label == "LookAtObject")
				{
					SetObjectSelectMode(true);
					mSelectedGridItem = propState.SelectedGridItem;
					ViewportControl.This.Pane.ActiveContent = ViewportControl.This;
				}
			}
			else
			{
				SetObjectSelectMode(false);
				mSelectedGridItem = null;
			}
		}

		private void tvwEvent_MouseMove(object sender, MouseEventArgs e)
		{
			if (propState.SelectedObject == null)
			{
				cmbCreateEventTrigger.Visible = false;
				cmbCreateEvent.Visible = false;
				return;
			}

			TreeNode nodeOnMouse = tvwEvent.GetNodeAt(new Point(30, e.Y));
			if (nodeOnMouse == null)
			{
				cmbCreateEventTrigger.Visible = false;
				cmbCreateEvent.Visible = false;
				return;
			}

			// 마우스가 위치한 곳의 마지막 이벤트 노드 위치에 이벤트 추가 버튼을 놓는다.
			TreeNode lastEventTriggerNode = null;
			TreeNode lastEventNode = null;
			if (nodeOnMouse.Level == 0)
			{
				lastEventNode = nodeOnMouse.LastNode;
				lastEventTriggerNode = tvwEvent.Nodes[tvwEvent.Nodes.Count - 1];

				mEventTriggerNodeOnMouse = nodeOnMouse;
				mLastEventTriggerNodeOnMouse = lastEventTriggerNode;
				mLastEventNodeOnMouse = lastEventNode;
			}
			else if (nodeOnMouse.Level == 1)
			{
				lastEventNode = nodeOnMouse.Parent.LastNode;
				lastEventTriggerNode = tvwEvent.Nodes[tvwEvent.Nodes.Count - 1];

				mEventTriggerNodeOnMouse = nodeOnMouse.Parent;
				mLastEventTriggerNodeOnMouse = lastEventTriggerNode;
				mLastEventNodeOnMouse = lastEventNode;
			}
			else
			{
				return;
			}

			if (mEventTriggerNodeOnMouse == null)
			{
				cmbCreateEventTrigger.Visible = false;
			}
			else
			{
				cmbCreateEventTrigger.Visible = true;
				cmbCreateEventTrigger.Location = lastEventTriggerNode.Bounds.Location;
			}

			if (lastEventNode == null)
			{
				cmbCreateEvent.Visible = false;
			}
			else
			{
				cmbCreateEvent.Visible = true;
				cmbCreateEvent.Location = lastEventNode.Bounds.Location;
			}
		}

		private void tvwEvent_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			// 이벤트 발생 조건 노드 선택
			if (e.Node.Level == 0)
			{
				FSMEventTrigger trigger = e.Node.Tag as FSMEventTrigger;
				propEvent.SelectedObject = trigger;
			}
			// 이벤트 노드 선택
			else if (e.Node.Level == 1)
			{
				FSMEvent evt = e.Node.Tag as FSMEvent;
				propEvent.SelectedObject = evt;
			}
		}

		private void tvwEvent_KeyDown(object sender, KeyEventArgs e)
		{
			// Delete키 누르면 선택된 이벤트 발생 조건 및 이벤트를 삭제함
			if (tvwEvent.SelectedNode != null)
			{
				FSMState state = propState.SelectedObject as FSMState;

				if (tvwEvent.SelectedNode.Tag is FSMEventTrigger)
				{
					state.RemoveEventTrigger(tvwEvent.SelectedNode.Tag as FSMEventTrigger);
				}
				else if (tvwEvent.SelectedNode.Tag is FSMEvent)
				{
					state.RemoveEvent(tvwEvent.SelectedNode.Tag as FSMEvent);
				}

				tvwEvent.Nodes.Remove(tvwEvent.SelectedNode);
				tvwEvent.SelectedNode = null;
			}
		}

		private void cmbCreateEventTrigger_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (propState.SelectedObject == null || cmbCreateEventTrigger.SelectedIndex == -1)
			{
				return;
			}

			// 상태에 이벤트가 하나도 없을 경우
			//if (tvwEvent.Nodes.Count == 1 && mEventTriggerNodeOnMouse == null)
			//{
			//    mEventTriggerNodeOnMouse = tvwEvent.Nodes[0];
			//}

			FSMEventTriggerType triggerType = (FSMEventTriggerType)cmbCreateEventTrigger.SelectedIndex;
			FSMState state = propState.SelectedObject as FSMState;
			FSMEventTrigger trigger = state.AddEventTrigger(triggerType);

			mLastEventTriggerNodeOnMouse.Text = FSMEventTriggerTypeDescription.Description(triggerType);
			mLastEventTriggerNodeOnMouse.Tag = trigger;
			mLastEventTriggerNodeOnMouse.Nodes.Add(mBlankTreeNodeText);
			mLastEventTriggerNodeOnMouse.ExpandAll();

			tvwEvent.Nodes.Add(mBlankTreeNodeText);

			cmbCreateEventTrigger.SelectedIndex = -1;

			tvwEvent.Refresh();
		}

		private void cmbCreateEvent_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (propState.SelectedObject == null || cmbCreateEvent.SelectedIndex == -1)
			{
				return;
			}

			FSMEventType eventType = (FSMEventType)cmbCreateEvent.SelectedIndex;
			FSMState state = propState.SelectedObject as FSMState;
			FSMEvent evt = state.AddEvent(mEventTriggerNodeOnMouse.Tag as FSMEventTrigger, eventType);

			mLastEventNodeOnMouse.Text = FSMEventTypeDescription.Description(eventType);
			mLastEventNodeOnMouse.Tag = evt;
			mLastEventNodeOnMouse.Parent.Nodes.Add(mBlankTreeNodeText);

			cmbCreateEvent.SelectedIndex = -1;

			cmbCreateEventTrigger.Visible = false;
			cmbCreateEvent.Visible = false;

			tvwEvent.Refresh();
		}

		private void propEvent_SelectedGridItemChanged(object sender, SelectedGridItemChangedEventArgs e)
		{
			//if (e.NewSelection.Label != "TargetObject" && e.NewSelection.Label != "TargetRegion")
			//{
			//    SetObjectSelectMode(false);
			//}

			if (e.NewSelection.Label == "TargetPoint")
			{
				TargetPointManager.This.ShowTargetPoint(true);
			}
			else
			{
				TargetPointManager.This.ShowTargetPoint(false);
			}
		}

		private void propEvent_GridDoubleClick(object sender, EventArgs e)
		{
			if (propEvent.SelectedObject is FSMEventTrigger)
			{
				if (propEvent.SelectedGridItem.Label == "TargetObject")
				{
					SetObjectSelectMode(true);
					mSelectedGridItem = propEvent.SelectedGridItem;
					ViewportControl.This.Pane.ActiveContent = ViewportControl.This;
				}

				if (propEvent.SelectedObject.GetType() == typeof(FSMZoneEnterTrigger) ||
					propEvent.SelectedObject.GetType() == typeof(FSMZoneExitTrigger))
				{
					if (propEvent.SelectedGridItem.Label == "TargetRegion")
					{
						SetRegionSelectMode(true);
						mSelectedGridItem = propEvent.SelectedGridItem;
						ViewportControl.This.Pane.ActiveContent = ViewportControl.This;
					}
				}
			}
			else if (propEvent.SelectedObject is FSMEvent)
			{
				if (propEvent.SelectedGridItem.Label == "TargetObject")
				{
					SetObjectSelectMode(true);
					mSelectedGridItem = propEvent.SelectedGridItem;
					ViewportControl.This.Pane.ActiveContent = ViewportControl.This;
				}

				if(propEvent.SelectedObject.GetType() == typeof(FSMMoveObjectEvent))
				{
					if (propEvent.SelectedGridItem.Label == "TargetPoint")
					{
						SetPointSelectMode(true);
						mSelectedGridItem = propEvent.SelectedGridItem;
						ViewportControl.This.Pane.ActiveContent = ViewportControl.This;
					}
				}
			}
			else
			{
				SetObjectSelectMode(false);
				SetPointSelectMode(false);
				TargetPointManager.This.ShowTargetPoint(false);
				mSelectedGridItem = null;
			}
		}
	}
}

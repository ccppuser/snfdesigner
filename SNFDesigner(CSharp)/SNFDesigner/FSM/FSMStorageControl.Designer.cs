﻿namespace SNFDesigner
{
	partial class FSMStorageControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.lvwFSMStorage = new System.Windows.Forms.ListView();
			this.btnCreateFSM = new System.Windows.Forms.Button();
			this.btnRemoveFSM = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.propFSM = new CustomPropertyGrid();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold);
			this.label1.Location = new System.Drawing.Point(50, 28);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(117, 16);
			this.label1.TabIndex = 3;
			this.label1.Text = "FSM 스토리지";
			// 
			// lvwFSMStorage
			// 
			this.lvwFSMStorage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lvwFSMStorage.FullRowSelect = true;
			this.lvwFSMStorage.Location = new System.Drawing.Point(12, 117);
			this.lvwFSMStorage.Name = "lvwFSMStorage";
			this.lvwFSMStorage.Size = new System.Drawing.Size(200, 252);
			this.lvwFSMStorage.TabIndex = 6;
			this.lvwFSMStorage.UseCompatibleStateImageBehavior = false;
			this.lvwFSMStorage.View = System.Windows.Forms.View.List;
			this.lvwFSMStorage.DoubleClick += new System.EventHandler(this.lvwFSMStorage_DoubleClick);
			this.lvwFSMStorage.Click += new System.EventHandler(this.lvwFSMStorage_Click);
			// 
			// btnCreateFSM
			// 
			this.btnCreateFSM.Location = new System.Drawing.Point(12, 59);
			this.btnCreateFSM.Name = "btnCreateFSM";
			this.btnCreateFSM.Size = new System.Drawing.Size(200, 23);
			this.btnCreateFSM.TabIndex = 7;
			this.btnCreateFSM.Text = "새 FSM 추가";
			this.btnCreateFSM.UseVisualStyleBackColor = true;
			this.btnCreateFSM.Click += new System.EventHandler(this.btnCreateFSM_Click);
			// 
			// btnRemoveFSM
			// 
			this.btnRemoveFSM.Location = new System.Drawing.Point(12, 88);
			this.btnRemoveFSM.Name = "btnRemoveFSM";
			this.btnRemoveFSM.Size = new System.Drawing.Size(200, 23);
			this.btnRemoveFSM.TabIndex = 8;
			this.btnRemoveFSM.Text = "선택한 FSM 삭제";
			this.btnRemoveFSM.UseVisualStyleBackColor = true;
			this.btnRemoveFSM.Click += new System.EventHandler(this.btnRemoveFSM_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::SNFDesigner.Properties.Resources.FSMStorage;
			this.pictureBox1.Location = new System.Drawing.Point(12, 12);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(32, 32);
			this.pictureBox1.TabIndex = 2;
			this.pictureBox1.TabStop = false;
			// 
			// propFSM
			// 
			this.propFSM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.propFSM.Location = new System.Drawing.Point(12, 375);
			this.propFSM.Name = "propFSM";
			this.propFSM.Size = new System.Drawing.Size(200, 179);
			this.propFSM.TabIndex = 9;
			this.propFSM.SelectedGridItemChanged += new System.Windows.Forms.SelectedGridItemChangedEventHandler(this.propFSM_SelectedGridItemChanged);
			// 
			// FSMStorageControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(224, 566);
			this.CloseButtonVisible = false;
			this.ControlBox = false;
			this.Controls.Add(this.propFSM);
			this.Controls.Add(this.btnRemoveFSM);
			this.Controls.Add(this.btnCreateFSM);
			this.Controls.Add(this.lvwFSMStorage);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.pictureBox1);
			this.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "FSMStorageControl";
			this.Text = "FSM 스토리지";
			this.Load += new System.EventHandler(this.FSMStorageControl_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.ListView lvwFSMStorage;
		private System.Windows.Forms.Button btnCreateFSM;
		private System.Windows.Forms.Button btnRemoveFSM;
		private CustomPropertyGrid propFSM;
	}
}
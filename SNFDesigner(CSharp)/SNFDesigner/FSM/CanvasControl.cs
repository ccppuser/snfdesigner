﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SNFDesigner
{
	public partial class CanvasControl : UserControl
	{
		public CanvasControl()
		{
			InitializeComponent();

			BackColor = SystemColors.Control;
			SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			SetStyle(ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using Mogre;

namespace SNFDesigner
{
	public enum FSMEventTriggerType
	{
		SightEnter,	// 시야 침범 감지
		SightExit,	// 시야 퇴장 감지
		ZoneEnter,	// 영역 침범 감지
		ZoneExit,	// 영역 퇴장 감지
		ElapsedTime,	// 일정 시간 경과
		AnimationEnded,	// 애니메이션 종료
		AnimationRepeated,	// 애니메이션 일정 횟수 반복
	}

	public static class FSMEventTriggerTypeDescription
	{
		public static string Description(FSMEventTriggerType triggerType)
		{
			switch (triggerType)
			{
				case FSMEventTriggerType.SightEnter: return "시야 침범 감지";
				case FSMEventTriggerType.SightExit: return "시야 퇴장 감지";
				case FSMEventTriggerType.ZoneEnter: return "영역 침범 감지";
				case FSMEventTriggerType.ZoneExit: return "영역 퇴장 감지";
				case FSMEventTriggerType.ElapsedTime: return "일정 시간 경과";
				case FSMEventTriggerType.AnimationEnded: return "애니메이션 종료";
				case FSMEventTriggerType.AnimationRepeated: return "애니메이션 일정 횟수 반복";
				default: return "";
			}
		}
	}

	public abstract class FSMEventTrigger
	{
		protected FSM mFSM;
		protected FSMEventTriggerType mTriggerType;

		[CategoryAttribute("이벤트 발동 조건 속성")]
		public FSMEventTriggerType TriggerType
		{
			get { return mTriggerType; }
		}

		[BrowsableAttribute(false)]
		public Object AgentObject
		{
			get { return mFSM.AssignedObject; }
		}

		public FSMEventTrigger(FSM fsm)
		{
			mFSM = fsm;
		}

		public virtual void Initialize()
		{
		}

		// 이벤트 발생 조건을 만족하는지 검사
		public abstract bool CheckCondition();
	}

	public class FSMSightEnterTrigger : FSMEventTrigger
	{
		protected Object mTargetObject = null;
		private bool mJustEnteredInSight = false;

		[TypeConverter(typeof(ObjectConverter)),
		CategoryAttribute("이벤트 발동 조건 속성")]
		public Object TargetObject
		{
			get { return mTargetObject; }
			set { mTargetObject = value; }
		}

		public FSMSightEnterTrigger(FSM fsm)
			: base(fsm)
		{
			mTriggerType = FSMEventTriggerType.SightEnter;
		}

		public override void Initialize()
		{
			mJustEnteredInSight = false;
		}

		public override bool CheckCondition()
		{
			// obj의 시야에 targetObj가 있는가
			if (AgentObject.IsInSight(mTargetObject))
			{
				if (!mJustEnteredInSight)
				{
					mJustEnteredInSight = true;
					return true;
				}
			}
			else
			{
				mJustEnteredInSight = false;
			}

			return false;
		}
	}

	public class FSMSightExitTrigger : FSMEventTrigger
	{
		protected Object mTargetObject = null;
		private bool mJustExitedInSight = false;

		[TypeConverter(typeof(ObjectConverter)),
		CategoryAttribute("이벤트 발동 조건 속성")]
		public Object TargetObject
		{
			get { return mTargetObject; }
			set { mTargetObject = value; }
		}

		public FSMSightExitTrigger(FSM fsm)
			: base(fsm)
		{
			mTriggerType = FSMEventTriggerType.SightExit;
		}

		public override void Initialize()
		{
			mJustExitedInSight = false;
		}

		public override bool CheckCondition()
		{
			// obj의 시야에 targetObj가 없다
			if (!AgentObject.IsInSight(mTargetObject))
			{
				if (!mJustExitedInSight)
				{
					mJustExitedInSight = true;
					return true;
				}
			}
			else
			{
				mJustExitedInSight = false;
			}

			return false;
		}
	}

	public class FSMZoneEnterTrigger : FSMEventTrigger
	{
		protected Object mTargetObject = null;
		private Region mRegion = null;
		private bool mJustEnteredInRegion = false;

		[TypeConverter(typeof(ObjectConverter)),
		CategoryAttribute("이벤트 발동 조건 속성")]
		public Object TargetObject
		{
			get { return mTargetObject; }
			set { mTargetObject = value; }
		}

		[TypeConverter(typeof(RegionConverter)),
		CategoryAttribute("이벤트 발동 조건 속성")]
		public Region TargetRegion
		{
			get { return mRegion; }
			set { mRegion = value; }
		}

		public FSMZoneEnterTrigger(FSM fsm)
			: base(fsm)
		{
			mTriggerType = FSMEventTriggerType.ZoneEnter;
		}

		public override void Initialize()
		{
			mJustEnteredInRegion = false;
		}

		public override bool CheckCondition()
		{
			// 영역에 들어왔다
			if (mRegion != null && mRegion.Intersects(mTargetObject))
			{
				if (!mJustEnteredInRegion)
				{
					mJustEnteredInRegion = true;
					return true;
				}
			}
			else
			{
				mJustEnteredInRegion = false;
			}

			return false;
		}
	}

	public class FSMZoneExitTrigger : FSMEventTrigger
	{
		protected Object mTargetObject = null;
		private Region mRegion = null;
		private bool mJustExitedInRegion = false;

		[TypeConverter(typeof(ObjectConverter)),
		CategoryAttribute("이벤트 발동 조건 속성")]
		public Object TargetObject
		{
			get { return mTargetObject; }
			set { mTargetObject = value; }
		}

		[TypeConverter(typeof(RegionConverter)),
		CategoryAttribute("이벤트 발동 조건 속성")]
		public Region TargetRegion
		{
			get { return mRegion; }
			set { mRegion = value; }
		}

		public FSMZoneExitTrigger(FSM fsm)
			: base(fsm)
		{
			mTriggerType = FSMEventTriggerType.ZoneExit;
		}

		public override void Initialize()
		{
			mJustExitedInRegion = false;
		}

		public override bool CheckCondition()
		{
			// 영역에 없다
			if (mRegion != null && !mRegion.Intersects(mTargetObject))
			{
				if (!mJustExitedInRegion)
				{
					mJustExitedInRegion = true;
					return true;
				}
			}
			else
			{
				mJustExitedInRegion = false;
			}

			return false;
		}
	}

	public class FSMElapsedTimeTrigger : FSMEventTrigger
	{
		private float mTargetElapsedTime = 0;
		private float mElapsedTime = 0;

		[CategoryAttribute("이벤트 발동 조건 속성")]
		public float TargetElapsedTime
		{
			get { return mTargetElapsedTime; }
			set { mTargetElapsedTime = value; }
		}

		public FSMElapsedTimeTrigger(FSM fsm)
			: base(fsm)
		{
			mTriggerType = FSMEventTriggerType.ElapsedTime;
		}

		public override void Initialize()
		{
			mElapsedTime = 0;
		}

		public override bool CheckCondition()
		{
			mElapsedTime += OgreManager.This.TimeDelta;
			if (mElapsedTime >= mTargetElapsedTime)
			{
				return true;
			}

			return false;
		}
	}

	public class FSMAnimationEndedTrigger : FSMEventTrigger
	{
		private string mAnimationName;
		private AnimationState mAnimationState = null;

		public FSMAnimationEndedTrigger(FSM fsm, string animationName)
			: base(fsm)
		{
			mTriggerType = FSMEventTriggerType.AnimationEnded;
			mAnimationName = animationName;
		}

		public override void Initialize()
		{
			mAnimationState = OgreManager.This.GetAnimationState(AgentObject, mAnimationName);
		}

		public override bool CheckCondition()
		{
			if (mAnimationState != null && mAnimationState.HasEnded)
			{
				return true;
			}

			return false;
		}
	}

	public class FSMAnimationRepeatedTrigger : FSMEventTrigger
	{
		private int mTargetRepeatedNumber = 0;
		private int mRepeatedNumber = 0;
		private string mAnimationName;
		private AnimationState mAnimationState = null;
		private float mLastTimePosition = 0;

		[CategoryAttribute("이벤트 발동 조건 속성")]
		public int TargetRepeatedNumber
		{
			get { return mTargetRepeatedNumber; }
			set { mTargetRepeatedNumber = value; }
		}

		public FSMAnimationRepeatedTrigger(FSM fsm, string animationName)
			: base(fsm)
		{
			mTriggerType = FSMEventTriggerType.AnimationRepeated;
			mAnimationName = animationName;
		}

		public override void Initialize()
		{
			mRepeatedNumber = 0;

			mAnimationState = OgreManager.This.GetAnimationState(AgentObject, mAnimationName);

			mLastTimePosition = 0;
		}

		public override bool CheckCondition()
		{
			if (mAnimationState == null)
			{
				return false;
			}

			if (mAnimationState.TimePosition < mLastTimePosition)
			{
				++mRepeatedNumber;
			}

			if (mRepeatedNumber >= mTargetRepeatedNumber)
			{
				return true;
			}

			mLastTimePosition = mAnimationState.TimePosition;

			return false;
		}
	}
}
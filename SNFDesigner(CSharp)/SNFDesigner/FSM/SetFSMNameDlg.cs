﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SNFDesigner
{
	public partial class SetFSMNameDlg : Form
	{
		private bool mCancel = false;

		public string FSMName
		{
			get { return txtFSMName.Text; }
		}

		public SetFSMNameDlg()
		{
			InitializeComponent();
		}

		private void SetFSMNameDlg_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (mCancel)
				return;

			if (txtFSMName.Text.Length == 0)
			{
				MessageBox.Show("FSM's name cannot be empty.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
			}

			if (SNFDocument.This.GetFSM(txtFSMName.Text) != null)
			{
				MessageBox.Show("FSM's name is duplicated.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			mCancel = true;
		}

		private void txtFSMName_MouseClick(object sender, MouseEventArgs e)
		{
			txtFSMName.SelectAll();
		}
	}
}

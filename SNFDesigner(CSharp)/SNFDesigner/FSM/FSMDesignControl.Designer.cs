﻿namespace SNFDesigner
{
	partial class FSMDesignControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.AddNewStateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// contextMenuStrip
			// 
			this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddNewStateToolStripMenuItem});
			this.contextMenuStrip.Name = "contextMenuStrip";
			this.contextMenuStrip.Size = new System.Drawing.Size(164, 26);
			// 
			// AddNewStateToolStripMenuItem
			// 
			this.AddNewStateToolStripMenuItem.Name = "AddNewStateToolStripMenuItem";
			this.AddNewStateToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
			this.AddNewStateToolStripMenuItem.Text = "새 상태 추가 (&N)";
			this.AddNewStateToolStripMenuItem.Click += new System.EventHandler(this.AddNewStateToolStripMenuItem_Click);
			// 
			// FSMDesignControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(284, 262);
			this.CloseButtonVisible = false;
			this.ContextMenuStrip = this.contextMenuStrip;
			this.DoubleBuffered = true;
			this.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Name = "FSMDesignControl";
			this.Text = "FSM 디자인";
			this.Load += new System.EventHandler(this.FSMDesignControl_Load);
			this.contextMenuStrip.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem AddNewStateToolStripMenuItem;
	}
}
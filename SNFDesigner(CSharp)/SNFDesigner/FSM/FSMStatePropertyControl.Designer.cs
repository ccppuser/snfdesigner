﻿namespace SNFDesigner
{
	partial class FSMStatePropertyControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.tvwEvent = new System.Windows.Forms.TreeView();
			this.cmbCreateEventTrigger = new System.Windows.Forms.ComboBox();
			this.cmbCreateEvent = new System.Windows.Forms.ComboBox();
			this.propEvent = new SNFDesigner.CustomPropertyGrid();
			this.propState = new SNFDesigner.CustomPropertyGrid();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label1.Location = new System.Drawing.Point(50, 28);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(123, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "FSM 상태 속성";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::SNFDesigner.Properties.Resources.FSM;
			this.pictureBox1.Location = new System.Drawing.Point(12, 12);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(32, 32);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// tvwEvent
			// 
			this.tvwEvent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tvwEvent.FullRowSelect = true;
			this.tvwEvent.ItemHeight = 20;
			this.tvwEvent.Location = new System.Drawing.Point(12, 216);
			this.tvwEvent.Name = "tvwEvent";
			this.tvwEvent.Size = new System.Drawing.Size(200, 160);
			this.tvwEvent.TabIndex = 3;
			this.tvwEvent.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tvwEvent_MouseMove);
			this.tvwEvent.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvwEvent_NodeMouseClick);
			this.tvwEvent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tvwEvent_KeyDown);
			// 
			// cmbCreateEventTrigger
			// 
			this.cmbCreateEventTrigger.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbCreateEventTrigger.FormattingEnabled = true;
			this.cmbCreateEventTrigger.Location = new System.Drawing.Point(12, 287);
			this.cmbCreateEventTrigger.Name = "cmbCreateEventTrigger";
			this.cmbCreateEventTrigger.Size = new System.Drawing.Size(179, 20);
			this.cmbCreateEventTrigger.TabIndex = 6;
			this.cmbCreateEventTrigger.Visible = false;
			this.cmbCreateEventTrigger.SelectedIndexChanged += new System.EventHandler(this.cmbCreateEventTrigger_SelectedIndexChanged);
			// 
			// cmbCreateEvent
			// 
			this.cmbCreateEvent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbCreateEvent.FormattingEnabled = true;
			this.cmbCreateEvent.Location = new System.Drawing.Point(12, 313);
			this.cmbCreateEvent.Name = "cmbCreateEvent";
			this.cmbCreateEvent.Size = new System.Drawing.Size(81, 20);
			this.cmbCreateEvent.TabIndex = 7;
			this.cmbCreateEvent.Visible = false;
			this.cmbCreateEvent.SelectedIndexChanged += new System.EventHandler(this.cmbCreateEvent_SelectedIndexChanged);
			// 
			// propEvent
			// 
			this.propEvent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.propEvent.Location = new System.Drawing.Point(12, 382);
			this.propEvent.Name = "propEvent";
			this.propEvent.Size = new System.Drawing.Size(200, 172);
			this.propEvent.TabIndex = 8;
			this.propEvent.SelectedGridItemChanged += new System.Windows.Forms.SelectedGridItemChangedEventHandler(this.propEvent_SelectedGridItemChanged);
			// 
			// propState
			// 
			this.propState.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.propState.Location = new System.Drawing.Point(12, 50);
			this.propState.Name = "propState";
			this.propState.Size = new System.Drawing.Size(200, 160);
			this.propState.TabIndex = 2;
			this.propState.SelectedGridItemChanged += new System.Windows.Forms.SelectedGridItemChangedEventHandler(this.propState_SelectedGridItemChanged);
			// 
			// FSMStatePropertyControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(224, 566);
			this.CloseButtonVisible = false;
			this.ControlBox = false;
			this.Controls.Add(this.propEvent);
			this.Controls.Add(this.cmbCreateEvent);
			this.Controls.Add(this.cmbCreateEventTrigger);
			this.Controls.Add(this.tvwEvent);
			this.Controls.Add(this.propState);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.pictureBox1);
			this.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "FSMStatePropertyControl";
			this.Text = "FSM 상태 속성";
			this.Load += new System.EventHandler(this.FSMPropertyControl_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label label1;
		private CustomPropertyGrid propState;
		private System.Windows.Forms.TreeView tvwEvent;
		private System.Windows.Forms.ComboBox cmbCreateEventTrigger;
		private System.Windows.Forms.ComboBox cmbCreateEvent;
		private CustomPropertyGrid propEvent;
	}
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using Mogre;

namespace SNFDesigner
{
	public enum AxisType
	{
		X,
		Y,
		Z,
	}

	public class Axis
	{
		private SceneManager mSceneManager = null;
		Entity mXAxis = null;
		Entity mYAxis = null;
		Entity mZAxis = null;
		SceneNode mAxisNode = null;

		public Axis(SceneManager sceneManager)
		{
			mSceneManager = sceneManager;
		}

		public void Create()
		{
			// 3D 화살표 생성
			mXAxis = mSceneManager.CreateEntity("XAxis", "xaxis.mesh");
			mXAxis.QueryFlags = QueryFlags.AXIS;	// 화살표 전용 쿼리 마스크 설정
			mXAxis.RenderQueueGroup = (byte)RenderQueueGroupID.RENDER_QUEUE_MAX;
			mXAxis.GetSubEntity(0).GetMaterial().SetDepthCheckEnabled(false);

			mYAxis = mSceneManager.CreateEntity("YAxis", "yaxis.mesh");
			mYAxis.QueryFlags = QueryFlags.AXIS;	// 화살표 전용 쿼리 마스크 설정
			mYAxis.RenderQueueGroup = (byte)RenderQueueGroupID.RENDER_QUEUE_MAX;
			mYAxis.GetSubEntity(0).GetMaterial().SetDepthCheckEnabled(false);

			mZAxis = mSceneManager.CreateEntity("ZAxis", "zaxis.mesh");
			mZAxis.QueryFlags = QueryFlags.AXIS;	// 화살표 전용 쿼리 마스크 설정
			mZAxis.RenderQueueGroup = (byte)RenderQueueGroupID.RENDER_QUEUE_MAX;
			mZAxis.GetSubEntity(0).GetMaterial().SetDepthCheckEnabled(false);

			mAxisNode = mSceneManager.RootSceneNode.CreateChildSceneNode("AxisNode");
			mAxisNode.AttachObject(mXAxis);
			mAxisNode.AttachObject(mYAxis);
			mAxisNode.AttachObject(mZAxis);
			mAxisNode.SetScale(0.005f, 0.005f, 0.005f);

			//mAxis.RenderQueueGroup = (byte)RenderQueueGroupID.RENDER_QUEUE_MAX;	// 어떠한 다른 오브젝트보다 앞에 보이도록 함
			//mAxis.SetRenderQueueGroupAndPriority((byte)RenderQueueGroupID.RENDER_QUEUE_MAX, 0);
			//mAxis.SetRenderQueueGroupAndPriority(50, 0);

			// TEST:
			//Entity test01 = mSceneManager.CreateEntity("Test01", "Barrel.mesh");
			//test01.QueryFlags = QueryFlags.EXCEPT;
			//SceneNode testNode01 = mSceneManager.RootSceneNode.CreateChildSceneNode("TestNode01");
			//testNode01.AttachObject(test01);
			//Entity test02 = mSceneManager.CreateEntity("Test02", "Barrel.mesh");
			//test02.QueryFlags = QueryFlags.EXCEPT;
			//SceneNode testNode02 = mSceneManager.RootSceneNode.CreateChildSceneNode("TestNode02");
			//testNode02.AttachObject(test02);
			//Entity test03 = mSceneManager.CreateEntity("Test03", "Barrel.mesh");
			//test03.QueryFlags = QueryFlags.EXCEPT;
			//SceneNode testNode03 = mSceneManager.RootSceneNode.CreateChildSceneNode("TestNode03");
			//testNode03.AttachObject(test03);

			//testNode01.SetVisible(false);
			//testNode02.SetVisible(false);
			//testNode03.SetVisible(false);

			// 일단 숨겨놓음
			SetVisible(false);
		}

		public void Destroy()
		{
			if (mXAxis != null)
			{
				mSceneManager.DestroyEntity(mXAxis);
			}
			if (mYAxis != null)
			{
				mSceneManager.DestroyEntity(mYAxis);
			}
			if (mZAxis != null)
			{
				mSceneManager.DestroyEntity(mZAxis);
			}

			if (mAxisNode != null)
			{
				mSceneManager.DestroySceneNode(mAxisNode);
			}
		}

		public void SetVisible(bool visible)
		{
			if (visible)
			{
				if (mAxisNode.Parent != null)
				{
					mAxisNode.Parent.RemoveChild(mAxisNode);
				}

				mSceneManager.RootSceneNode.AddChild(mAxisNode);
			}
			else
			{
				if (mAxisNode.Parent != null)
				{
					mAxisNode.Parent.RemoveChild(mAxisNode);
				}
			}
		}

		private Vector3 mWorldPosition;

		public void UpdatePosition()
		{
			mAxisNode.Position = OgreManager.This.CalculateScreenPosition(mWorldPosition);
		}

		public void SetPosition(Vector3 position)
		{
			mWorldPosition = position;
			UpdatePosition();
		}

		// 대상 node를 scale을 제외한 translation, rotation만 동일하게 함
		public void Follow(SceneNode node)
		{
			if (node == null)
			{
				return;
			}

			mWorldPosition = node._getDerivedPosition();
			UpdatePosition();
			mAxisNode.Orientation = node._getDerivedOrientation();
		}

		public void FollowTranslation(SceneNode node)
		{
			if (node == null)
			{
				return;
			}

			mWorldPosition = node._getDerivedPosition();
			UpdatePosition();
		}

		public void FollowRotation(SceneNode node)
		{
			if (node == null)
			{
				return;
			}

			mAxisNode.Orientation = node._getDerivedOrientation();
		}

		//public void SetParent(SceneNode node)
		//{
		//    if (mAxisNode.Parent != null)
		//    {
		//        mAxisNode.Parent.RemoveChild(mAxisNode);
		//    }

		//    if (node != null)
		//    {
		//        node.AddChild(mAxisNode);
		//    }
		//}

		public AxisType GetAxisType(Entity entity)
		{
			if (entity == mXAxis)
			{
				return AxisType.X;
			}
			else if (entity == mYAxis)
			{
				return AxisType.Y;
			}
			else if (entity == mZAxis)
			{
				return AxisType.Z;
			}
			else
			{
				throw new Exception("Axis.GetAxisType(): 잘못된 엔티티");
			}
		}
	}
}

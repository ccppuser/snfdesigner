﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SNFDesigner
{
	public partial class SetRegionNameDlg : Form
	{
		private bool mCancel = false;

		public string RegionName
		{
			get { return txtRegionName.Text; }
		}

		public SetRegionNameDlg()
		{
			InitializeComponent();
		}

		private void SetRegionNameDlg_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (mCancel)
			{
				return;
			}

			if (txtRegionName.Text.Length == 0)
			{
				MessageBox.Show("The name of a region cannot be empty.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
			}

			if (SNFDocument.This.GetRegion(txtRegionName.Text) != null)
			{
				MessageBox.Show("The name of a region is duplicated.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			mCancel = true;
		}

		private void txtRegionName_MouseClick(object sender, MouseEventArgs e)
		{
			txtRegionName.SelectAll();
		}
	}
}

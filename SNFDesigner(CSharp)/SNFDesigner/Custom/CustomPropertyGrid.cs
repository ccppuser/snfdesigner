﻿using System;
using System.Windows.Forms;

namespace SNFDesigner
{
	public class CustomPropertyGrid : PropertyGrid
	{
		private const int WM_PARENTNOTIFY = 0x0210;
		private const int WM_LBUTTONDOWN = 0x0201;

		private DateTime mLastClickTime = DateTime.Now;

		public event EventHandler GridDoubleClick;

		protected override void WndProc(ref Message m)
		{
			base.WndProc(ref m);

			switch (m.Msg)
			{
				case WM_PARENTNOTIFY:
					if (m.WParam.ToInt32().Equals(WM_LBUTTONDOWN))
					{
						DateTime currTime = DateTime.Now;
						TimeSpan ts = currTime - mLastClickTime;
						if (ts.TotalMilliseconds < SystemInformation.DoubleClickTime)
						{
							GridDoubleClick(this, EventArgs.Empty);
						}

						mLastClickTime = DateTime.Now;
					}

					break;
			}
		}
	}
}

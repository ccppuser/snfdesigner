﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Mogre;

namespace SNFDesigner
{
	// Object에서 AssignedFSM속성을 더블 클릭했을 때,
	// 현재 만들어져 있는 FSM의 목록을 보여준다.
	public class AssignedFSMConverter : TypeConverter
	{
		public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
		{
			return true;
		}

		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			return new StandardValuesCollection(SNFDocument.This.GetFSMNames());
		}

		public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
		{
			return true;
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))
			{
				return true;
			}

			return base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if (value is string)
			{
				string fsmName = value as string;
				return SNFDocument.This.GetFSM(fsmName);
			}

			return base.ConvertFrom(context, culture, value);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof(string))
			{
				return true;
			}

			return base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			if (value is FSM && destinationType == typeof(string))
			{
				FSM fsm = value as FSM;
				return fsm.Name;
			}

			return base.ConvertTo(context, culture, value, destinationType);
		}
	}

	public class InitialStateConverter : TypeConverter
	{
		public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
		{
			return true;
		}

		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			FSM fsm = context.Instance as FSM;
			return new StandardValuesCollection(fsm.GetStateNames());
		}

		public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
		{
			return true;
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))
			{
				return true;
			}

			return base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if (value is string)
			{
				FSM fsm = context.Instance as FSM;
				string stateName = value as string;
				return fsm.GetState(stateName);
			}

			return base.ConvertFrom(context, culture, value);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof(string))
			{
				return true;
			}

			return base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			if (value is FSMState && destinationType == typeof(string))
			{
				FSM fsm = context.Instance as FSM;
				FSMState state = value as FSMState;
				return state.Name;
			}

			return base.ConvertTo(context, culture, value, destinationType);
		}
	}

	public class ObjectConverter : TypeConverter
	{
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof(Object))
			{
				return true;
			}

			return base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof(string) && value is Object)
			{
				Object obj = value as Object;

				return obj.ObjectName;
			}

			return base.ConvertTo(context, culture, value, destinationType);
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))
			{
				return true;
			}

			return base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if (value is string)
			{
				string objectName = value as string;
				return SNFDocument.This.GetObject(objectName);
			}

			return base.ConvertFrom(context, culture, value);
		}
	}

	// FSMChangeStateEvent에서 TargetState속성을 더블 클릭했을 때,
	// 현재 TargetObject에 적힌 대상 오브젝트가 가진 상태 목록을 보여준다.
	public class TargetStateConverter : TypeConverter
	{
		public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
		{
			return true;
		}

		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			FSMChangeStateEvent evt = context.Instance as FSMChangeStateEvent;
			FSM assignedFSM = evt.AgentObject.AssignedFSM;
			if (assignedFSM == null)
			{
				return null;
			}
			else
			{
				string[] stateNames = assignedFSM.GetStateNames();

				return new StandardValuesCollection(stateNames);
			}
		}

		public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
		{
			return true;
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))
			{
				return true;
			}

			return base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if (value is string)
			{
				FSMChangeStateEvent evt = context.Instance as FSMChangeStateEvent;
				FSM assignedFSM = evt.AgentObject.AssignedFSM;
				string stateName = value as string;
				return assignedFSM.GetState(stateName);
			}

			return base.ConvertFrom(context, culture, value);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof(string))
			{
				return true;
			}

			return base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			if (value is FSMState && destinationType == typeof(string))
			{
				FSMState state = value as FSMState;
				return state.Name;
			}

			return base.ConvertTo(context, culture, value, destinationType);
		}
	}

	public class Vector3Converter : TypeConverter
	{
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof(Mogre.Vector3))
			{
				return true;
			}

			return base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof(string) && value is Mogre.Vector3)
			{
				Mogre.Vector3 v = (Mogre.Vector3)value;

				return v.ToString();
			}

			return base.ConvertTo(context, culture, value, destinationType);
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))
			{
				return true;
			}

			return base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if (value is string)
			{
				try
				{
					string vString = (string)value;
					Mogre.Vector3 v = Mogre.StringConverter.ParseVector3(vString);
					return v;
				}
				catch
				{
					return new Mogre.Vector3();
				}
			}

			return base.ConvertFrom(context, culture, value);
		}
	}

	public class RegionConverter : TypeConverter
	{
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof(Region))
			{
				return true;
			}

			return base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof(string) && value is Region)
			{
				Region region = value as Region;

				return region.Name;
			}

			return base.ConvertTo(context, culture, value, destinationType);
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))
			{
				return true;
			}

			return base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if (value is string)
			{
				string regionName = value as string;
				return SNFDocument.This.GetRegion(regionName);
			}

			return base.ConvertFrom(context, culture, value);
		}
	}

	public class AnimationNameConverter : TypeConverter
	{
		public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
		{
			return true;
		}

		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			Object obj = null;
			if (context.Instance.GetType() == typeof(FSMState))
			{
				obj = ((FSMState)context.Instance).AgentObject;
			}

			if (obj == null)
			{
				return null;
			}

			Entity entity = ((SceneNode)OgreManager.This.RootSceneNode.GetChild(obj.ObjectName)).GetAttachedObject(0) as Entity;
			AnimationStateIterator animations = entity.AllAnimationStates.GetAnimationStateIterator();
			List<string> animationNames = new List<string>();
			while(animations.MoveNext())
			{
				animationNames.Add(animations.CurrentKey);
			}

			return new StandardValuesCollection(animationNames.ToArray());
		}

		public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
		{
			return true;
		}
	}
}
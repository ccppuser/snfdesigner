﻿namespace SNFDesigner
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
			WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
			WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
			WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
			WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
			WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
			WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
			WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
			WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
			WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
			WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
			WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
			WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
			WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
			WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.menuStrip = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveTheSceneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStrip = new System.Windows.Forms.ToolStrip();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.dockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
			this.btnSaveProject = new System.Windows.Forms.ToolStripButton();
			this.btnSelectObject = new System.Windows.Forms.ToolStripButton();
			this.btnTranslateObject = new System.Windows.Forms.ToolStripButton();
			this.btnRotateObject = new System.Windows.Forms.ToolStripButton();
			this.btnScaleObject = new System.Windows.Forms.ToolStripButton();
			this.btnRemoveObject = new System.Windows.Forms.ToolStripButton();
			this.btnCreateRegion = new System.Windows.Forms.ToolStripButton();
			this.btnShowSightRegion = new System.Windows.Forms.ToolStripButton();
			this.btnPlay = new System.Windows.Forms.ToolStripButton();
			this.btnStop = new System.Windows.Forms.ToolStripButton();
			this.menuStrip.SuspendLayout();
			this.toolStrip.SuspendLayout();
			this.statusStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip
			// 
			this.menuStrip.BackColor = System.Drawing.Color.Gainsboro;
			this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.helpToolStripMenuItem});
			this.menuStrip.Location = new System.Drawing.Point(0, 0);
			this.menuStrip.Name = "menuStrip";
			this.menuStrip.Size = new System.Drawing.Size(1008, 24);
			this.menuStrip.TabIndex = 0;
			this.menuStrip.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveTheSceneToolStripMenuItem,
            this.toolStripSeparator3,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// saveTheSceneToolStripMenuItem
			// 
			this.saveTheSceneToolStripMenuItem.Name = "saveTheSceneToolStripMenuItem";
			this.saveTheSceneToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
			this.saveTheSceneToolStripMenuItem.Text = "&Save the scene";
			this.saveTheSceneToolStripMenuItem.Click += new System.EventHandler(this.saveTheSceneToolStripMenuItem_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(151, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
			this.exitToolStripMenuItem.Text = "E&xit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "&Edit";
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.helpToolStripMenuItem.Text = "&Help";
			// 
			// toolStrip
			// 
			this.toolStrip.BackColor = System.Drawing.Color.Gainsboro;
			this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
			this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSaveProject,
            this.toolStripSeparator1,
            this.btnSelectObject,
            this.btnTranslateObject,
            this.btnRotateObject,
            this.btnScaleObject,
            this.btnRemoveObject,
            this.toolStripSeparator2,
            this.btnCreateRegion,
            this.btnShowSightRegion,
            this.toolStripSeparator4,
            this.btnPlay,
            this.btnStop});
			this.toolStrip.Location = new System.Drawing.Point(0, 24);
			this.toolStrip.Name = "toolStrip";
			this.toolStrip.Size = new System.Drawing.Size(1008, 39);
			this.toolStrip.TabIndex = 1;
			this.toolStrip.Text = "toolStrip1";
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
			// 
			// statusStrip
			// 
			this.statusStrip.BackColor = System.Drawing.Color.Gainsboro;
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
			this.statusStrip.Location = new System.Drawing.Point(0, 708);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(1008, 22);
			this.statusStrip.TabIndex = 2;
			this.statusStrip.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(39, 17);
			this.toolStripStatusLabel1.Text = "Ready";
			// 
			// saveFileDialog
			// 
			this.saveFileDialog.DefaultExt = "snf";
			this.saveFileDialog.Filter = "SNF Format Files(*.snf)|*.snf";
			// 
			// openFileDialog
			// 
			this.openFileDialog.DefaultExt = "snf";
			this.openFileDialog.Filter = "SNF Format Files(*.snf)|*.snf";
			// 
			// dockPanel
			// 
			this.dockPanel.ActiveAutoHideContent = null;
			this.dockPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dockPanel.DockBackColor = System.Drawing.Color.Gainsboro;
			this.dockPanel.DocumentStyle = WeifenLuo.WinFormsUI.Docking.DocumentStyle.DockingWindow;
			this.dockPanel.Location = new System.Drawing.Point(0, 66);
			this.dockPanel.Name = "dockPanel";
			this.dockPanel.Size = new System.Drawing.Size(1008, 639);
			dockPanelGradient1.EndColor = System.Drawing.SystemColors.ControlLight;
			dockPanelGradient1.StartColor = System.Drawing.SystemColors.ControlLight;
			autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
			tabGradient1.EndColor = System.Drawing.SystemColors.Control;
			tabGradient1.StartColor = System.Drawing.SystemColors.Control;
			tabGradient1.TextColor = System.Drawing.SystemColors.ControlDarkDark;
			autoHideStripSkin1.TabGradient = tabGradient1;
			autoHideStripSkin1.TextFont = new System.Drawing.Font("맑은 고딕", 9F);
			dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
			tabGradient2.EndColor = System.Drawing.SystemColors.ControlLightLight;
			tabGradient2.StartColor = System.Drawing.SystemColors.ControlLightLight;
			tabGradient2.TextColor = System.Drawing.SystemColors.ControlText;
			dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
			dockPanelGradient2.EndColor = System.Drawing.SystemColors.Control;
			dockPanelGradient2.StartColor = System.Drawing.SystemColors.Control;
			dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
			tabGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
			tabGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
			tabGradient3.TextColor = System.Drawing.SystemColors.ControlText;
			dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
			dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
			dockPaneStripSkin1.TextFont = new System.Drawing.Font("맑은 고딕", 9F);
			tabGradient4.EndColor = System.Drawing.SystemColors.ActiveCaption;
			tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
			tabGradient4.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
			tabGradient4.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
			dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
			tabGradient5.EndColor = System.Drawing.SystemColors.Control;
			tabGradient5.StartColor = System.Drawing.SystemColors.Control;
			tabGradient5.TextColor = System.Drawing.SystemColors.ControlText;
			dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
			dockPanelGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
			dockPanelGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
			dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
			tabGradient6.EndColor = System.Drawing.SystemColors.InactiveCaption;
			tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
			tabGradient6.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
			tabGradient6.TextColor = System.Drawing.SystemColors.InactiveCaptionText;
			dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
			tabGradient7.EndColor = System.Drawing.Color.Transparent;
			tabGradient7.StartColor = System.Drawing.Color.Transparent;
			tabGradient7.TextColor = System.Drawing.SystemColors.ControlDarkDark;
			dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
			dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
			dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
			this.dockPanel.Skin = dockPanelSkin1;
			this.dockPanel.TabIndex = 3;
			// 
			// btnSaveProject
			// 
			this.btnSaveProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnSaveProject.Image = global::SNFDesigner.Properties.Resources.Save;
			this.btnSaveProject.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnSaveProject.Name = "btnSaveProject";
			this.btnSaveProject.Size = new System.Drawing.Size(36, 36);
			this.btnSaveProject.Text = "save project";
			this.btnSaveProject.ToolTipText = "프로젝트 저장 (Ctrl + S)";
			this.btnSaveProject.Click += new System.EventHandler(this.btnSaveScene_Click);
			// 
			// btnSelectObject
			// 
			this.btnSelectObject.Checked = true;
			this.btnSelectObject.CheckState = System.Windows.Forms.CheckState.Checked;
			this.btnSelectObject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnSelectObject.Image = global::SNFDesigner.Properties.Resources.Select;
			this.btnSelectObject.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnSelectObject.Name = "btnSelectObject";
			this.btnSelectObject.Size = new System.Drawing.Size(36, 36);
			this.btnSelectObject.Text = "select object";
			this.btnSelectObject.ToolTipText = "오브젝트 선택 (1)";
			this.btnSelectObject.Click += new System.EventHandler(this.btnSelectObject_Click);
			// 
			// btnTranslateObject
			// 
			this.btnTranslateObject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnTranslateObject.Image = global::SNFDesigner.Properties.Resources.Translate;
			this.btnTranslateObject.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnTranslateObject.Name = "btnTranslateObject";
			this.btnTranslateObject.Size = new System.Drawing.Size(36, 36);
			this.btnTranslateObject.Text = "translate object";
			this.btnTranslateObject.ToolTipText = "오브젝트 이동 (2)";
			this.btnTranslateObject.Click += new System.EventHandler(this.btnTranslateObject_Click);
			// 
			// btnRotateObject
			// 
			this.btnRotateObject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnRotateObject.Image = global::SNFDesigner.Properties.Resources.Rotate;
			this.btnRotateObject.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnRotateObject.Name = "btnRotateObject";
			this.btnRotateObject.Size = new System.Drawing.Size(36, 36);
			this.btnRotateObject.Text = "rotate object";
			this.btnRotateObject.ToolTipText = "오브젝트 회전 (3)";
			this.btnRotateObject.Click += new System.EventHandler(this.btnRotateObject_Click);
			// 
			// btnScaleObject
			// 
			this.btnScaleObject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnScaleObject.Image = global::SNFDesigner.Properties.Resources.Scale;
			this.btnScaleObject.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnScaleObject.Name = "btnScaleObject";
			this.btnScaleObject.Size = new System.Drawing.Size(36, 36);
			this.btnScaleObject.Text = "scale object";
			this.btnScaleObject.ToolTipText = "오브젝트 크기 조절 (4)";
			this.btnScaleObject.Click += new System.EventHandler(this.btnScaleObject_Click);
			// 
			// btnRemoveObject
			// 
			this.btnRemoveObject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnRemoveObject.Image = global::SNFDesigner.Properties.Resources.Remove;
			this.btnRemoveObject.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnRemoveObject.Name = "btnRemoveObject";
			this.btnRemoveObject.Size = new System.Drawing.Size(36, 36);
			this.btnRemoveObject.Text = "toolStripButton1";
			this.btnRemoveObject.ToolTipText = "오브젝트 제거(Delete)";
			this.btnRemoveObject.Click += new System.EventHandler(this.btnRemoveObject_Click);
			// 
			// btnCreateRegion
			// 
			this.btnCreateRegion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnCreateRegion.Image = global::SNFDesigner.Properties.Resources.Region;
			this.btnCreateRegion.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnCreateRegion.Name = "btnCreateRegion";
			this.btnCreateRegion.Size = new System.Drawing.Size(36, 36);
			this.btnCreateRegion.Text = "toolStripButton1";
			this.btnCreateRegion.ToolTipText = "영역 생성 (Ctrl + R)";
			this.btnCreateRegion.Click += new System.EventHandler(this.btnCreateRegion_Click);
			// 
			// btnShowSightRegion
			// 
			this.btnShowSightRegion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnShowSightRegion.Image = global::SNFDesigner.Properties.Resources.ClosedEye;
			this.btnShowSightRegion.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnShowSightRegion.Name = "btnShowSightRegion";
			this.btnShowSightRegion.Size = new System.Drawing.Size(36, 36);
			this.btnShowSightRegion.Text = "toolStripButton1";
			this.btnShowSightRegion.ToolTipText = "시야 및 영역 보기 (F6)";
			this.btnShowSightRegion.Click += new System.EventHandler(this.btnShowSightRegion_Click);
			// 
			// btnPlay
			// 
			this.btnPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnPlay.Image = global::SNFDesigner.Properties.Resources.Play;
			this.btnPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnPlay.Name = "btnPlay";
			this.btnPlay.Size = new System.Drawing.Size(36, 36);
			this.btnPlay.Text = "toolStripButton1";
			this.btnPlay.ToolTipText = "재생 (F5)";
			this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
			// 
			// btnStop
			// 
			this.btnStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnStop.Image = global::SNFDesigner.Properties.Resources.Stop;
			this.btnStop.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnStop.Name = "btnStop";
			this.btnStop.Size = new System.Drawing.Size(36, 36);
			this.btnStop.Text = "toolStripButton1";
			this.btnStop.ToolTipText = "중지 (F5)";
			this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Gainsboro;
			this.ClientSize = new System.Drawing.Size(1008, 730);
			this.Controls.Add(this.dockPanel);
			this.Controls.Add(this.statusStrip);
			this.Controls.Add(this.toolStrip);
			this.Controls.Add(this.menuStrip);
			this.DoubleBuffered = true;
			this.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.MainMenuStrip = this.menuStrip;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Scene & FSM Designer";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyUp);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
			this.menuStrip.ResumeLayout(false);
			this.menuStrip.PerformLayout();
			this.toolStrip.ResumeLayout(false);
			this.toolStrip.PerformLayout();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.ToolStripButton btnSaveProject;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnScaleObject;
        private System.Windows.Forms.ToolStripButton btnRotateObject;
        private System.Windows.Forms.ToolStripButton btnTranslateObject;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnSelectObject;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveTheSceneToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
		private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel;
		private System.Windows.Forms.ToolStripButton btnPlay;
		private System.Windows.Forms.ToolStripButton btnCreateRegion;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripButton btnStop;
		private System.Windows.Forms.ToolStripButton btnRemoveObject;
		private System.Windows.Forms.ToolStripButton btnShowSightRegion;


    }
}
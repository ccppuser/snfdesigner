﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Mogre;

namespace SNFDesigner
{
	public class SNFDocument
	{
		private static SNFDocument mThis = null;
		private const string strProjectInfoFileName = "ProjectInfo.snfobj";
		private string strProjectPath = "";
		private string strFullProjectInfoFileName = "";
		private bool bNewFile = true;
		private bool bModified = false;
		private List<string> lstMeshName = new List<string>();	// 메쉬 리스트
		private Dictionary<string, Object> dicObject = new Dictionary<string, Object>();    // 키: 오브젝트 이름, 값: 오브젝트 이름에 해당하는 오브젝트 정보 리스트
		private Dictionary<string, FSM> dicFSM = new Dictionary<string, FSM>();	// 키: FSM 이름, 값: FSM 이름에 해당하는 FSM 정보
		private Dictionary<string, Region> dicRegion = new Dictionary<string, Region>();

		public static string ProjectInfoFileName { get { return strProjectInfoFileName; } }
		public string ProjectPath { get { return strProjectPath; } }
		public bool IsNewFile { get { return bNewFile; } }
		public bool IsModified { get { return bModified; } }

		// TODO: 수정되었을 때의 이벤트 핸들러 구현
		//public event EventHandler Modified = null;

		public static SNFDocument This
		{
			get
			{
				if (mThis == null)
				{
					mThis = new SNFDocument();
				}

				return mThis;
			}
		}

		/// <summary>
		/// 생성자(새로 만들기 용도)
		/// </summary>
		private SNFDocument()
		{
			this.Initialize("");
		}

		private void Initialize(string projectPath)
		{
			this.bNewFile = true;
			this.strProjectPath = projectPath;
			this.strFullProjectInfoFileName = strProjectPath + '/' + strProjectInfoFileName;
			this.lstMeshName.Clear();
			this.dicObject.Clear();
			this.dicFSM.Clear();
			this.dicRegion.Clear();
		}

		public Object GetObject(string objectName)
		{
			if (dicObject.ContainsKey(objectName))
			{
				return dicObject[objectName];
			}
			else
			{
				return null;
			}
		}

		public Dictionary<string, Object>.Enumerator GetObjectEnumerator()
		{
			return dicObject.GetEnumerator();
		}

		public FSM GetFSM(string fsmName)
		{
			if (dicFSM.ContainsKey(fsmName))
			{
				return dicFSM[fsmName];
			}
			else
			{
				return null;
			}
		}

		public string[] GetFSMNames()
		{
			return dicFSM.Keys.ToList().ToArray();
		}

		public Region GetRegion(string regionName)
		{
			if (dicRegion.ContainsKey(regionName))
			{
				return dicRegion[regionName];
			}
			else
			{
				return null;
			}
		}

		public Dictionary<string, Region>.Enumerator GetRegionEnumerator()
		{
			return dicRegion.GetEnumerator();
		}

		public void New(string projectPath)
		{
			this.Initialize(projectPath);

			// 프로젝트 폴더 내의 모든 메쉬 이름을 읽음
			this.LoadMeshListInProjectFolder();

			// 오브젝트 스토리지에 메쉬 목록 추가
			this.AddAllMeshNameToObjectStorage();

			// FSM 스토리지에 FSM 목록 추가
			this.AddAllFSMToFSMStorage();
		}

		public bool Open(string projectPath)
		{
			this.Initialize(projectPath);

			if (!this.ParseProjectInfoFile(projectPath))
			{
				return false;
			}

			// 프로젝트 정보에 있는 모든 메쉬가 프로젝트 폴더 내에 존재하는지 검사
			// 또한 프로젝트에 없는 메쉬가 프로젝트 폴더 내에 더 있으면, 그것들도 메쉬 정보에 포함시킨다.
			if (!this.LoadMeshListInProjectFolder())
			{
				MessageBox.Show("씬에 배치된 메쉬 중 프로젝트 폴더에 존재하지 않는 메쉬가 있습니다.\n씬에 배치된 해당 메쉬들은 렌더링할 수 없습니다.", "경고", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}

			// 오브젝트 스토리지에 메쉬 목록 추가
			this.AddAllMeshNameToObjectStorage();

			// 오브젝트 리스트를 씬에 추가
			this.AddAllObjectToScene();

			// FSM 스토리지에 FSM 목록 추가
			this.AddAllFSMToFSMStorage();

			return true;
		}

		private void ParseString(BinaryReader reader, out string result)
		{
			int length = reader.ReadInt32();
			result = new string(reader.ReadChars(length));
		}

		private bool ParseStringCond(BinaryReader reader, out string result)
		{
			if (reader.ReadBoolean())
			{
				ParseString(reader, out result);
				return true;
			}
			else
			{
				result = "";
				return false;
			}
		}

		private bool ParseProjectInfoFile(string projectPath)
		{
			// SNF 파일로부터 오브젝트 정보 추출
			FileStream fileStream = new FileStream(strFullProjectInfoFileName, FileMode.Open, FileAccess.Read);
			BinaryReader reader = new BinaryReader(fileStream, Encoding.Default);
			if (reader == null)
				return false;

			try
			{
				// 헤더 검사
				if (!ParseHeader(reader))
					return false;

				// 메쉬 리스트 파싱
				ParseMeshList(reader);

				// 오브젝트 리스트 파싱
				ParseObjectList(reader);

				// 영역 리스트 파싱
				ParseRegionList(reader);

				// FSM 리스트 파싱
				ParseFSMList(reader);
			}
			catch (Exception e)
			{
				reader.Close();
				MessageBox.Show("프로젝트 정보를 불러올 수 없습니다.\nMessage:\n" + e.Message + "\nSource:\n" + e.Source + "\nTarget Site:\n" + e.TargetSite + "\nStack Trace:\n" + e.StackTrace, "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);

				return false;
			}

			reader.Close();
			fileStream.Close();

			return true;
		}

		private bool ParseHeader(BinaryReader reader)
		{
			try
			{
				string header = new string(reader.ReadChars(8));  // <snfobj> 파싱
				if (header != "<snfobj>")  // 헤더가 <snfobj> 인지 검사
					return false;
			}
			catch (Exception)
			{
				throw;
			}

			return true;
		}

		private void ParseMeshList(BinaryReader reader)
		{
			try
			{
				int meshNameCount = reader.ReadInt32();	// 총 메쉬 이름 개수 파싱

				for (int i = 0; i < meshNameCount; ++i)
				{
					string meshName;
					ParseString(reader, out meshName);

					lstMeshName.Add(meshName);
				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		private void ParseObjectList(BinaryReader reader)
		{
			try
			{
				int objectInfoCount = reader.ReadInt32();	// 총 오브젝트 정보 개수 파싱

				for (int i = 0; i < objectInfoCount; ++i)
				{
					string objectName;
					ParseString(reader, out objectName);    // 오브젝트 이름 파싱

					string meshName;
					ParseString(reader, out meshName);  // 메쉬 이름 파싱

					ObjectType type = (ObjectType)reader.ReadInt32();	// 오브젝트 종류 파싱

					Vector3 position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());	// 오브젝트 위치 파싱

					Quaternion rotation = new Quaternion(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());	// 오브젝트 회전 각도 파싱

					float scale = reader.ReadSingle();	// 오브젝트 스케일 비율 파싱

					bool hasSight = reader.ReadBoolean();	// 시야 여부 파싱

					Vector3 sightPosition = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());	// 시야 위치 파싱

					Quaternion sightRotation = new Quaternion(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());	// 시야 회전 각도 파싱

					float sightRange = reader.ReadSingle();	// 시야 거리 파싱

					float sightWidth = reader.ReadSingle();

					float sightHeight = reader.ReadSingle();

					bool player = reader.ReadBoolean();

					OgreManager.This.AddObjectFromMeshName(objectName, meshName);

					Object newObj = new Object(
						objectName,
						meshName,
						type,
						position,
						rotation,
						scale,
						hasSight,
						sightPosition,
						sightRotation,
						sightRange,
						sightWidth,
						sightHeight,
						player
					);

					if (player)
					{
						FSMPlayer.This.SetPlayerObject(newObj);
					}

					dicObject.Add(objectName, newObj);
				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		private void ParseRegionList(BinaryReader reader)
		{
			try
			{
				int regionCount = reader.ReadInt32();

				for (int i = 0; i < regionCount; ++i)
				{
					string name;
					ParseString(reader, out name);

					float diameter = reader.ReadSingle();

					Vector3 position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

					Quaternion orientation = new Quaternion(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

					Entity region = OgreManager.This.SceneManager.CreateEntity("region.mesh");
					region.QueryFlags = QueryFlags.REGION;
					SceneNode regionNode = OgreManager.This.RootSceneNode.CreateChildSceneNode();
					regionNode.AttachObject(region);
					regionNode.Position = position;
					regionNode.Orientation = orientation;
					regionNode.SetScale(diameter, diameter, diameter);

					AddRegion(new Region(regionNode));
				}
			}
			catch
			{
				throw;
			}
		}

		// 파싱용 임시
		private Dictionary<FSMChangeStateEvent, string> mTargetStateDic = new Dictionary<FSMChangeStateEvent, string>();

		private void ParseFSMList(BinaryReader reader)
		{
			try
			{
				mTargetStateDic.Clear();

				int fsmCount = reader.ReadInt32();

				for (int i = 0; i < fsmCount; ++i)
				{
					string fsmName;
					ParseString(reader, out fsmName);

					FSM fsm = new FSM(fsmName);
					AddFSM(fsm);

					int stateCount = reader.ReadInt32();

					for (int j = 0; j < stateCount; ++j)
					{
						FSMState state = new FSMState(fsm);
						fsm.AddState(state);

						string stateName;
						ParseString(reader, out stateName);
						state.Name = stateName;

						string lookAtObject;
						if (ParseStringCond(reader, out lookAtObject))
						{
							state.LookAtObject = GetObject(lookAtObject);
						}

						string animationName;
						if (ParseStringCond(reader, out animationName))
						{
							state.AnimationName = animationName;
						}

						state.AnimationPlayCount = reader.ReadInt32();

						state.CenterPosition = new System.Drawing.Point(reader.ReadInt32(), reader.ReadInt32());

						state.CircleRadius = reader.ReadSingle();

						state.TextSize = new System.Drawing.SizeF(reader.ReadSingle(), reader.ReadSingle());

						int triggerCount = reader.ReadInt32();

						for (int k = 0; k < triggerCount; ++k)
						{
							FSMEventTrigger trigger = null;

							FSMEventTriggerType triggerType = (FSMEventTriggerType)reader.ReadInt32();

							string temp;

							switch (triggerType)
							{
								case FSMEventTriggerType.SightEnter:
									FSMSightEnterTrigger sightEnter = (FSMSightEnterTrigger)state.AddEventTrigger(FSMEventTriggerType.SightEnter);
									trigger = sightEnter;
									if (ParseStringCond(reader, out temp))
									{
										sightEnter.TargetObject = GetObject(temp);
									}
									break;
								case FSMEventTriggerType.SightExit:
									FSMSightExitTrigger sightExit = (FSMSightExitTrigger)state.AddEventTrigger(FSMEventTriggerType.SightExit);
									trigger = sightExit;
									if (ParseStringCond(reader, out temp))
									{
										sightExit.TargetObject = GetObject(temp);
									}
									break;
								case FSMEventTriggerType.ZoneEnter:
									FSMZoneEnterTrigger zoneEnter = (FSMZoneEnterTrigger)state.AddEventTrigger(FSMEventTriggerType.ZoneEnter);
									trigger = zoneEnter;
									if (ParseStringCond(reader, out temp))
									{
										zoneEnter.TargetObject = GetObject(temp);
									}
									if (ParseStringCond(reader, out temp))
									{
										zoneEnter.TargetRegion = GetRegion(temp);
									}
									break;
								case FSMEventTriggerType.ZoneExit:
									FSMZoneExitTrigger zoneExit = (FSMZoneExitTrigger)state.AddEventTrigger(FSMEventTriggerType.ZoneExit);
									trigger = zoneExit;
									if (ParseStringCond(reader, out temp))
									{
										zoneExit.TargetObject = GetObject(temp);
									}
									if (ParseStringCond(reader, out temp))
									{
										zoneExit.TargetRegion = GetRegion(temp);
									}
									break;
								case FSMEventTriggerType.ElapsedTime:
									FSMElapsedTimeTrigger elapsedTime = (FSMElapsedTimeTrigger)state.AddEventTrigger(FSMEventTriggerType.ElapsedTime);
									trigger = elapsedTime;
									elapsedTime.TargetElapsedTime = reader.ReadSingle();
									break;
								case FSMEventTriggerType.AnimationEnded:
									FSMAnimationEndedTrigger animationEnded = (FSMAnimationEndedTrigger)state.AddEventTrigger(FSMEventTriggerType.AnimationEnded);
									trigger = animationEnded;
									break;
								case FSMEventTriggerType.AnimationRepeated:
									FSMAnimationRepeatedTrigger animationRepeated = (FSMAnimationRepeatedTrigger)state.AddEventTrigger(FSMEventTriggerType.AnimationRepeated);
									trigger = animationRepeated;
									animationRepeated.TargetRepeatedNumber = reader.ReadInt32();
									break;
							}

							int eventCount = reader.ReadInt32();

							for (int l = 0; l < eventCount; ++l)
							{
								FSMEventType eventType = (FSMEventType)reader.ReadInt32();

								switch (eventType)
								{
									case FSMEventType.ChangeState:
										FSMChangeStateEvent changeState = (FSMChangeStateEvent)state.AddEvent(trigger, eventType);
										if (ParseStringCond(reader, out temp))
										{
											mTargetStateDic.Add(changeState, temp);
										}
										break;
									case FSMEventType.MoveObject:
										FSMMoveObjectEvent moveObject = (FSMMoveObjectEvent)state.AddEvent(trigger, eventType);
										moveObject.TargetPoint = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
										moveObject.Speed = reader.ReadSingle();
										break;
									case FSMEventType.RemoveObject:
										FSMRemoveObjectEvent removeObject = (FSMRemoveObjectEvent)state.AddEvent(trigger, eventType);
										break;
								}
							}
						}
					}

					foreach (var pair in mTargetStateDic)
					{
						pair.Key.TargetState = fsm.GetState(pair.Value);
					}
					mTargetStateDic.Clear();

					int transitionCount = reader.ReadInt32();

					for (int j = 0; j < transitionCount; ++j)
					{
						string fromStateName, toStateName;
						ParseString(reader, out fromStateName);
						ParseString(reader, out toStateName);
						fsm.AddTransition(fsm.GetState(fromStateName), fsm.GetState(toStateName));
					}

					string initialStateName;
					if (ParseStringCond(reader, out initialStateName))
					{
						fsm.InitialState = fsm.GetState(initialStateName);
					}

					string assignedObjectName;
					if (ParseStringCond(reader, out assignedObjectName))
					{
						fsm.AssignedObject = GetObject(assignedObjectName);
					}
				}
			}
			catch
			{
				throw;
			}
		}

		private void WriteString(BinaryWriter writer, string value)
		{
			writer.Write(value.Length);
			writer.Write(value.ToCharArray());
		}

		//private void WriteStringCond(BinaryWriter writer, bool condition, string value)
		//{
		//    if (!condition)
		//    {
		//        writer.Write(false);
		//    }
		//    else
		//    {
		//        writer.Write(true);
		//        WriteString(writer, value);
		//    }
		//}

		private void WriteVector3(BinaryWriter writer, Vector3 value)
		{
			writer.Write(value.x);
			writer.Write(value.y);
			writer.Write(value.z);
		}

		private void WriteQuaternion(BinaryWriter writer, Quaternion value)
		{
			writer.Write(value.w);
			writer.Write(value.x);
			writer.Write(value.y);
			writer.Write(value.z);
		}

		public bool Save()
		{
			// 메쉬 및 오브젝트 정보 등을 파일에 저장한다.
			FileStream fileStream = new FileStream(strFullProjectInfoFileName, FileMode.Create, FileAccess.Write);
			BinaryWriter writer = new BinaryWriter(fileStream, Encoding.Default);
			if (writer == null)
				return false;

			// snf 포맷 파일임을 명시
			writer.Write(new char[] { '<', 's', 'n', 'f', 'o', 'b', 'j', '>' });

			// 메쉬 리스트 쓰기
			WriteMeshList(writer);

			// 오브젝트 리스트 쓰기
			WriteObjectList(writer);

			// 영역 리스트 쓰기
			WriteRegionList(writer);

			// FSM 리스트 쓰기
			WriteFSMList(writer);

			// snf 포맷 파일의 끝을 명시
			writer.Write(new char[] { '\n', '<', '/', 's', 'n', 'f', 'o', 'b', 'j', '>' });

			// 파일을 저장하고 닫기
			writer.Close();
			fileStream.Close();

			bNewFile = false;
			bModified = false;

			return true;
		}

		private void WriteMeshList(BinaryWriter writer)
		{
			writer.Write(lstMeshName.Count);	// 총 메쉬 개수 쓰기

			foreach (string meshName in lstMeshName)	// 모든 메쉬 이름 쓰기
			{
				WriteString(writer, meshName);
			}
		}

		private void WriteObjectList(BinaryWriter writer)
		{
			writer.Write(dicObject.Count);	// 총 오브젝트 정보 개수 쓰기

			foreach (KeyValuePair<string, Object> pair in dicObject)	// 모든 오브젝트 정보 쓰기
			{
				WriteString(writer, pair.Key);	// 오브젝트 이름 쓰기

				WriteString(writer, pair.Value.MeshName);	// 메쉬 이름 쓰기

				writer.Write((int)pair.Value.ObjectType);	// 오브젝트 종류 쓰기

				WriteVector3(writer, pair.Value.ObjectPosition);	// 오브젝트 위치 쓰기

				WriteQuaternion(writer, pair.Value.ObjectRotation);	// 오브젝트 회전 각도 쓰기

				writer.Write(pair.Value.ObjectScale); // 오브젝트 스케일 비율 쓰기

				writer.Write(pair.Value.HasSight);    // 시야 여부 쓰기

				WriteVector3(writer, pair.Value.SightPosition);	// 시야 위치 쓰기

				WriteQuaternion(writer, pair.Value.SightRotation);	// 시야 회전 각도 쓰기

				writer.Write(pair.Value.SightRange);  // 시야 거리 쓰기

				writer.Write(pair.Value.SightWidth);  // 시야 가로 크기 쓰기

				writer.Write(pair.Value.SightHeight); // 시야 세로 크기 쓰기

				writer.Write(pair.Value.Player);	// 플레이어 여부
			}
		}

		private void WriteRegionList(BinaryWriter writer)
		{
			writer.Write(dicRegion.Count);	// 총 영역 개수 쓰기

			foreach (var pair in dicRegion)
			{
				WriteString(writer, pair.Key);	// 영역 이름 쓰기

				writer.Write(pair.Value.Diameter);	// 영역 지름 쓰기

				WriteVector3(writer, pair.Value.Position);	// 영역 위치 쓰기

				WriteQuaternion(writer, pair.Value.Orientation);	// 영역 방향 쓰기
			}
		}

		private void WriteFSMList(BinaryWriter writer)
		{
			writer.Write(dicFSM.Count);	// 총 FSM 개수 쓰기

			foreach (var pair in dicFSM)	// 모든 FSM 쓰기
			{
				WriteString(writer, pair.Key);

				writer.Write(pair.Value.StateCount);	// 상태 개수 쓰기

				for (int i = 0; i < pair.Value.StateCount; ++i)	// 모든 상태 쓰기
				{
					FSMState state = pair.Value.GetState(i);

					WriteString(writer, state.Name);

					if (state.LookAtObject == null) { writer.Write(false); }
					else { writer.Write(true); WriteString(writer, state.LookAtObject.ObjectName); }

					if (state.AnimationName == "") { writer.Write(false); }
					else { writer.Write(true); WriteString(writer, state.AnimationName); }

					writer.Write(state.AnimationPlayCount);

					writer.Write(state.CenterPosition.X);
					writer.Write(state.CenterPosition.Y);

					writer.Write(state.CircleRadius);

					writer.Write(state.TextSize.Width);
					writer.Write(state.TextSize.Height);

					writer.Write(state.EventTriggerCount);	// 이벤트 발동 조건 개수 쓰기

					for (int j = 0; j < state.EventTriggerCount; ++j)	// 모든 이벤트 발동 조건 쓰기
					{
						FSMEventTrigger t = state.GetEventTrigger(j);

						writer.Write((int)t.TriggerType);

						switch(t.TriggerType)
						{
							case FSMEventTriggerType.SightEnter:
								FSMSightEnterTrigger sightEnter = (FSMSightEnterTrigger)t;
								if (sightEnter.TargetObject == null) { writer.Write(false); }
								else { writer.Write(true); WriteString(writer, sightEnter.TargetObject.ObjectName); }
								break;
							case FSMEventTriggerType.SightExit:
								FSMSightExitTrigger sightExit = (FSMSightExitTrigger)t;
								if (sightExit.TargetObject == null) { writer.Write(false); }
								else { writer.Write(true); WriteString(writer, sightExit.TargetObject.ObjectName); }
								break;
							case FSMEventTriggerType.ZoneEnter:
								FSMZoneEnterTrigger zoneEnter = (FSMZoneEnterTrigger)t;
								if (zoneEnter.TargetObject == null) { writer.Write(false); }
								else { writer.Write(true); WriteString(writer, zoneEnter.TargetObject.ObjectName); }
								if (zoneEnter.TargetRegion == null) { writer.Write(false); }
								else { writer.Write(true); WriteString(writer, zoneEnter.TargetRegion.Name); }
								break;
							case FSMEventTriggerType.ZoneExit:
								FSMZoneEnterTrigger zoneExit = (FSMZoneEnterTrigger)t;
								if (zoneExit.TargetObject == null) { writer.Write(false); }
								else { writer.Write(true); WriteString(writer, zoneExit.TargetObject.ObjectName); }
								if (zoneExit.TargetRegion == null) { writer.Write(false); }
								else { writer.Write(true); WriteString(writer, zoneExit.TargetRegion.Name); }
								break;
							case FSMEventTriggerType.ElapsedTime:
								FSMElapsedTimeTrigger elapsedTime = (FSMElapsedTimeTrigger)t;
								writer.Write(elapsedTime.TargetElapsedTime);
								break;
							case FSMEventTriggerType.AnimationEnded:
								//FSMAnimationEndedTrigger animationEnded = (FSMAnimationEndedTrigger)t;
								break;
							case FSMEventTriggerType.AnimationRepeated:
								FSMAnimationRepeatedTrigger animationRepeated = (FSMAnimationRepeatedTrigger)t;
								writer.Write(animationRepeated.TargetRepeatedNumber);
								break;
						}

						writer.Write(state.GetEventCount(t));	// 모든 이벤트 개수 쓰기

						for (int k = 0; k < state.GetEventCount(t); ++k)	// 모든 이벤트 쓰기
						{
							FSMEvent e = state.GetEvent(t, k);

							writer.Write((int)e.EventType);

							switch (e.EventType)
							{
								case FSMEventType.ChangeState:
									FSMChangeStateEvent changeState = (FSMChangeStateEvent)e;
									if (changeState.TargetState == null) { writer.Write(false); }
									else { writer.Write(true); WriteString(writer, changeState.TargetState.Name); }
									break;
								case FSMEventType.MoveObject:
									FSMMoveObjectEvent moveObject = (FSMMoveObjectEvent)e;
									WriteVector3(writer, moveObject.TargetPoint);
									writer.Write(moveObject.Speed);
									break;
								case FSMEventType.RemoveObject:
									//FSMRemoveObjectEvent removeObject = (FSMRemoveObjectEvent)e;
									break;
							}
						}
					}
				}

				writer.Write(pair.Value.TransitionCount);	// 전환 개수 쓰기

				for (int i = 0; i < pair.Value.TransitionCount; ++i)	// 모든 전환 쓰기
				{
					FSMStateTransition transition = pair.Value.GetTransition(i);

					WriteString(writer, transition.FromState.Name);

					WriteString(writer, transition.ToState.Name);
				}

				if (pair.Value.InitialState == null) { writer.Write(false); }	// 최초 상태 이름 쓰기
				else { writer.Write(true); WriteString(writer, pair.Value.InitialState.Name); }

				if (pair.Value.AssignedObject == null) { writer.Write(false); }	// 할당된 오브젝트 이름 쓰기
				else { writer.Write(true); WriteString(writer, pair.Value.AssignedObject.ObjectName); }
			}
		}

		//public bool SaveAs(string fileName)
		//{
		//    strProjectFileName = fileName;
		//    return this.Save();
		//}

		/// <summary>
		/// 현재 문서가 수정되었음을 표시
		/// </summary>
		public void SetModified()
		{
			bModified = true;
		}

		/// <summary>
		/// 기존의 문서가 수정되었는지 여부를 확인하고 수정되었을 경우 저장
		/// </summary>
		/// <returns>문서가 수정되지 않았을 경우 바로true리턴하며, 수정되었을 경우 Yes/No를 눌렀을 경우에 true, Cancel을 눌렀을 경우 false리턴</returns>
		public bool ConfirmModifiedAndSave(SaveFileDialog saveFileDialog)
		{
			// 새로 만들기 전에 기존 문서의 수정 여부 확인
			if (SNFDocument.This.IsModified)
			{
				// 수정되었다면 저장 여부 묻기
				DialogResult result = MessageBox.Show("The project is modified.\n Would you want to save this file?", "Warning", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3);
				// 저장하기로 결정하였다면
				if (result == DialogResult.Yes)
				{
					// 저장
					if (saveFileDialog.ShowDialog() == DialogResult.OK)
					{
						SNFDocument.This.Save();
					}
					else
					{
						return false;
					}
				}
				else if (result == DialogResult.Cancel)
				{
					return false;
				}
			}

			return true;
		}

		public void AddObject(string meshName, string objectName, Vector3 position)
		{
			if (!dicObject.ContainsKey(objectName))
			{
				dicObject.Add(objectName, new Object(objectName, meshName, position));
			}
		}

		//public void ModifyObject(SceneNode node)
		//{
		//    Object o = this.dicObject[node.Name];
		//    //o.Set(node.Position,
		//}

		public void RemoveObject(string objectName)
		{
			// FSM에서 해당 오브젝트를 속성으로 갖고 있는 모든 속성을 null로 바꿈
			string[] fsmNames = SNFDocument.This.GetFSMNames();
			foreach (var fsmName in fsmNames)
			{
				FSM fsm = SNFDocument.This.GetFSM(fsmName);
				if (fsm.AssignedObject != null && fsm.AssignedObject.ObjectName == objectName)
				{
					fsm.AssignedObject = null;
				}

				for (int i = 0; i < fsm.StateCount; ++i)
				{
					FSMState state = fsm.GetState(i);

					for (int j = 0; j < state.EventTriggerCount; ++j)
					{
						FSMEventTrigger trigger = state.GetEventTrigger(j);

						switch (trigger.TriggerType)
						{
							case FSMEventTriggerType.SightEnter:
								FSMSightEnterTrigger sightEnter = (FSMSightEnterTrigger)trigger;
								if (sightEnter.TargetObject != null && sightEnter.TargetObject.ObjectName == objectName)
								{
									sightEnter.TargetObject = null;
								}
								break;
							case FSMEventTriggerType.SightExit:
								FSMSightExitTrigger sightExit = (FSMSightExitTrigger)trigger;
								if (sightExit.TargetObject != null && sightExit.TargetObject.ObjectName == objectName)
								{
									sightExit.TargetObject = null;
								}
								break;
							case FSMEventTriggerType.ZoneEnter:
								FSMZoneEnterTrigger zoneEnter = (FSMZoneEnterTrigger)trigger;
								if (zoneEnter.TargetObject != null && zoneEnter.TargetObject.ObjectName == objectName)
								{
									zoneEnter.TargetObject = null;
								}
								break;
							case FSMEventTriggerType.ZoneExit:
								FSMZoneExitTrigger zoneExit = (FSMZoneExitTrigger)trigger;
								if (zoneExit.TargetObject != null && zoneExit.TargetObject.ObjectName == objectName)
								{
									zoneExit.TargetObject = null;
								}
								break;
						}

						//for (int k = 0; k < state.GetEventCount(trigger); ++k)
						//{
						//    FSMEvent evt = state.GetEvent(trigger, k);
						//}
					}
				}
			}

			// 제거
			dicObject.Remove(objectName);
		}

		private bool LoadMeshListInProjectFolder()
		{
			// 프로젝트 폴더 내의 모든 메쉬 파일 이름을 가져옴
			string[] existMeshFileNames = Directory.GetFiles(strProjectPath, "*.mesh");
			foreach (string meshFilePath in existMeshFileNames)
			{
				string meshFileName = meshFilePath.Split(new char[] { '\\' }).Last();

				// 프로젝트 정보에 없는 메쉬 파일이 프로젝트 폴더에 있다!
				if (!lstMeshName.Contains(meshFileName))
				{
					// 새로 추가된 메쉬 파일을 프로젝트 정보에 추가
					lstMeshName.Add(meshFileName);
				}
			}

			// 또한 프로젝트 정보에는 있지만 프로젝트 폴더 내엔 존재하지 않는 파일이 있는지 검사한다.
			foreach (string meshName in lstMeshName)
			{
				if (!File.Exists(strProjectPath + '/' + meshName))
				{
					return false;
				}
			}

			return true;
		}

		private void AddAllMeshNameToObjectStorage()
		{
			foreach (string name in lstMeshName)
			{
				ObjectStorageControl.This.ImportObject(name);
			}

			ObjectStorageControl.This.Refresh();
		}

		private void AddAllObjectToScene()
		{
			foreach (var pair in dicObject)
			{
				SceneNode objectNode = OgreManager.This.RootSceneNode.GetChild(pair.Value.ObjectName) as SceneNode;
				SceneNode sightNode = OgreManager.This.GetSightNode(objectNode);

				// 배치 정보 적용
				objectNode.Position = pair.Value.ObjectPosition;
				objectNode.Orientation = pair.Value.ObjectRotation;
				objectNode.SetScale(pair.Value.ObjectScale, pair.Value.ObjectScale, pair.Value.ObjectScale);

				if (pair.Value.HasSight)
				{
					sightNode.Position = pair.Value.SightPosition;
					sightNode.Orientation = pair.Value.SightRotation;
					sightNode.SetScale(pair.Value.SightWidth, pair.Value.SightHeight, pair.Value.SightRange);

					pair.Value.UpdateSightCamera();
				}
			}
		}

		public void AddFSM(FSM fsm)
		{
			if (!dicFSM.ContainsKey(fsm.Name))
			{
				dicFSM.Add(fsm.Name, fsm);
			}
		}

		//public void ModifyFSM(FSM fsm)
		//{
		//throw new NotImplementedException();
		//}

		//public void RemoveFSM(string fsmName)
		//{
		//    dicFSM.Remove(fsmName);
		//}

		private void AddAllFSMToFSMStorage()
		{
			foreach (KeyValuePair<string, FSM> pair in dicFSM)
			{
				FSMStorageControl.This.AddFSMName(pair.Key);
			}

			FSMStorageControl.This.Refresh();
		}

		public void AddRegion(Region region)
		{
			if (!dicRegion.ContainsKey(region.Name))
			{
				dicRegion.Add(region.Name, region);
			}
		}

		//public void ModifyRegion(Region region)
		//{
		//throw new NotImplementedException();
		//}

		//public void RemoveRegion(string regionName)
		//{
		//    dicRegion.Remove(regionName);
		//}

		//public void AcceptChanges(OgreWindow ogreWindow)
		//{
		//    SceneNode rootSceneNode = ogreWindow.GetRootSceneNode();
		//    // 모든 씬 노드를 순회
		//    Node.ChildNodeIterator itrNode = rootSceneNode.GetChildIterator();
		//    while (itrNode.MoveNext())
		//    {
		//        string objectName = itrNode.Current.Name;   // 씬 노드의 이름이 곧 오브젝트의 이름이므로
		//        // 해당 objectName이 오브젝트 리스트에 해당하는 이름이 맞을 경우
		//        if (this.dicObject.ContainsKey(objectName))
		//        {
		//            this.ModifyObject((SceneNode)itrNode.Current);
		//        }
		//    }
		//}
	}
}

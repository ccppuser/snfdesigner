﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SNFDesigner
{
	public partial class NewOrOpenDlg : Form
	{
		public string ExistProjectPath { get { return txtSelectedExistPath.Text; } }

		public string NewProjectPath { get { return txtSelectedNewPath.Text; } }

		public int Result { get { return nResult; } }

		private int nResult = 0;

		public NewOrOpenDlg()
		{
			InitializeComponent();
		}

		private void NewOrOpenDlg_Load(object sender, EventArgs e)
		{
			// 기존 프로젝트 열기 / 새 프로젝트 시작 버튼 일단 비활성화
			btnOpenExistProject.Enabled = false;
			btnCreateNewProject.Enabled = false;
		}

		private void btnSelectExistPath_Click(object sender, EventArgs e)
		{
			// 불러올 프로젝트 폴더 입력받음
			FolderBrowserDialog folderDialog = new FolderBrowserDialog();
			folderDialog.ShowNewFolderButton = false;
			if (folderDialog.ShowDialog() != DialogResult.Cancel)
			{
				txtSelectedExistPath.Text = folderDialog.SelectedPath;
			}
		}

		private void btnSelectNewPath_Click(object sender, EventArgs e)
		{
			// 새 프로젝트 폴더 입력받음
			FolderBrowserDialog folderDialog = new FolderBrowserDialog();
			folderDialog.ShowNewFolderButton = true;
			if (folderDialog.ShowDialog() != DialogResult.Cancel)
			{
				txtSelectedNewPath.Text = folderDialog.SelectedPath;
			}
		}

		private void txtSelectedExistPath_TextChanged(object sender, EventArgs e)
		{
			// 유효하지 않은 폴더 입력시 프로젝트 열기 버튼 비활성화
			if (txtSelectedExistPath.Text.Length == 0 ||
				!System.IO.Directory.Exists(txtSelectedExistPath.Text) ||
				!System.IO.File.Exists(txtSelectedExistPath.Text + '/' + SNFDocument.ProjectInfoFileName))
			{
				btnOpenExistProject.Enabled = false;
			}
			else
			{
				btnOpenExistProject.Enabled = true;
			}
		}

		private void txtSelectedNewPath_TextChanged(object sender, EventArgs e)
		{
			// 유효하지 않은 폴더 입력시 프로젝트 시작 버튼 비활성화
			if (txtSelectedNewPath.Text.Length == 0 ||
				!System.IO.Directory.Exists(txtSelectedNewPath.Text))
			{
				btnCreateNewProject.Enabled = false;
			}
			else
			{
				btnCreateNewProject.Enabled = true;
			}
		}

		private void btnOpenExistProject_Click(object sender, EventArgs e)
		{
			nResult = 1;
			Close();
		}

		private void btnCreateNewProject_Click(object sender, EventArgs e)
		{
			nResult = 2;
			Close();
		}
	}
}

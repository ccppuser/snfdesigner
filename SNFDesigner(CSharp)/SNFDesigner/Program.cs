﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SNFDesigner
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

			try
			{
				// 메인 폼 띄우기
				MainForm mainForm = new MainForm();
				mainForm.Show();
				while (!mainForm.Created) { }

				if (!mainForm.Initialize())
					return;

				// 새로운 프로젝트를 시작할 건지, 기존 snf파일을 로드할 건지 선택하는 대화상자 띄움(Unity3d 스타일)
				NewOrOpenDlg introDlg = new NewOrOpenDlg();
				introDlg.ShowDialog();
				// 사용자가 그냥 닫기 버튼을 눌렀다
				if (introDlg.Result == 0)
				{
					return;	// 종료
				}
				// 사용자가 기존 프로젝트를 열기로 했다.
				else if (introDlg.Result == 1)
				{
					if (!mainForm.OpenProject(introDlg.ExistProjectPath))
					{
						MessageBox.Show("The project information file is damaged or missing.", "Exception", MessageBoxButtons.OK);
						return;
					}
				}
				// 사용자가 새 프로젝트를 시작하기로 했다.
				else if (introDlg.Result == 2)
				{
					mainForm.NewProject(introDlg.NewProjectPath);
				}

				mainForm.Run();
			}
			catch (Exception e)
			{
				Mogre.OgreException oe = Mogre.OgreException.LastException;
				if (oe != null)
				{
					MessageBox.Show(oe.FullDescription, "예외 발생", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}

				MessageBox.Show(e.InnerException + ": " + e.Message + "\n" + e.StackTrace, "예외 발생", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
        }
    }
}

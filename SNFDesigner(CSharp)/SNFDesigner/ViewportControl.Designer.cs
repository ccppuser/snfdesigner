﻿namespace SNFDesigner
{
	partial class ViewportControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// ViewportControl
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 262);
			this.CloseButtonVisible = false;
			this.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Name = "ViewportControl";
			this.Text = "뷰포트";
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ViewportControl_MouseUp);
			this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ViewportControl_MouseClick);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ViewportControl_MouseDown);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.ViewportControl_DragEnter);
			this.QueryContinueDrag += new System.Windows.Forms.QueryContinueDragEventHandler(this.ViewportControl_QueryContinueDrag);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ViewportControl_MouseMove);
			this.DragOver += new System.Windows.Forms.DragEventHandler(this.ViewportControl_DragOver);
			this.Resize += new System.EventHandler(this.ViewportControl_Resize);
			this.ResumeLayout(false);

		}

		#endregion
	}
}
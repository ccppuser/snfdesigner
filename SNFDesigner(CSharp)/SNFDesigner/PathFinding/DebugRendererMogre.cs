﻿using System;
using System.Collections.Generic;
using System.Text;
using Mogre;
using RecastDetourDotNet;

namespace SNFDesigner
{
	class DebugRendererMogre : DebugRenderer
	{
		private static int mInstanceCount = 0;
		private ManualObject mObject = null;
		private string mObjectName;
		private string mMaterialName;
		private bool mDefined = false;

		public bool Defined { get { return mDefined; } set { mDefined = value; } }

		public DebugRendererMogre()
		{
			++mInstanceCount;

			mObject = OgreManager.This.SceneManager.CreateManualObject();
			mObject.QueryFlags = QueryFlags.EXCEPT;
			mObjectName = mObject.Name;

			OgreManager.This.RootSceneNode.AttachObject(mObject);

			mMaterialName = "DebugRendererMaterial_" + mInstanceCount.ToString("00");
			if (!MaterialManager.Singleton.ResourceExists(mMaterialName))
			{
				MaterialPtr material = MaterialManager.Singleton.Create(mMaterialName, "General");
				//material.GetTechnique(0).GetPass(0).SetSceneBlending(SceneBlendFactor.SBF_SOURCE_ALPHA, SceneBlendFactor.SBF_DEST_COLOUR);
				material.GetTechnique(0).GetPass(0).SetDiffuse(0, 1, 0, 0.5f);
				material.GetTechnique(0).GetPass(0).SetAmbient(0, 1, 0);
				material.GetTechnique(0).GetPass(0).SetSelfIllumination(0, 1, 0);

				//material.GetTechnique(0).CreatePass();
				//material.GetTechnique(0).GetPass(1).SetDiffuse(0, 1, 0, 1);
				//material.GetTechnique(0).GetPass(1).SetAmbient(0, 1, 0);
				//material.GetTechnique(0).GetPass(1).SetSelfIllumination(0, 1, 0);
				//material.GetTechnique(0).GetPass(1).PolygonMode = PolygonMode.PM_WIREFRAME;
			}
		}

		~DebugRendererMogre()
		{
		}

		public override void DepthMask(bool state)
		{
			MaterialPtr material = MaterialManager.Singleton.GetByName(mMaterialName);
			material.GetTechnique(0).SetDepthCheckEnabled(state);
			material.GetTechnique(0).SetDepthWriteEnabled(state);
		}

		public override void Texture(bool state)
		{
		}

		public override void Begin(DebugDrawPrimitives prim, float size)
		{
			if (!mDefined)
			{
				MaterialPtr material = MaterialManager.Singleton.GetByName(mMaterialName);
				switch (prim)
				{
					case DebugDrawPrimitives.DU_DRAW_POINTS:
						material.GetTechnique(0).SetPointSize(size);
						mObject.Begin(mMaterialName, RenderOperation.OperationTypes.OT_POINT_LIST);
						break;
					case DebugDrawPrimitives.DU_DRAW_LINES:
						material.GetTechnique(0).SetPointSize(size);
						mObject.Begin(mMaterialName, RenderOperation.OperationTypes.OT_LINE_LIST);
						break;
					case DebugDrawPrimitives.DU_DRAW_TRIS:
						mObject.Begin(mMaterialName, RenderOperation.OperationTypes.OT_TRIANGLE_LIST);
						break;
					case DebugDrawPrimitives.DU_DRAW_QUADS:
						// 오우거엔 quad로 그리기가 없다
						mObject.Begin(mMaterialName, RenderOperation.OperationTypes.OT_POINT_LIST);
						break;
				}
			}
		}

		public override void Vertex(float x, float y, float z, uint color)
		{
			if (!mDefined)
			{
				mObject.Position(x, y, z);
				float r, g, b;
				IntToColor((int)color, out r, out g, out b);
				mObject.Colour(r, g, b, 0.5f);
			}
		}

		public override void Vertex(float x, float y, float z, uint color, float u, float v)
		{
			Vertex(x, y, z, color);
		}

		public override void End()
		{
			if (!mDefined)
			{
				mObject.End();
			}

			mDefined = true;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Microsoft.Win32;

namespace SNFDesigner
{
    public partial class MainForm : DockContent
    {
		private static MainForm mThis = null;
		private DeserializeDockContent deserializeDockContent = null;
		private Dictionary<Keys, bool> mKeyStates = new Dictionary<Keys, bool>();
		private Point mCenterPosition = Point.Empty;

		public static MainForm This { get { return mThis; } }
		public Point CenterPosition { get { return mCenterPosition; } }

        /// <summary>
        /// 생성자
        /// </summary>
        public MainForm()
        {
			if (mThis == null)
			{
				mThis = this;
			}

            InitializeComponent();

			deserializeDockContent = new DeserializeDockContent(GetDockContent);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // 화면 구성 초기화
			new ViewportControl();
			new FSMDesignControl();
			new ObjectStorageControl();
			new ObjectPropertyControl();
			new FSMStorageControl();
			new FSMStatePropertyControl();

			try
			{
				dockPanel.LoadFromXml("DockPanel.config", deserializeDockContent);
			}
			catch
			{
				FSMDesignControl.This.Show(dockPanel, DockState.Document);
				FSMDesignControl.This.DockTo(dockPanel, DockStyle.Fill);

				ViewportControl.This.Show(dockPanel, DockState.Document);
				ViewportControl.This.DockTo(dockPanel, DockStyle.Fill);

				ObjectStorageControl.This.Show(dockPanel);
				ObjectStorageControl.This.DockTo(dockPanel, DockStyle.Right);

				ObjectPropertyControl.This.Show(dockPanel);
				ObjectPropertyControl.This.DockTo(dockPanel, DockStyle.Right);

				FSMStorageControl.This.Show(dockPanel);
				FSMStorageControl.This.DockTo(dockPanel, DockStyle.Right);

				FSMStatePropertyControl.This.Show(dockPanel);
				FSMStatePropertyControl.This.DockTo(dockPanel, DockStyle.Right);
			}

            // 메인 창의 제목 표시줄에 문서 이름 표시
            this.Text = "Scene & FSM Designer - " + SNFDocument.This.ProjectPath;

			//// TODO: 레지스트리에서 최근 사용 파일 목록 얻어옴
			//RegistryKey key = Registry.Users;
			//try
			//{
			//    string[] mruList = new string[10];
			//    for (int i = 0; i < 10; ++i)
			//    {
			//        mruList[i] = (string)key.GetValue("MRU" + (i + 1));
			//    }
			//}
			//catch (Exception)
			//{
			//}

			btnStop.Enabled = false;
		}

		private IDockContent GetDockContent(string persistString)
		{
			if (persistString == typeof(ViewportControl).ToString())
			{
				return ViewportControl.This;
			}
			else if (persistString == typeof(FSMDesignControl).ToString())
			{
				return FSMDesignControl.This;
			}
			else if (persistString == typeof(ObjectStorageControl).ToString())
			{
				return ObjectStorageControl.This;
			}
			else if (persistString == typeof(ObjectPropertyControl).ToString())
			{
				return ObjectPropertyControl.This;
			}
			else if (persistString == typeof(FSMStorageControl).ToString())
			{
				return FSMStorageControl.This;
			}
			else if (persistString == typeof(FSMStatePropertyControl).ToString())
			{
				return FSMStatePropertyControl.This;
			}
			else
			{
				return null;
			}
		}

		public bool OpenProject(string projectPath)
		{
			OgreManager.This.AddResourcePath(projectPath);

			this.New();

			// 문서 열기
			if (!SNFDocument.This.Open(projectPath))
			{
				MessageBox.Show("The project information file is damaged or missing. Failed to open the file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}

			btnShowSightRegion_Click(null, null);

			// 메인 창의 제목 표시줄에 문서 이름 표시
			this.Text = "Scene & FSM Designer - " + SNFDocument.This.ProjectPath;

			// TODO: 연 프로젝트를 레지스트리의 최근 사용 파일 목록에 추가
			// 해당 프로젝트가 최근 사용 파일 목록에 이미 있으면, 지우고 최신으로 등록
			//RegistryKey key = Registry.Users;

			return true;
		}

		public void NewProject(string projectPath)
		{
			OgreManager.This.AddResourcePath(projectPath);

			this.New();

			// SNFDocument 초기화
			SNFDocument.This.New(projectPath);

			// 메인 창의 제목 표시줄에 문서 이름 표시
			this.Text = "Scene & FSM Designer - " + SNFDocument.This.ProjectPath;

			// TODO: 새로 만든 프로젝트를 레지스트리의 최근 사용파일 목록에 추가
			// RegistryKey key = Registry.Users;
		}

        public bool Initialize()
        {
			return ViewportControl.This.Initialize();
        }

        public void Run()
        {
			while (ViewportControl.This.Render())
            {
                this.Idle();

                Application.DoEvents();
            }
        }

		//private DateTime mLastTime = DateTime.Now;

        public void Idle()
        {
			//DateTime currentTime = DateTime.Now;
			//float timeDelta = (float)((currentTime - mLastTime).TotalMilliseconds) * 0.001f;
			////System.Diagnostics.Debug.WriteLine(timeDelta);
			//mLastTime = currentTime;

			
        }

		public bool IsKeyDown(Keys keyCode)
		{
			if (!mKeyStates.ContainsKey(keyCode))
			{
				mKeyStates.Add(keyCode, false);
			}

			return mKeyStates[keyCode];
		}

		private void New()
		{
			if (!ViewportControl.This.New())
			{
				this.Close();
			}

			// 각종 옵션 창 리셋
			ObjectStorageControl.This.CleanupObjectStorage();    // 오브젝트 스토리지 정리
		}

		// 모든 콘트롤 비활성화
		public void EnableAllControls(bool enable)
		{
			//exitToolStripMenuItem.Enabled = enable;
			//saveTheSceneToolStripMenuItem.Enabled = enable;
			//btnSaveProject.Enabled = enable;
			//btnScaleObject.Enabled = enable;
			//btnRotateObject.Enabled = enable;
			//btnTranslateObject.Enabled = enable;
			//btnSelectObject.Enabled = enable;
			//btnCreateRegion.Enabled = enable;
			//btnPlay.Enabled = enable;
			//btnStop.Enabled = enable;

			//menuStrip.Enabled = enable;
			//toolStrip.Enabled = enable;

			foreach (ToolStripItem item in menuStrip.Items)
			{
				item.Enabled = enable;
			}

			foreach (ToolStripItem item in toolStrip.Items)
			{
				item.Enabled = enable;
			}

			if (FSMPlayer.This.Playing)
			{
				btnStop.Enabled = true;
			}
			else
			{
				btnStop.Enabled = false;
			}

			ObjectStorageControl.This.Enabled = enable;
			ObjectPropertyControl.This.Enabled = enable;
			FSMStorageControl.This.Enabled = enable;
			FSMStatePropertyControl.This.Enabled = enable;
		}

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
			OgreManager.This.Dispose();

			dockPanel.SaveAsXml("DockPanel.config", Encoding.Default);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // 종료를 취소할 경우
            if (!SNFDocument.This.ConfirmModifiedAndSave(saveFileDialog))
            {
                e.Cancel = true;
            }
        }

		//private void btnNewScene_Click(object sender, EventArgs e)
		//{
		//    // 기존 문서 저장 확인 후
		//    if (SNFDocument.This.ConfirmModifiedAndSave(saveFileDialog))
		//    {
		//        // SNFDocument 초기화
		//        SNFDocument.This.New();

		//        // 메인 창의 제목 표시줄에 문서 이름 표시
		//        this.Text = "Scene & FSM Designer - " + SNFDocument.This.FileName;

		//        this.New();
		//    }
		//}

		//private void btnOpenScene_Click(object sender, EventArgs e)
		//{
		//    // 기존 문서 저장 확인 후
		//    if (SNFDocument.This.ConfirmModifiedAndSave(saveFileDialog))
		//    {
		//        if (openFileDialog.ShowDialog() == DialogResult.OK)
		//        {
		//            // 해당 파일 존재하지 않으면 취소
		//            if(!System.IO.File.Exists(openFileDialog.FileName))
		//                return;

		//            this.New();

		//            // 문서 열기
		//            if (!SNFDocument.This.Open(openFileDialog.FileName))
		//            {
		//                MessageBox.Show("The file is damaged or missing. Failed to open the file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
		//                return;
		//            }

		//            // 오브젝트 스토리지에 메쉬 목록 추가
		//            SNFDocument.This.AddAllMeshToObjectStorage();

		//            // 오브젝트 리스트를 씬에 추가
		//            SNFDocument.This.AddAllObjectToScene();

		//            // 메인 창의 제목 표시줄에 문서 이름 표시
		//            this.Text = "Scene & FSM Designer - " + SNFDocument.This.FileName;

		//            // TODO: 연 문서를 레지스트리의 최근 사용 파일 목록에 추가
		//            //RegistryKey key = Registry.Users;
		//        }
		//    }
		//}

        /// <summary>
        /// save scene(저장) 버튼 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveScene_Click(object sender, EventArgs e)
        {
			//if (SNFDocument.This.IsNewFile)
			//{
			//    this.btnSaveSceneAs_Click(null, null);
			//}
			//else
            {
                // 모든 오브젝트의 변경 사항을 문서에 적용
                //SNFDocument.Singleton().AcceptChanges(ogreWindow);

                SNFDocument.This.Save();
            }
        }

		///// <summary>
		///// save scene as...(다른 이름으로 저장) 클릭 이벤트
		///// </summary>
		///// <param name="sender"></param>
		///// <param name="e"></param>
		//private void btnSaveSceneAs_Click(object sender, EventArgs e)
		//{
		//    if (saveFileDialog.ShowDialog() == DialogResult.OK)
		//    {
		//        // 모든 오브젝트의 변경 사항을 문서에 적용
		//        //SNFDocument.Singleton().AcceptChanges(ogreWindow);

		//        SNFDocument.This.SaveAs(saveFileDialog.FileName);

		//        // 메인 창의 제목 표시줄에 문서 이름 표시
		//        this.Text = "Scene & FSM Designer" + SNFDocument.This.FileName;
		//    }
		//}

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
		{
			// ESC 키 눌렀을 때 작업들
			if (e.KeyCode == Keys.Escape)
			{
				if (FSMPlayer.This.Playing)
				{
					btnStop_Click(null, null);
				}
				FSMStatePropertyControl.This.SetObjectSelectMode(false);
				TargetPointManager.This.ShowTargetPoint(false);
				RegionManager.This.CancelScale();
				EnableAllControls(true);
			}

			if (!mKeyStates.ContainsKey(e.KeyCode))
			{
				mKeyStates.Add(e.KeyCode, true);
			}
			else
			{
				mKeyStates[e.KeyCode] = true;
			}
        }

        private void MainForm_KeyUp(object sender, KeyEventArgs e)
		{
			if (!mKeyStates.ContainsKey(e.KeyCode))
			{
				mKeyStates.Add(e.KeyCode, false);
			}
			else
			{
				mKeyStates[e.KeyCode] = false;
			}
		}

        /// <summary>
        /// 오브젝트 선택 모드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectObject_Click(object sender, EventArgs e)
        {
            btnSelectObject.Checked = true;
            btnScaleObject.Checked = false;
            btnRotateObject.Checked = false;
            btnTranslateObject.Checked = false;
			OgreManager.This.ObjectControlMode = ObjectControlMode.SELECT;

			// 오브젝트의 시야를 이동시킬 수 있는 axis 숨김
			OgreManager.This.SetAxisVisible(false);
        }

        /// <summary>
        /// 오브젝트 스케일 모드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnScaleObject_Click(object sender, EventArgs e)
        {
            btnSelectObject.Checked = false;
            btnScaleObject.Checked = true;
            btnRotateObject.Checked = false;
            btnTranslateObject.Checked = false;
			OgreManager.This.ObjectControlMode = ObjectControlMode.SCALE;

			// 오브젝트의 시야를 이동시킬 수 있는 axis 띄움
			OgreManager.This.SetAxisVisible(true);
        }

        /// <summary>
        /// 오브젝트 회전 모드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRotateObject_Click(object sender, EventArgs e)
        {
            btnSelectObject.Checked = false;
            btnScaleObject.Checked = false;
            btnRotateObject.Checked = true;
            btnTranslateObject.Checked = false;
			OgreManager.This.ObjectControlMode = ObjectControlMode.ROTATE;

			// 오브젝트의 시야를 이동시킬 수 있는 axis 띄움
			OgreManager.This.SetAxisVisible(true);
        }

        /// <summary>
        /// 오브젝트 이동 모드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTranslateObject_Click(object sender, EventArgs e)
        {
            btnSelectObject.Checked = false;
            btnScaleObject.Checked = false;
            btnRotateObject.Checked = false;
            btnTranslateObject.Checked = true;
			OgreManager.This.ObjectControlMode = ObjectControlMode.TRANSLATE;

			// 오브젝트의 시야를 이동시킬 수 있는 axis 띄움
			OgreManager.This.SetAxisVisible(true);
		}

		// 오브젝트 제거
		private void btnRemoveObject_Click(object sender, EventArgs e)
		{
			OgreManager.This.RemoveSelectObject();
		}

		private void btnCreateRegion_Click(object sender, EventArgs e)
		{
			// 영역 생성 모드 토글
			RegionManager.This.Deploying = !RegionManager.This.Deploying;
			if (RegionManager.This.Deploying)
			{
				EnableAllControls(false);
			}
			else
			{
				EnableAllControls(true);
			}
		}

		private bool mShowSightRegion = false;

		private void btnShowSightRegion_Click(object sender, EventArgs e)
		{
			// 시야 / 영역 보이기 / 숨기기
			mShowSightRegion = !mShowSightRegion;

			if (!mShowSightRegion)
			{
				btnShowSightRegion.Image = Properties.Resources.OpenedEye;
			}
			else
			{
				btnShowSightRegion.Image = Properties.Resources.ClosedEye;
			}

			OgreManager.This.ShowSightRegion(mShowSightRegion);
		}

		// 인공지능 재생
		private void btnPlay_Click(object sender, EventArgs e)
		{
			FSMPlayer.This.Play();	// 재생

			EnableAllControls(false);	// 다른 작업 못 하게 막음

			if (FSMPlayer.This.GetPlayerObject() != null)
			{
				Cursor.Hide();
				Point viewportCenter = new Point(0, 0);
				viewportCenter.X += Size.Width/2;
				viewportCenter.Y += Size.Height/2;

				Cursor.Position = PointToScreen(viewportCenter);
				mCenterPosition = Cursor.Position;
			}
		}

		// 인공지능 재생 중지
		private void btnStop_Click(object sender, EventArgs e)
		{
			FSMPlayer.This.Stop();	// 중지

			EnableAllControls(true);

			Cursor.Show();
		}

		private void saveTheSceneToolStripMenuItem_Click(object sender, EventArgs e)
		{
			btnSaveScene_Click(null, null);
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.Close();
		}
    }
}

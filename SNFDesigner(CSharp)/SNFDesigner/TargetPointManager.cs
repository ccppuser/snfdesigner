﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Mogre;

namespace SNFDesigner
{
	public class TargetPointManager
	{
		private static TargetPointManager mThis = null;
		private bool mMovingTargetPoint = false;
		private SceneNode mTargetPointNode = null;

		public static TargetPointManager This
		{
			get
			{
				if (mThis == null)
				{
					mThis = new TargetPointManager();
				}

				return mThis;
			}
		}

		public bool MovingTargetPoint
		{
			get { return mMovingTargetPoint; }
			set
			{
				mMovingTargetPoint = value;

				if (mMovingTargetPoint)
				{
					MainForm.This.Cursor = Cursors.Cross;

					ShowTargetPoint(true);
				}
				else
				{
					MainForm.This.Cursor = Cursors.Arrow;

					ShowTargetPoint(false);
				}
			}
		}

		public TargetPointManager()
		{
			// FSMMoveObjectEvent의 목표 지점 속성을 나타내는 메쉬 생성
			Entity targetPoint = OgreManager.This.SceneManager.CreateEntity("targetpoint.mesh");
			targetPoint.QueryFlags = QueryFlags.EXCEPT;
			mTargetPointNode = OgreManager.This.RootSceneNode.CreateChildSceneNode("TargetPointNode");
			mTargetPointNode.AttachObject(targetPoint);

			// 일단 안 보이게 함
			mTargetPointNode.Parent.RemoveChild(mTargetPointNode);
		}

		public void FollowMouse(float x, float y)
		{
			if (MovingTargetPoint)
			{
				Vector3 groundPos = OgreManager.This.CalculateGroundPosition(x, y);
				mTargetPointNode.Position = groundPos;
			}
		}

		public void ShowTargetPoint(bool show)
		{
			if (show)
			{
				if (mTargetPointNode.Parent == null)
				{
					OgreManager.This.RootSceneNode.AddChild(mTargetPointNode);
				}
			}
			else
			{
				if (mTargetPointNode.Parent != null)
				{
					mTargetPointNode.Parent.RemoveChild(mTargetPointNode);
				}
			}
		}

		public void SetTargetPointPosition(Vector3 position)
		{
			mTargetPointNode.Position = position;
		}
	}
}

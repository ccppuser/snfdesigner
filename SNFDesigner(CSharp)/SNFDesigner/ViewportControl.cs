﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace SNFDesigner
{
	public enum TransformTarget
	{
		None,
		Object,
		Sight,
	}

	public partial class ViewportControl : DockContent
	{
		private static ViewportControl mThis = null;
		private Point mLastMousePoint = Point.Empty;
		private Point mStartTranslationMousePoint = Point.Empty;
		private TransformTarget mTransformTarget = TransformTarget.None;

		public static ViewportControl This
		{
			get { return mThis; }
		}

		public ViewportControl()
		{
			if (mThis == null)
			{
				mThis = this;
			}

			InitializeComponent();
		}

		public bool New()
		{
			// 오우거 리셋
			if (!OgreManager.This.New())
			{
				MessageBox.Show("Failed to initialize the ogre environment.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}

			return true;
		}

		public bool Initialize()
		{
			// 렌더링 창에 오우거를 초기화
			new OgreManager(this.Handle, this.Width, this.Height);
			//if (!OgreManager.This.Initialize())
			//{
			//    MessageBox.Show("Failed to initialize the ogre environment.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			//    return false;
			//}

			return true;
		}

		public bool Render()
		{
			//if (!OgreManager.This.Render())
			//{
			//    MessageBox.Show("Failed to render the ogre environment.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			//    this.Close();
			//}
			if (OgreManager.This != null)
			{
				if (this.Pane.ActiveContent == this)
				{
					OgreManager.This.Render();
				}

				return true;
			}
			else
			{
				return false;
			}
		}

		// 오브젝트 스토리지에서 오브젝트를 끌어왔을 때 오브젝트 로드
		private void ViewportControl_DragEnter(object sender, DragEventArgs e)
		{
			if (!OgreManager.This.IsDummyNodeEmpty() && e.Data.GetDataPresent(typeof(string)))
			{
				string meshName = e.Data.GetData(typeof(string)) as string;

				// 씬에 오브젝트 추가
				OgreManager.This.SetDummyMesh(meshName);

				// 오브젝트를 마우스 위치로 이동
				Rectangle rect = this.ClientRectangle;
				Point pt = this.PointToClient(new Point(e.X, e.Y));
				OgreManager.This.SetDummyNodePosition((float)pt.X / (float)rect.Width, (float)pt.Y / (float)rect.Height);

				Render();
			}
		}

		// 위에서 오브젝트를 드래그 중일 때 마우스 위치에 따라 오브젝트 이동
		private void ViewportControl_DragOver(object sender, DragEventArgs e)
		{
			Rectangle rect = this.ClientRectangle;
			Point pt = this.PointToClient(new Point(e.X, e.Y));
			OgreManager.This.SetDummyNodePosition((float)pt.X / (float)rect.Width, (float)pt.Y / (float)rect.Height);

			Render();
		}

		// 오브젝트 선택
		private void ViewportControl_MouseClick(object sender, MouseEventArgs e)
		{
		}

		// 마우스 드래그로 오브젝트의 이동, 회전, 크기 조절
		private void ViewportControl_MouseMove(object sender, MouseEventArgs e)
		{
			Rectangle rect = this.ClientRectangle;
			float x = (float)e.X / (float)rect.Width;
			float y = (float)e.Y / (float)rect.Height;

			Point currentMousePoint = e.Location;
			if (mLastMousePoint == currentMousePoint)
			{
				return;
			}

			Point deltaPoint = new Point(mLastMousePoint.X - currentMousePoint.X, mLastMousePoint.Y - currentMousePoint.Y);

			// 재생 중 아닐 경우
			if (!FSMPlayer.This.Playing)
			{
				if (e.Button == MouseButtons.Left)
				{
					// 영역 크기 조절 중
					if (RegionManager.This.Deploying)
					{
						if (MainForm.This.IsKeyDown(Keys.ShiftKey))
						{
							RegionManager.This.Scale(x, y);
						}
						else
						{
							RegionManager.This.Scale(x, y);
						}
					}
					// 오브젝트 크기/회전/이동 중
					else
					{
						if (OgreManager.This.SelectedNode != null)
						{
							Object obj = SNFDocument.This.GetObject(OgreManager.This.SelectedNode.Name);
							if (obj.ObjectType != ObjectType.Terrain)
							{
								switch (OgreManager.This.ObjectControlMode)
								{
									case ObjectControlMode.SCALE:
										{
											// 오브젝트 피킹됐으면 오브젝트 크기 조절
											if (mTransformTarget == TransformTarget.Object)
											{
												OgreManager.This.ScaleSelectObject(deltaPoint.Y * OgreManager.This.TimeDelta);
											}
											// 3D 화살표 피킹됐으면 시야 크기 조절
											else if (mTransformTarget == TransformTarget.Sight)
											{
												// 마우스 드래그 시작할 때 위치에서 현재 어느 방향으로 움직이는지 알아냄
												Mogre.Vector2 currentMousePosition = new Mogre.Vector2(currentMousePoint.X, currentMousePoint.Y);
												Mogre.Vector2 mouseVector = currentMousePosition - new Mogre.Vector2(mLastMousePoint.X, mLastMousePoint.Y);	// 마우스 움직이는 방향

												OgreManager.This.ScaleSightNodeOfSelectedNode(mouseVector);
											}
										}
										break;
									case ObjectControlMode.ROTATE:
										{
											// 오브젝트 피킹됐으면 오브젝트 회전
											if (mTransformTarget == TransformTarget.Object)
											{
												OgreManager.This.RotateSelectObject(deltaPoint.X * OgreManager.This.TimeDelta);
											}
											// 3D 화살표 피킹됐으면 시야 회전
											else if (mTransformTarget == TransformTarget.Sight)
											{
												// 마우스 드래그 시작할 때 위치에서 현재 어느 방향으로 움직이는지 알아냄
												Mogre.Vector2 currentMousePosition = new Mogre.Vector2(currentMousePoint.X, currentMousePoint.Y);
												Mogre.Vector2 mouseVector = currentMousePosition - new Mogre.Vector2(mLastMousePoint.X, mLastMousePoint.Y);	// 마우스 움직이는 방향

												OgreManager.This.RotateSightNodeOfSelectedNode(mouseVector, OgreManager.This.TimeDelta);
											}
										}
										break;
									case ObjectControlMode.TRANSLATE:
										{
											// 오브젝트 피킹됐으면 오브젝트 이동
											if (mTransformTarget == TransformTarget.Object)
											{
												Mogre.Vector3 groundPos = OgreManager.This.CalculateGroundPositionExceptMe(x, y);
												OgreManager.This.SetSelectObjectPosition(groundPos);
											}
											// 3D화살표 피킹됐으면 시야 이동
											else if (mTransformTarget == TransformTarget.Sight)
											{
												// 마우스 드래그 시작할 때 위치에서 현재 어느 방향으로 움직이는지 알아냄
												Mogre.Vector2 currentMousePosition = new Mogre.Vector2(currentMousePoint.X, currentMousePoint.Y);
												Mogre.Vector2 mouseVector = currentMousePosition - new Mogre.Vector2(mLastMousePoint.X, mLastMousePoint.Y);	// 마우스 움직이는 방향

												OgreManager.This.TranslateSightNodeOfSelectedNode(mouseVector);
											}
										}
										break;
								}
							}
						}
					}
				}
				// 아무 마우스 버튼 안 누르고 마우스 이동 시
				else
				{
					if (TargetPointManager.This.MovingTargetPoint)
					{
						TargetPointManager.This.FollowMouse(x, y);
					}
					else if (RegionManager.This.Deploying)
					{
						RegionManager.This.FollowMouse(x, y);
					}
				}
			}

			if (!FSMPlayer.This.Playing ||
				(FSMPlayer.This.Playing && FSMPlayer.This.GetPlayerObject() == null))
			{
				// 마우스 오른쪽 버튼으로 카메라 회전
				if (e.Button == MouseButtons.Right)
				{
					OgreManager.This.RotateCameraYaw(deltaPoint.X * OgreManager.This.TimeDelta);
					OgreManager.This.RotateCameraPitch(deltaPoint.Y * OgreManager.This.TimeDelta);
				}
				// 마우스 가운데 버튼으로 카메라 패닝(panning)
				else if (e.Button == MouseButtons.Middle)
				{
					OgreManager.This.TranslateCamera(0, -deltaPoint.Y * OgreManager.This.TimeDelta * 100, deltaPoint.X * OgreManager.This.TimeDelta * 100);
				}
			}

			mLastMousePoint = currentMousePoint;
		}

		// 마우스 드래그로 오브젝트 이동/회전/크기 조절 시작할 때,
		private void ViewportControl_MouseDown(object sender, MouseEventArgs e)
		{
			// 이동 시작시 처리
			if (e.Button == MouseButtons.Left)
			{
				mStartTranslationMousePoint = new Point(e.X, e.Y);

				Rectangle rect = this.ClientRectangle;
				float x = (float)e.X / (float)rect.Width;
				float y = (float)e.Y / (float)rect.Height;

				// 영역을 편집 중일 경우
				if (RegionManager.This.Deploying)
				{
					RegionManager.This.StartScale(x, y);
				}
				// FSM 의 주체 / 대상 오브젝트를 선택하는 중일 경우
				else
				{
					Mogre.Entity intersectedEntity = OgreManager.This.TryPick(x, y);
					// 아무 것도 피킹 안 됐으면 선택 취소
					if (intersectedEntity == null)
					{
						mTransformTarget = TransformTarget.None;

						OgreManager.This.UnpickObject();
						ObjectPropertyControl.This.SetObjectProperty(null);
						FSMStatePropertyControl.This.SetObjectSelectMode(false);
						FSMStatePropertyControl.This.SetPointSelectMode(false);
						TargetPointManager.This.ShowTargetPoint(false);
					}
					// 오브젝트 피킹됐으면 오브젝트 이동
					else if (intersectedEntity.QueryFlags == QueryFlags.ENTITY || intersectedEntity.QueryFlags == QueryFlags.TERRAIN)
					{
						mTransformTarget = TransformTarget.Object;

						// FSM과 연결할 오브젝트를 선택하는 중일 경우
						if (FSMStorageControl.This.ObjectSelectMode)
						{
							FSMStorageControl.This.ObjectSelectMode = false;

							// 오브젝트 피킹
							if (OgreManager.This.PickObject(x, y))
							{
								// 선택한 오브젝트의 이름을 알려줌
								string objectName = OgreManager.This.SelectedNode.Name;
								FSMStorageControl.This.SetPickedObject(SNFDocument.This.GetObject(objectName));
							}
						}
						// FSM에서 이벤트 속성으로 대상 오브젝트를 선택하는 중일 경우
						else if (FSMStatePropertyControl.This.ObjectSelectMode)
						{
							FSMStatePropertyControl.This.SetObjectSelectMode(false);

							// 오브젝트 피킹
							if (OgreManager.This.PickObject(x, y))
							{
								// 선택한 오브젝트의 이름을 알려줌
								string objectName = OgreManager.This.SelectedNode.Name;
								FSMStatePropertyControl.This.SetPickedObject(SNFDocument.This.GetObject(objectName));
							}
						}
						// FSM에서 이벤트 속성으로 목표 지점을 선택하는 중일 경우
						else if (FSMStatePropertyControl.This.PointSelectMode)
						{
							FSMStatePropertyControl.This.SetPointSelectMode(false);

							// 표면 피킹
							FSMStatePropertyControl.This.SetPickedGroundPoint(OgreManager.This.CalculateGroundPosition(x, y));
						}
						// 그냥 오브젝트 하나 선택하는 경우
						else
						{
							// 해당 마우스 위치로 오브젝트 선택
							// 선택된 오브젝트가 있으면
							if (OgreManager.This.PickObject(x, y))
							{
								// 오른쪽 창에 오브젝트 속성 창을 띄움
								ObjectPropertyControl.This.SetObjectProperty(SNFDocument.This.GetObject(OgreManager.This.SelectedNode.Name));
								ObjectPropertyControl.This.Pane.ActiveContent = ObjectPropertyControl.This;
							}
							// 선택된 오브젝트가 없으면
							else
							{
								// 오른쪽 창을 오브젝트 스토리지 창으로 바꿈
								ObjectStorageControl.This.Pane.ActiveContent = ObjectStorageControl.This;
							}
						}
					}
					// 영역 피킹됐으면..
					else if (intersectedEntity.QueryFlags == QueryFlags.REGION)
					{
						// FSM에서 목표 영역을 선택하는 중일 경우
						if (FSMStatePropertyControl.This.RegionSelectMode)
						{
							FSMStatePropertyControl.This.SetRegionSelectMode(false);

							// 영역 피킹
							string regionName = intersectedEntity.ParentSceneNode.Name;
							FSMStatePropertyControl.This.SetPickedRegion(SNFDocument.This.GetRegion(regionName));
						}
					}
					// 3D화살표 피킹됐으면 시야 이동
					else if (intersectedEntity.QueryFlags == QueryFlags.AXIS)
					{
						mTransformTarget = TransformTarget.Sight;

						FSMStatePropertyControl.This.SetObjectSelectMode(false);
						FSMStatePropertyControl.This.SetPointSelectMode(false);
						TargetPointManager.This.ShowTargetPoint(false);

						OgreManager.This.SetSelectedAxisType(intersectedEntity);
					}
				}
			}
		}

		// 마우스 드래그로 오브젝트 이동/회전/크기 조절 끝난 후,
		private void ViewportControl_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				// 영역 배치 완료
				if (RegionManager.This.Deploying)
				{
					RegionManager.This.EndScale();
					MainForm.This.EnableAllControls(true);

					RegionManager.This.Deploying = false;
				}
				else
				{
					switch(OgreManager.This.ObjectControlMode)
					{
						case ObjectControlMode.SCALE:
							if (mTransformTarget == TransformTarget.Sight)
							{
								ObjectPropertyControl.This.UpdateObjectProperty();
							}
							break;
					}
				}
			}
		}

		// 오브젝트 드래그 중에 ESC키 누르면 오브젝트 이동 취소
		private void ViewportControl_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
		{
			if (e.EscapePressed)    // 오브젝트 드래그 중에 ESC키 누르면 취소
			{
				e.Action = DragAction.Cancel;
			}
		}

		private void ViewportControl_Resize(object sender, EventArgs e)
		{
			if (OgreManager.This != null)
			{
				OgreManager.This.OnResize();
			}
		}
	}
}

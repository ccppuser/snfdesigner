﻿using System;
using System.Collections.Generic;
using System.Text;
using Mogre;

namespace SNFDesigner
{
    public static class OgreHelper
    {
        // 리소스 파일 이름으로 리소스 전체 경로 얻기
        public static string GetFullResourcePath(ResourceGroupManager rgm, string resourceName)
        {
            if (rgm == null)
                return "";

            // 리소스 파일이 포함된 위치 탐색
            StringVector groupNames = rgm.GetResourceGroups();
            foreach (string groupName in groupNames)
            {
                FileInfoListPtr fileInfoList = rgm.FindResourceFileInfo(groupName, resourceName, false);
                // 리소스 파일 경로 얻음
                foreach (FileInfo_NativePtr fileInfo in fileInfoList)
                {
                    // 최종 리소스 파일 경로 계산
                    return fileInfo.archive.Name + '/' + resourceName;
                }
            }

            return "";
        }

        // 바이트 버퍼로부터 DataStreamPtr 얻기
        public static DataStreamPtr BufferToDataStream(byte[] buffer)
        {
            MemoryDataStream rawStream;
            unsafe
            {
                fixed (void* pBuffer = &buffer[0])
                {
                    rawStream = new MemoryDataStream(pBuffer, (uint)buffer.Length);
                }
            }
            if (rawStream == null)
                return null;

            return new DataStreamPtr(rawStream);
        }

        // 문자열 버퍼로부터 DataStreamPtr 얻기
        public static DataStreamPtr BufferToDataStream(char[] buffer)
        {
            MemoryDataStream rawStream;
            unsafe
            {
                fixed (void* pBuffer = &buffer[0])
                {
                    rawStream = new MemoryDataStream(pBuffer, (uint)buffer.Length * sizeof(char));
                }
            }
            if (rawStream == null)
                return null;

            return new DataStreamPtr(rawStream);
        }

        // 하위 씬노드에서 해당 이름의 노드 존재하는지 확인
        public static bool IsChildExist(SceneNode node, string name)
        {
            SceneNode.ChildNodeIterator itr = node.GetChildIterator();

            while (itr.MoveNext())
            {
                if (itr.Current.Name == name)
                    return true;
            }

            return false;
		}

		public static bool IsEntityExist(string name)
		{
			return OgreManager.This.SceneManager.HasEntity(name);
		}

        // X, Y, Z 각 축의 회전 각도를 갖고 있는 Vector3객체로부터 각 축 회전 행렬을 곱한 최종 행렬을 만들어 줌
        public static Matrix3 MatricesFromAngles(Vector3 angles)
        {
            Matrix3 rotMatX = new Matrix3();
            Matrix3 rotMatY = new Matrix3();
            Matrix3 rotMatZ = new Matrix3();
            rotMatX.FromAxisAngle(Vector3.UNIT_X, new Radian(angles.x));
            rotMatY.FromAxisAngle(Vector3.UNIT_Y, new Radian(angles.y));
            rotMatZ.FromAxisAngle(Vector3.UNIT_Z, new Radian(angles.z));

            return rotMatX * rotMatY * rotMatZ;
        }

		public static float CalculateAngle(Vector2 a, Vector2 b)
		{
			return Mogre.Math.ATan2(new Vector2(-a.y, a.x).DotProduct(b), a.DotProduct(b)).ValueRadians;
		}
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace SNFDesigner
{
	public partial class ObjectStorageControl : DockContent
	{
		private static ObjectStorageControl mThis = null;
		private int mAutoGeneratedCount = 0;

		public static ObjectStorageControl This
		{
			get { return mThis; }
		}

		public ObjectStorageControl()
		{
			if (mThis == null)
			{
				mThis = this;
			}

			InitializeComponent();
		}

		public void CleanupObjectStorage()
		{
			this.lvwModelStorage.Clear();
		}

		public string ImportObject(string objectName)
		{
			// 모델 이름 리스트 뷰에 추가
			string modelName;
			if (objectName.Contains("\\") || objectName.Contains("/"))
			{
				string[] strTemp = objectName.Split(new char[] { '\\', '/' });
				modelName = strTemp[strTemp.Length - 1];
			}
			else
			{
				modelName = objectName;
			}

			this.lvwModelStorage.Items.Add(modelName);

			return modelName;
		}

		/// <summary>
		/// 오브젝트 스토리지에서 오브젝트 드래그 시작할 경우
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void lvwModelStorage_ItemDrag(object sender, ItemDragEventArgs e)
		{
			ListView lvw = (ListView)sender;
			if (lvw.SelectedItems.Count > 1)
			{
				MessageBox.Show("You can add only one object by one dragging.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			string meshName = lvw.SelectedItems[0].Text;
			// 드래그 시작
			DragDropEffects dropEffect = lvw.DoDragDrop(meshName, DragDropEffects.All | DragDropEffects.Link);

			// 드래그 중...

			// 드래그 완료
			SetObjectNameDlg dlg = new SetObjectNameDlg();
			dlg.MeshName = meshName;
			// 오브젝트 이름 정하기 대화상자 띄움
			DialogResult result = dlg.ShowDialog();

			// 이름 안 정하고 취소하면 더미 노드에서 오브젝트 삭제
			if (result == DialogResult.Cancel)
			{
				OgreManager.This.EmptyDummyNode();
				return;
			}

			// 이름 정했으면 오브젝트를 더미 노드에서 실제 노드(오브젝트 이름으로 된 노드)로 이동
			string objectName = dlg.ObjectName;
			if (objectName == "(Auto)")
			{
				do
				{
					objectName = "object" + (mAutoGeneratedCount++).ToString();
				}
				while (OgreHelper.IsChildExist(OgreManager.This.RootSceneNode, objectName));
			}

			OgreManager.This.MoveDummyObjectTo(objectName);

			// 추가한 오브젝트의 이름과 위치를 SNF문서에 추가
			SNFDocument.This.AddObject(meshName, objectName, OgreManager.This.GetObjectPosition(objectName));
		}
	}
}

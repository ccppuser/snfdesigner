﻿namespace SNFDesigner
{
	partial class ObjectStorageControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lvwModelStorage = new System.Windows.Forms.ListView();
			this.label1 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// lvwModelStorage
			// 
			this.lvwModelStorage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lvwModelStorage.Location = new System.Drawing.Point(12, 60);
			this.lvwModelStorage.Name = "lvwModelStorage";
			this.lvwModelStorage.Size = new System.Drawing.Size(200, 494);
			this.lvwModelStorage.TabIndex = 5;
			this.lvwModelStorage.UseCompatibleStateImageBehavior = false;
			this.lvwModelStorage.View = System.Windows.Forms.View.List;
			this.lvwModelStorage.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvwModelStorage_ItemDrag);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label1.Location = new System.Drawing.Point(50, 28);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(150, 16);
			this.label1.TabIndex = 4;
			this.label1.Text = "오브젝트 스토리지";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::SNFDesigner.Properties.Resources.ObjectStorage;
			this.pictureBox1.Location = new System.Drawing.Point(12, 12);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(32, 32);
			this.pictureBox1.TabIndex = 3;
			this.pictureBox1.TabStop = false;
			// 
			// openFileDialog
			// 
			this.openFileDialog.DefaultExt = "mesh";
			this.openFileDialog.Filter = "OGRE3D Mesh Files(*.mesh)|*.mesh";
			this.openFileDialog.Multiselect = true;
			// 
			// ObjectStorageControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(224, 566);
			this.CloseButtonVisible = false;
			this.ControlBox = false;
			this.Controls.Add(this.lvwModelStorage);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.pictureBox1);
			this.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "ObjectStorageControl";
			this.Text = "오브젝트 스토리지";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListView lvwModelStorage;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
	}
}
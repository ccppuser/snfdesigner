﻿namespace SNFDesigner
{
	partial class ObjectPropertyControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblPropertyType = new System.Windows.Forms.Label();
			this.picPropertyIcon = new System.Windows.Forms.PictureBox();
			this.propObject = new System.Windows.Forms.PropertyGrid();
			((System.ComponentModel.ISupportInitialize)(this.picPropertyIcon)).BeginInit();
			this.SuspendLayout();
			// 
			// lblPropertyType
			// 
			this.lblPropertyType.AutoSize = true;
			this.lblPropertyType.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblPropertyType.Location = new System.Drawing.Point(50, 28);
			this.lblPropertyType.Name = "lblPropertyType";
			this.lblPropertyType.Size = new System.Drawing.Size(116, 16);
			this.lblPropertyType.TabIndex = 6;
			this.lblPropertyType.Text = "오브젝트 속성";
			// 
			// picPropertyIcon
			// 
			this.picPropertyIcon.Image = global::SNFDesigner.Properties.Resources.Object;
			this.picPropertyIcon.Location = new System.Drawing.Point(12, 12);
			this.picPropertyIcon.Name = "picPropertyIcon";
			this.picPropertyIcon.Size = new System.Drawing.Size(32, 32);
			this.picPropertyIcon.TabIndex = 5;
			this.picPropertyIcon.TabStop = false;
			// 
			// propObject
			// 
			this.propObject.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.propObject.Location = new System.Drawing.Point(12, 59);
			this.propObject.Name = "propObject";
			this.propObject.Size = new System.Drawing.Size(200, 495);
			this.propObject.TabIndex = 2;
			// 
			// ObjectPropertyControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(224, 566);
			this.CloseButtonVisible = false;
			this.ControlBox = false;
			this.Controls.Add(this.propObject);
			this.Controls.Add(this.lblPropertyType);
			this.Controls.Add(this.picPropertyIcon);
			this.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "ObjectPropertyControl";
			this.Text = "오브젝트 속성";
			this.Load += new System.EventHandler(this.ObjectPropertyControl_Load);
			((System.ComponentModel.ISupportInitialize)(this.picPropertyIcon)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		protected System.Windows.Forms.Label lblPropertyType;
		protected System.Windows.Forms.PictureBox picPropertyIcon;
		private System.Windows.Forms.PropertyGrid propObject;
	}
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace SNFDesigner
{
	public partial class ObjectPropertyControl : DockContent
	{
		private static ObjectPropertyControl mThis = null;
		//Object mObject = null;
		//ObjectProperty mObjectProperty = new ObjectProperty();

		public static ObjectPropertyControl This
		{
			get { return mThis; }
		}

		public ObjectPropertyControl()
		{
			if (mThis == null)
			{
				mThis = this;
			}

			InitializeComponent();
		}

		private void ObjectPropertyControl_Load(object sender, EventArgs e)
		{
			//this.propObject.SelectedObject = mObjectProperty;
			this.propObject.SelectedObject = null;
			this.propObject.LineColor = Color.Gainsboro;
			this.propObject.ToolbarVisible = false;
		}

		public void SetObjectProperty(Object o)
		{
			//mObject = o;

			//this.mObjectProperty.ObjectName = objectName;
			//this.mObjectProperty.MeshName = o.mMeshName;
			//this.mObjectProperty.ObjectPosition = o.mObjectPosition.ToString();
			//this.mObjectProperty.ObjectRotation = o.mObjectRotation.ToString();
			//this.mObjectProperty.ObjectScale = o.mObjectScale;
			//this.mObjectProperty.HasSight = o.mHasSight;
			//this.mObjectProperty.SightPosition = o.mSightPosition.ToString();
			//this.mObjectProperty.SightRotation = o.mSightRotation.ToString();
			//this.mObjectProperty.SightRange = o.mSightRange;
			//this.mObjectProperty.SightWidth = o.mSightWidth;
			//this.mObjectProperty.SightHeight = o.mSightHeight;

			this.propObject.SelectedObject = o;

			this.propObject.Refresh();
		}

		public void UpdateObjectProperty()
		{
			this.propObject.Refresh();
		}
	}
}

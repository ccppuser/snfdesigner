﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SNFDesigner
{
    public partial class SetObjectNameDlg : Form
    {
        private bool bCancel = false;

        public string MeshName
        {
            set { txtMeshName.Text = value; }
        }

        public string ObjectName
        {
            get { return txtObjectName.Text; }
        }

        public SetObjectNameDlg()
        {
            InitializeComponent();
        }

        private void SetObjectNameDlg_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (bCancel)
                return;

			if (txtObjectName.Text.Length == 0)
            {
                MessageBox.Show("Object's name cannot be empty.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }

			if (OgreHelper.IsChildExist(OgreManager.This.RootSceneNode, txtObjectName.Text))
            {
                MessageBox.Show("Object's name is duplicated.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            bCancel = true;
        }

		private void txtObjectName_MouseClick(object sender, MouseEventArgs e)
		{
			txtObjectName.SelectAll();
		}
    }
}

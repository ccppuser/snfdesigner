﻿namespace SNFDesigner
{
	partial class NewOrOpenDlg
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewOrOpenDlg));
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.btnOpenExistProject = new System.Windows.Forms.Button();
			this.btnSelectExistPath = new System.Windows.Forms.Button();
			this.txtSelectedExistPath = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.btnCreateNewProject = new System.Windows.Forms.Button();
			this.btnSelectNewPath = new System.Windows.Forms.Button();
			this.txtSelectedNewPath = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Location = new System.Drawing.Point(12, 12);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(541, 146);
			this.tabControl1.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.btnOpenExistProject);
			this.tabPage1.Controls.Add(this.btnSelectExistPath);
			this.tabPage1.Controls.Add(this.txtSelectedExistPath);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(533, 120);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "기존 프로젝트 불러오기";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// btnOpenExistProject
			// 
			this.btnOpenExistProject.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOpenExistProject.Location = new System.Drawing.Point(186, 72);
			this.btnOpenExistProject.Name = "btnOpenExistProject";
			this.btnOpenExistProject.Size = new System.Drawing.Size(160, 23);
			this.btnOpenExistProject.TabIndex = 3;
			this.btnOpenExistProject.Text = "프로젝트 불러오기";
			this.btnOpenExistProject.UseVisualStyleBackColor = true;
			this.btnOpenExistProject.Click += new System.EventHandler(this.btnOpenExistProject_Click);
			// 
			// btnSelectExistPath
			// 
			this.btnSelectExistPath.Location = new System.Drawing.Point(414, 34);
			this.btnSelectExistPath.Name = "btnSelectExistPath";
			this.btnSelectExistPath.Size = new System.Drawing.Size(113, 23);
			this.btnSelectExistPath.TabIndex = 2;
			this.btnSelectExistPath.Text = "폴더 선택...";
			this.btnSelectExistPath.UseVisualStyleBackColor = true;
			this.btnSelectExistPath.Click += new System.EventHandler(this.btnSelectExistPath_Click);
			// 
			// txtSelectedExistPath
			// 
			this.txtSelectedExistPath.Location = new System.Drawing.Point(8, 36);
			this.txtSelectedExistPath.Name = "txtSelectedExistPath";
			this.txtSelectedExistPath.Size = new System.Drawing.Size(400, 21);
			this.txtSelectedExistPath.TabIndex = 1;
			this.txtSelectedExistPath.TextChanged += new System.EventHandler(this.txtSelectedExistPath_TextChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 12);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(125, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "불러올 프로젝트 폴더:";
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.btnCreateNewProject);
			this.tabPage2.Controls.Add(this.btnSelectNewPath);
			this.tabPage2.Controls.Add(this.txtSelectedNewPath);
			this.tabPage2.Controls.Add(this.label2);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(533, 120);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "새 프로젝트 시작";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// btnCreateNewProject
			// 
			this.btnCreateNewProject.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnCreateNewProject.Location = new System.Drawing.Point(186, 72);
			this.btnCreateNewProject.Name = "btnCreateNewProject";
			this.btnCreateNewProject.Size = new System.Drawing.Size(160, 23);
			this.btnCreateNewProject.TabIndex = 3;
			this.btnCreateNewProject.Text = "프로젝트 만들기";
			this.btnCreateNewProject.UseVisualStyleBackColor = true;
			this.btnCreateNewProject.Click += new System.EventHandler(this.btnCreateNewProject_Click);
			// 
			// btnSelectNewPath
			// 
			this.btnSelectNewPath.Location = new System.Drawing.Point(414, 34);
			this.btnSelectNewPath.Name = "btnSelectNewPath";
			this.btnSelectNewPath.Size = new System.Drawing.Size(113, 23);
			this.btnSelectNewPath.TabIndex = 2;
			this.btnSelectNewPath.Text = "폴더 선택...";
			this.btnSelectNewPath.UseVisualStyleBackColor = true;
			this.btnSelectNewPath.Click += new System.EventHandler(this.btnSelectNewPath_Click);
			// 
			// txtSelectedNewPath
			// 
			this.txtSelectedNewPath.Location = new System.Drawing.Point(8, 36);
			this.txtSelectedNewPath.Name = "txtSelectedNewPath";
			this.txtSelectedNewPath.Size = new System.Drawing.Size(400, 21);
			this.txtSelectedNewPath.TabIndex = 1;
			this.txtSelectedNewPath.Text = "D:\\SampleForSNFDesigner ";
			this.txtSelectedNewPath.TextChanged += new System.EventHandler(this.txtSelectedNewPath_TextChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 12);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(101, 12);
			this.label2.TabIndex = 0;
			this.label2.Text = "새 프로젝트 폴더:";
			// 
			// NewOrOpenDlg
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(565, 226);
			this.Controls.Add(this.tabControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "NewOrOpenDlg";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "SNF 디자이너 시작";
			this.Load += new System.EventHandler(this.NewOrOpenDlg_Load);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Button btnSelectExistPath;
		private System.Windows.Forms.TextBox txtSelectedExistPath;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnSelectNewPath;
		private System.Windows.Forms.TextBox txtSelectedNewPath;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnOpenExistProject;
		private System.Windows.Forms.Button btnCreateNewProject;

	}
}
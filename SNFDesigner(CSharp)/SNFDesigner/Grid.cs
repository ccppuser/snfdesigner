﻿using System;
using System.Collections.Generic;
using System.Text;
using Mogre;

namespace SNFDesigner
{
    public class Grid
    {
        private SceneManager mSceneManager = null;

        public Grid(SceneManager sceneManager)
        {
            mSceneManager = sceneManager;
        }

        public void Create()
        {
            // grid를 위한 material 생성
            MaterialPtr gridMaterial = MaterialManager.Singleton.Create("GridMaterial", ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME, true);
            gridMaterial.ReceiveShadows = false;
            gridMaterial.GetTechnique(0).SetLightingEnabled(false);
            gridMaterial.GetTechnique(0).GetPass(0).SetDiffuse(1.0f, 1.0f, 1.0f, 1.0f);
            gridMaterial.GetTechnique(0).GetPass(0).SetAmbient(1.0f, 1.0f, 1.0f);
            gridMaterial.GetTechnique(0).GetPass(0).SetSelfIllumination(1.0f, 1.0f, 1.0f);
            gridMaterial.Dispose();

            // ManualObject 생성
            ManualObject grid = mSceneManager.CreateManualObject("Grid");

            // 가로 grid 그리기
            for (float z = 0.0f; z < 15.0f; z += 1.0f)
            {
                grid.Begin("GridMaterial", RenderOperation.OperationTypes.OT_LINE_LIST);
                if (z == 7.0f)
                    grid.Colour(0.0f, 0.0f, 0.0f);
                else
                    grid.Colour(0.5f, 0.5f, 0.5f);
                grid.Position((0.0f - 7.0f) * 10.0f, 0.0f, (z - 7.0f) * 10.0f);
                grid.Position((14.0f - 7.0f) * 10.0f, 0.0f, (z - 7.0f) * 10.0f);
                grid.End();
            }
            // 세로 grid 그리기
            for (float x = 0.0f; x < 15.0f; x += 1.0f)
            {
                grid.Begin("GridMaterial", RenderOperation.OperationTypes.OT_LINE_LIST);
                if (x == 7.0f)
                    grid.Colour(0.0f, 0.0f, 0.0f);
                else
                    grid.Colour(0.5f, 0.5f, 0.5f);
                grid.Position((x - 7.0f) * 10.0f, 0.0f, (0.0f - 7.0f) * 10.0f);
                grid.Position((x - 7.0f) * 10.0f, 0.0f, (14.0f - 7.0f) * 10.0f);
                grid.End();
            }

            // 쿼리 마스크 설정(피킹 못하도록)
            grid.QueryFlags = QueryFlags.EXCEPT;

            SceneNode gridNode = mSceneManager.RootSceneNode.CreateChildSceneNode("GridNode");
            gridNode.SetPosition(0.0f, 0.0f, 0.0f);
            gridNode.AttachObject(grid);
        }

        public void Destroy()
        {
            mSceneManager.DestroyManualObject("Grid");
            mSceneManager.DestroySceneNode("GridNode");
        }

        public void SetVisible(bool visible)
        {
            mSceneManager.GetSceneNode("GridNode").SetVisible(visible);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SNFDesigner
{
	class Helper
	{
		public static float GetDistance(Point point1, Point point2)
		{
			//pythagorean theorem c^2 = a^2 + b^2
			//thus c = square root(a^2 + b^2)
			float a = (float)(point2.X - point1.X);
			float b = (float)(point2.Y - point1.Y);

			return (float)Math.Sqrt(a * a + b * b);
		}
	}
}

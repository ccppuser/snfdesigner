﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Mogre;
using MMOC;

namespace SNFDesigner
{
    public enum ObjectControlMode
    {
        SELECT,
        SCALE,
        ROTATE,
        TRANSLATE
    }

    public class QueryFlags
    {
        public const uint ENTITY =	1;
        public const uint EXCEPT =	2;
		public const uint AXIS =	4;
		public const uint REGION =	8;
		public const uint TERRAIN =	16;
    }

	public class OgreManager
	{
		private static OgreManager mThis = null;
		//private static string strSNFResourceGroup = "SNF";
		//private static string strSNFResourceGroupFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\SNF";
		private List<string> resourceFolderList = new List<string>();
		private bool readyToRender = false;
		private IntPtr hWnd;
		private int nWidth;
		private int nHeight;
		private string basePath = "";
		private Root root = null;
		private SceneManager sceneManager = null;
		private Camera camera = null;
		private Viewport viewport = null;
		private RenderWindow renderWindow = null;
		private Grid grid = null;
		private Axis axis = null;
		private RaySceneQuery raySceneQuery = null;
		private SceneNode dummyNode = null;
		private SceneNode currentSelectNode = null;
		private ObjectControlMode objectControlMode = ObjectControlMode.SELECT;
		private CollisionTools collisionTools = null;
		private AxisType mSelectedAxisType = AxisType.X;
		private float mTimeDelta = 0;
		private List<AnimationState> mPlayingAnimationList = new List<AnimationState>();

		//public static string SNFResourceGroup { get { return strSNFResourceGroup; } }
		public SceneManager SceneManager { get { return sceneManager; } }
		public SceneNode RootSceneNode { get { return sceneManager.RootSceneNode; } }
		public SceneNode SelectedNode { get { return currentSelectNode; } }
		public ObjectControlMode ObjectControlMode { get { return objectControlMode; } set { objectControlMode = value; } }
		public float TimeDelta { get { return mTimeDelta; } }

		public static OgreManager This
		{
			get { return mThis; }
		}

		/// <summary>
		/// 생성자
		/// </summary>
		/// <param name="control">렌더링 할 대상 콘트롤</param>
		public OgreManager(IntPtr _hWnd, int _width, int _height)
		{
			if (mThis == null)
			{
				mThis = this;
			}

			//if (!Directory.Exists(strSNFResourceGroupFolder))
			//{
			//    Directory.CreateDirectory(strSNFResourceGroupFolder);
			//}
			this.hWnd = _hWnd;
			this.nWidth = _width;
			this.nHeight = _height;
			this.basePath = Directory.GetCurrentDirectory();

			this.root = new Root();
		}

		public void AddResourcePath(string path)
		{
			if (!resourceFolderList.Contains(path))
			{
				resourceFolderList.Add(path);
			}

			//ResourceGroupManager.Singleton.AddResourceLocation(path, "FileSystem", ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME, true);
			//ResourceGroupManager.Singleton.InitialiseResourceGroup(ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME);
			//ResourceGroupManager.Singleton.LoadResourceGroup(ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME);

			//Archive archive = ArchiveManager.Singleton.Load(path, "FileSystem");
			//StringVectorPtr fileNames = archive.Find("*.material");
			//foreach (string fileName in fileNames)
			//{
			//    DataStreamPtr stream = archive.Open(fileName);
			//    MaterialManager.Singleton.ParseScript(stream, ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME);
			//}
			//ArchiveManager.Singleton.Unload(archive);
		}

		public bool Initialize()
		{
			root = null;
			sceneManager = null;
			camera = null;
			viewport = null;
			renderWindow = null;
			grid = null;
			raySceneQuery = null;
			dummyNode = null;
			currentSelectNode = null;
			objectControlMode = ObjectControlMode.SELECT;
			collisionTools = null;

			if (!this.InitializeOgreWindow())
				return false;

			this.SetupScene();

			// SNF 리소스그룹 생성
			//string groupName = strSNFResourceGroup;
			//{
			//    ResourceGroupManager.Singleton.CreateResourceGroup(groupName);
			//    ResourceGroupManager.Singleton.AddResourceLocation(strSNFResourceGroupFolder, "FileSystem", groupName, true);
			//    ResourceGroupManager.Singleton.InitialiseResourceGroup(groupName);
			//    ResourceGroupManager.Singleton.LoadResourceGroup(groupName);
			//}

			// 메쉬 로드
			//string meshName = "athene";
			//{
			//    string fileName = "../../Media/models/testCube.mesh";
			//    MeshPtr mesh = MeshManager.Singleton.CreateManual(meshName, groupName);
			//    // 데이터 버퍼 준비
			//    StreamReader streamReader = new StreamReader(fileName);
			//    if (streamReader == null)
			//        return false;
			//    int streamLength = (int)streamReader.BaseStream.Length;
			//    byte[] meshBuffer = new byte[streamLength];
			//    int readLength = streamReader.BaseStream.Read(meshBuffer, 0, streamLength);
			//    streamReader.Close();
			//    MemoryDataStream rawStream;
			//    unsafe
			//    {
			//        fixed (void* pBuffer = &meshBuffer[0])
			//        {
			//            rawStream = new MemoryDataStream(pBuffer, (uint)streamLength);
			//        }
			//    }
			//    DataStreamPtr meshStream = new DataStreamPtr(rawStream);
			//    // 메쉬 파싱
			//    MeshSerializer fileReader = new MeshSerializer();
			//    fileReader.Listener = MeshManager.Singleton.Listener;
			//    fileReader.ImportMesh(meshStream, mesh.Target);
			//    string name = mesh.Name;
			//    int nSubMesh = mesh.NumSubMeshes;
			//    // 재질 및 텍스쳐 테스트
			//    MaterialPtr material = MaterialManager.Singleton.Create("asdf", ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME);
			//    material.ReceiveShadows = false;
			//    material.GetTechnique(0).SetLightingEnabled(true);
			//    material.GetTechnique(0).GetPass(0).CreateTextureUnitState("BeachStones.jpg");
			//    string textureName = material.GetTechnique(0).GetPass(0).GetTextureUnitState(0).Name;
			//    material.GetTechnique(0).GetPass(0).SetDiffuse(1.0f, 0.0f, 0.0f, 1.0f);
			//    material.GetTechnique(0).GetPass(0).SetAmbient(0.0f, 1.0f, 0.0f);
			//    material.GetTechnique(0).GetPass(0).SetSelfIllumination(0.0f, 0.0f, 1.0f);
			//    //mesh.GetSubMesh(0).MaterialName = "asdf";
			//}
			//// 엔티티 생성
			//Entity entity = sceneManager.CreateEntity("athene", meshName);
			//SceneNode sceneNode = sceneManager.RootSceneNode.CreateChildSceneNode("atheneNode", new Vector3(0.0f, 0.0f, 0.0f));
			//sceneNode.AttachObject(entity);
			//sceneNode.Rotate(new Vector3(0.0f, 1.0f, 0.0f), 3.14f * 1.5f);

			// 테스트 지형
			//sceneManager.SetWorldGeometry("terrain.cfg");

			return true;
		}

		private bool InitializeOgreWindow()
		{
			Directory.SetCurrentDirectory(basePath);

			// 오우거 생성
			if (root == null)
			{
				root = new Root();
			}

			// 'resources.cfg' 파일 내의 모든 리소스 디렉토리 검사
			ConfigFile cf = new ConfigFile();
			cf.Load("resources.cfg", "\t:=", true);
			ConfigFile.SectionIterator secItr = cf.GetSectionIterator();
			string secName, typeName, archName;
			while (secItr.MoveNext())
			{
				secName = secItr.CurrentKey;
				ConfigFile.SettingsMultiMap settings = secItr.Current;
				foreach (KeyValuePair<string, string> pair in settings)
				{
					typeName = pair.Key;
					archName = pair.Value;
					ResourceGroupManager.Singleton.AddResourceLocation(archName, typeName, secName);
				}
			}

			// 부가적인 리소스 디렉토리도 추가해줌
			foreach (string path in resourceFolderList)
			{
				ResourceGroupManager.Singleton.AddResourceLocation(path, "FileSystem");
			}

			// 다이렉트3D9 사용 가능한지 검사
			foreach (RenderSystem rs in root.GetAvailableRenderers())
			{
				if (rs.Name == "Direct3D9 Rendering Subsystem")
				{
					root.RenderSystem = rs;
					break;
				}
			}

			// 컴퓨터에서 Direct3D9를 지원하지 않는다면 false 리턴
			if (root.RenderSystem == null)
				return false;

			// 렌더러 환경 설정
			root.RenderSystem.SetConfigOption("Full Screen", "No");
			root.RenderSystem.SetConfigOption("Video Mode", nWidth + " x " + nHeight + " @ 32-bit colour");

			// 윈도우 생성
			root.Initialise(false);
			NameValuePairList misc = new NameValuePairList();
			misc["externalWindowHandle"] = hWnd.ToString();
			renderWindow = root.CreateRenderWindow("Render Window", 0, 0, false, misc);
			ResourceGroupManager.Singleton.InitialiseAllResourceGroups();

			root.FrameStarted += new FrameListener.FrameStartedHandler(root_FrameStarted);

			readyToRender = true;

			return true;
		}

		private bool root_FrameStarted(FrameEvent evt)
		{
			mTimeDelta = evt.timeSinceLastFrame;

			// 재생 중일 경우 플레이어 조종
			if (FSMPlayer.This.Playing && FSMPlayer.This.GetPlayerObject() != null && mTimeDelta != 0)
			{
				if (MainForm.This.IsKeyDown(Keys.W))
					PlayerMoveForward(mTimeDelta);
				if (MainForm.This.IsKeyDown(Keys.S))
					PlayerMoveBack(mTimeDelta);
				if (MainForm.This.IsKeyDown(Keys.A))
					PlayerMoveLeft(mTimeDelta);
				if (MainForm.This.IsKeyDown(Keys.D))
					PlayerMoveRight(mTimeDelta);

				System.Drawing.Point currentMousePosition = Cursor.Position;
				// 재생 중일 경우 마우스 움직이기만 해도 카메라 돌리기
				if (FSMPlayer.This.Playing && FSMPlayer.This.GetPlayerObject() != null)
				{
					System.Drawing.Point deltaPosition = new System.Drawing.Point(MainForm.This.CenterPosition.X - currentMousePosition.X, MainForm.This.CenterPosition.Y - currentMousePosition.Y);

					OgreManager.This.RotateCameraYaw(deltaPosition.X * mTimeDelta * 0.5f);
					OgreManager.This.RotateCameraPitch(deltaPosition.Y * mTimeDelta * 0.5f);

					Cursor.Position = MainForm.This.CenterPosition;
				}
			}

			// 뷰포트에 포커스 있을 때만 카메라 조종
			if (ViewportControl.This.Focused)
			{
				if (!FSMPlayer.This.Playing ||
					(FSMPlayer.This.Playing && FSMPlayer.This.GetPlayerObject() == null))
				{
					float slow = 100;
					float fast = 1000;
					if (MainForm.This.IsKeyDown(Keys.ShiftKey))
					{
						if (MainForm.This.IsKeyDown(Keys.W))
							TranslateCamera(fast * mTimeDelta, 0, 0);
						if (MainForm.This.IsKeyDown(Keys.S))
							TranslateCamera(-fast * mTimeDelta, 0, 0);
						if (MainForm.This.IsKeyDown(Keys.A))
							TranslateCamera(0, 0, -fast * mTimeDelta);
						if (MainForm.This.IsKeyDown(Keys.D))
							TranslateCamera(0, 0, fast * mTimeDelta);
					}
					else
					{
						if (MainForm.This.IsKeyDown(Keys.W))
							TranslateCamera(slow * mTimeDelta, 0, 0);
						if (MainForm.This.IsKeyDown(Keys.S))
							TranslateCamera(-slow * mTimeDelta, 0, 0);
						if (MainForm.This.IsKeyDown(Keys.A))
							TranslateCamera(0, 0, -slow * mTimeDelta);
						if (MainForm.This.IsKeyDown(Keys.D))
							TranslateCamera(0, 0, slow * mTimeDelta);
					}
				}
			}

			FSMPlayer.This.Update();

			foreach (AnimationState animationState in mPlayingAnimationList)
			{
				animationState.AddTime(mTimeDelta);
			}

			return true;
		}

		private void SetupScene()
		{
			// 오우거 씬 매니저 생성
			sceneManager = root.CreateSceneManager(SceneType.ST_GENERIC, "SceneManager");
			sceneManager.AmbientLight = new ColourValue(0.5f, 0.5f, 0.5f);
			sceneManager.ShadowTechnique = ShadowTechnique.SHADOWTYPE_STENCIL_ADDITIVE;

			// 카메라 생성
			camera = sceneManager.CreateCamera("SceneCamera");
			camera.Position = new Vector3(-200.0f, 200.0f, 200.0f);   // -200cm, 200cm, 200cm
			camera.LookAt(new Vector3(0.0f, 0.0f, 0.0f));
			camera.NearClipDistance = 0.1f; // 0.1cm
			camera.FarClipDistance = 100000.0f; // 1km
			camera.AutoAspectRatio = true;

			// 뷰포트 설정
			viewport = renderWindow.AddViewport(camera);
			viewport.BackgroundColour = new ColourValue(95.0f / 255.0f, 95.0f / 255.0f, 95.0f / 255.0f, 1.0f);

			// 기본 방향성 광원 설치
			Light light = sceneManager.CreateLight("light");
			light.Type = Light.LightTypes.LT_DIRECTIONAL;
			light.Direction = new Vector3(-1.0f, -1.0f, -1.0f);
			light.DiffuseColour = new ColourValue(0.5f, 0.5f, 0.5f);
			sceneManager.RootSceneNode.CreateChildSceneNode("lightNode").AttachObject(light);

			// grid 띄우기
			grid = new Grid(sceneManager);
			grid.Create();

			// 3d 화살표 띄우기
			axis = new Axis(sceneManager);
			axis.Create();

			// 더미 노드(씬에 오브젝트 추가할 때 사용되는 임시 노드) 생성
			dummyNode = sceneManager.RootSceneNode.CreateChildSceneNode("Dummy", Vector3.ZERO);

			// RaySceneQuery 객체 초기화
			raySceneQuery = sceneManager.CreateRayQuery(new Ray());

			//
			collisionTools = new CollisionTools(sceneManager);
		}

		public void Cleanup()
		{
			if (raySceneQuery != null)
			{
				sceneManager.DestroyQuery(raySceneQuery);
				raySceneQuery = null;
			}

			if (grid != null)
			{
				grid.Destroy();
				grid = null;
			}

			if (sceneManager != null)
			{
				root.DestroySceneManager(sceneManager);
				sceneManager = null;
			}

			if (root != null)
			{
				root.Dispose();
				root = null;
				readyToRender = false;
			}
		}

		public void Dispose()
		{
			this.Cleanup();

			mThis = null;
		}

		public bool New()
		{
			this.Cleanup();
			return this.Initialize();
		}

		public bool Render()
		{
			if (readyToRender)
			{
				// TEST: 캐릭터 목표로 이동시키기
				UpdateObjectMove();

				return root.RenderOneFrame();
			}
			else
			{
				return false;
			}
		}

		public void OnResize()
		{
			renderWindow.WindowMovedOrResized();
		}

		public void RotateCameraYaw(float degree)
		{
			camera.Yaw(degree);
		}

		public void RotateCameraPitch(float degree)
		{
			camera.Pitch(degree);
		}

		public void TranslateCamera(float lookDegree, float upDegree, float rightDegree)
		{
			Vector3 lookDelta = camera.Direction * lookDegree;
			Vector3 upDelta = camera.Up * upDegree;
			Vector3 rightDelta = camera.Right * rightDegree;
			camera.Position = camera.Position + lookDelta + upDelta + rightDelta;
			axis.UpdatePosition();
		}

		public void AddObjectFromMeshName(SceneNode node, string meshName)
		{
			//string entityName = "";
			//do
			//{
			//    entityName = meshName + '_' + (nMeshCount++).ToString("00");
			//}
			//while (OgreHelper.IsEntityExist(entityName));

			// 엔티티 생성
			Entity entity = sceneManager.CreateEntity(meshName);
			entity.QueryFlags = QueryFlags.ENTITY;
			entity.RenderQueueGroup = (byte)RenderQueueGroupID.RENDER_QUEUE_MAIN;
			entity.CastShadows = true;	// 그림자 생기도록

			// 노드에 엔티티 붙임
			node.AttachObject(entity);
		}

		/// <summary>
		/// meshName에 해당하는 메시를 엔티티로 생성하여 node에 attach함
		/// </summary>
		/// <param name="node"></param>
		/// <param name="meshName"></param>
		public SceneNode AddObjectFromMeshName(string nodeName, string meshName)
		{
			//string groupName = strSNFResourceGroup;
			//MeshInfo meshInfo = SNFDocument.This.GetMeshInfo(meshName);

			//// 텍스처 파일 생성
			//foreach (string textureName in meshInfo.lstTextureName)
			//{
			//    FileInfo fileInfo = new FileInfo(strSNFResourceGroupFolder + '\\' + textureName);
			//    if (!fileInfo.Exists)
			//    {
			//        byte[] textureData = SNFDocument.This.GetTextureData(textureName);
			//        FileStream stream = new FileStream(strSNFResourceGroupFolder + '\\' + textureName, FileMode.Create, FileAccess.Write);
			//        BinaryWriter writer = new BinaryWriter(stream, Encoding.Default);
			//        writer.Write(textureData);
			//        writer.Close();
			//        stream.Close();
			//    }
			//}

			//// 재질 파일 생성
			//foreach (string materialName in meshInfo.lstMaterialName)
			//{
			//    if (!MaterialManager.Singleton.ResourceExists(materialName))
			//    {
			//        string strMaterialText = SNFDocument.This.GetMaterialText(materialName);
			//        StreamWriter writer = new StreamWriter(strSNFResourceGroupFolder + "\\snf.material", true, Encoding.Default);
			//        writer.Write(strMaterialText);
			//        writer.Close();
			//    }
			//}

			//// 생성한 텍스처/재질 파일 로드
			//ResourceGroupManager.Singleton.ClearResourceGroup(strSNFResourceGroup);
			//ResourceGroupManager.Singleton.InitialiseResourceGroup(strSNFResourceGroup);
			//ResourceGroupManager.Singleton.LoadResourceGroup(strSNFResourceGroup);

			//// 메쉬 로드
			//byte[] meshBuffer = SNFDocument.This.GetMeshData(meshName);
			//DataStreamPtr dsp = OgreHelper.BufferToDataStream(meshBuffer);
			//MeshSerializer meshSerializer = new MeshSerializer();
			//meshSerializer.Listener = MeshManager.Singleton.Listener;
			//MeshPtr mesh = MeshManager.Singleton.CreateManual(meshName, groupName);
			//meshSerializer.ImportMesh(dsp, mesh.Target);
			//dsp.Dispose();

			//// 엔티티 생성
			//Entity entity = sceneManager.CreateEntity(meshName + '_' + (nMeshCount++), meshName);
			//entity.QueryFlags = (uint)QueryFlags.ENTITY;

			//// 노드에 엔티티 붙임
			//node.AttachObject(entity);

			SceneNode node = sceneManager.RootSceneNode.CreateChildSceneNode(nodeName);

			AddObjectFromMeshName(node, meshName);

			return node;
		}

		/// <summary>
		/// 씬에 오브젝트 추가
		/// </summary>
		/// <param name="meshName">추가할 오브젝트 이름</param>
		public void SetDummyMesh(string meshName)
		{
			this.AddObjectFromMeshName(dummyNode, meshName);
		}

		public void ScaleSelectObject(float delta)
		{
			if (currentSelectNode != null)
			{
				float lastScale = currentSelectNode.GetScale().x;
				currentSelectNode.SetScale(new Vector3(lastScale + delta));

				// 자료구조에도 적용
				Object o = SNFDocument.This.GetObject(this.currentSelectNode.Name);
				o.ObjectScale = lastScale + delta;
			}
		}

		public void RotateSelectObject(float delta)
		{
			if (currentSelectNode != null)
			{
				Quaternion q = new Quaternion(new Radian(delta), Vector3.UNIT_Y);
				currentSelectNode.Rotate(q);

				// 3D 화살표도 회전
				axis.FollowRotation(GetSightNodeOfSelectedNode());

				// 자료구조에도 적용
				Object o = SNFDocument.This.GetObject(this.currentSelectNode.Name);
				o.ObjectRotation = currentSelectNode.Orientation;
				o.UpdateSightCamera();
			}
		}

		/// <summary>
		/// 현재 선택된 오브젝트를 이동 시킴
		/// </summary>
		/// <param name="position">이동 시킬 목표 위치</param>
		public void SetSelectObjectPosition(Vector3 targetPos)
		{
			if (currentSelectNode != null)
			{
				currentSelectNode.GetAttachedObject(0).QueryFlags = QueryFlags.EXCEPT;
				currentSelectNode.Position = targetPos;
				currentSelectNode.GetAttachedObject(0).QueryFlags = QueryFlags.ENTITY;

				// 3D 화살표도 이동
				axis.FollowTranslation(GetSightNodeOfSelectedNode());

				// 자료구조에도 적용
				Object o = SNFDocument.This.GetObject(this.currentSelectNode.Name);
				o.ObjectPosition = targetPos;
				o.UpdateSightCamera();
			}
		}

		public void LookAtObject(Object agent, Object target)
		{
			SceneNode targetNode = RootSceneNode.GetChild(target.ObjectName) as SceneNode;

			LookAtPosition(agent, targetNode.Position);
		}

		public void LookAtPosition(Object agent, Vector3 position)
		{
			SceneNode agentNode = RootSceneNode.GetChild(agent.ObjectName) as SceneNode;

			position.y = agentNode.Position.y;
			agentNode.LookAt(position, Node.TransformSpace.TS_WORLD);
			//agentNode.Rotate(agent.ObjectRotation);
			//Quaternion turnBack = new Quaternion(new Radian(Mogre.Math.PI), Vector3.UNIT_Y);
			//agentNode.Orientation *= turnBack;

			//agent.ObjectRotation = agentNode.Orientation;
			agent.UpdateSightCamera();
		}

		public void RemoveSelectObject()
		{
			SetAxisVisible(false);
			SetSightNodeOfSelectedNodeVisible(false);

			// 오브젝트 속성 제거
			SNFDocument.This.RemoveObject(currentSelectNode.Name);

			// 3D 공간에서 제거
			Entity entity = currentSelectNode.GetAttachedObject(0) as Entity;
			entity.DetachFromParent();

			sceneManager.DestroyEntity(entity);
			sceneManager.DestroySceneNode(currentSelectNode);

			currentSelectNode = null;
		}

		/// <summary>
		/// "Dummy" 씬 노드를 이동 시킴
		/// </summary>
		/// <param name="destination">이동 시킬 목표 위치</param>
		public void SetDummyNodePosition(float x, float y)
		{
			if (dummyNode != null)
			{
				dummyNode.GetAttachedObject(0).QueryFlags = QueryFlags.EXCEPT;
				dummyNode.Position = CalculateGroundPosition(x, y);
				dummyNode.GetAttachedObject(0).QueryFlags = QueryFlags.ENTITY;
			}
		}

		/// <summary>
		/// 다른 오브젝트와의 교차점을 찾거나 교차점이 없다면 y가 0.0f인 평면과의 교차점을 찾음
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		public Vector3 CalculateGroundPosition(float x, float y)
		{
			Ray ray = camera.GetCameraToViewportRay(x, y);

			Vector3 intersection = collisionTools.CalculateIntersection(ray, QueryFlags.ENTITY | QueryFlags.TERRAIN);
			if (intersection == Vector3.ZERO)
				return PickZeroPlane(x, y);

			return intersection;
		}

		public Vector3 CalculateGroundPosition(Vector3 rayStart)
		{
			Ray ray = new Ray(rayStart, Vector3.NEGATIVE_UNIT_Y);

			Vector3 intersection = collisionTools.CalculateIntersection(ray, QueryFlags.ENTITY | QueryFlags.TERRAIN);
			if (intersection == Vector3.ZERO)
				return PickZeroPlane(rayStart);

			return intersection;
		}

		/// <summary>
		/// CalculateGroundPositon과 거의 동일하지만, 현재 선택된 엔티티에 대한 교차점은 무시한다는 점이다.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="me"></param>
		/// <returns></returns>
		public Vector3 CalculateGroundPositionExceptMe(float x, float y)
		{
			Ray ray = camera.GetCameraToViewportRay(x, y);

			Vector3 intersection = collisionTools.CalculateIntersectionExceptMe(ray, QueryFlags.ENTITY | QueryFlags.TERRAIN, this.currentSelectNode.GetAttachedObject(0));
			// 아무 오브젝트도 피킹 못 했으면
			if (intersection == Vector3.ZERO)
			{
				return PickZeroPlane(x, y);	// 0높이 평면에서 교점 찾음
			}

			return intersection;
		}

		public Vector3 CalculateGroundPositionExceptMe(Vector3 rayStart, SceneNode node)
		{
			Ray ray = new Ray(rayStart, Vector3.NEGATIVE_UNIT_Y);

			Vector3 intersection = collisionTools.CalculateIntersectionExceptMe(ray, QueryFlags.ENTITY | QueryFlags.TERRAIN, node.GetAttachedObject(0));
			// 아무 오브젝트도 피킹 못 했으면
			if (intersection == Vector3.ZERO)
			{
				return PickZeroPlane(rayStart);	// 0높이 평면에서 교점 찾음
			}

			return intersection;
		}

		/// <summary>
		/// y가 0.0f인 평면과 마우스 좌표와의 교점 계산
		/// </summary>
		/// <param name="x">0.0f ~ 1.0f 사이의 가로 좌표</param>
		/// <param name="y">0.0f ~ 1.0f 사이의 세로 좌표</param>
		/// <returns></returns>
		private Vector3 PickZeroPlane(float x, float y)
		{
			// 윈도우 상의 마우스 위치로 뷰포트 상의 마우스 위치에서 발사되는 광선 생성
			Ray ray = camera.GetCameraToViewportRay(x, y);
			// y가 0.0f인 평면 생성
			Plane zeroPlane = new Plane(new Vector3(0.0f, 1.0f, 0.0f), 0.0f);
			// 평면과 광선 교차점 찾음
			Pair<bool, float> intersection = ray.Intersects(zeroPlane);

			if (!intersection.first)
				return Vector3.ZERO;

			// 교차점 리턴
			return ray.GetPoint(intersection.second);
		}

		private Vector3 PickZeroPlane(Vector3 rayStart)
		{
			Ray ray = new Ray(rayStart, Vector3.NEGATIVE_UNIT_Y);
			// y가 0.0f인 평면 생성
			Plane zeroPlane = new Plane(new Vector3(0.0f, 1.0f, 0.0f), 0.0f);
			// 평면과 광선 교차점 찾음
			Pair<bool, float> intersection = ray.Intersects(zeroPlane);

			if (!intersection.first)
				return Vector3.ZERO;

			// 교차점 리턴
			return ray.GetPoint(intersection.second);
		}

		/// <summary>
		/// 현재 더미 노드에 자식 오브젝트가 존재하는가
		/// </summary>
		/// <returns>존재하면 true</returns>
		public bool IsDummyNodeEmpty()
		{
			if (dummyNode.NumAttachedObjects() == 0)
				return false;

			return true;
		}

		/// <summary>
		/// 더미 노드에 연결된 자식 오브젝트 삭제
		/// </summary>
		public void EmptyDummyNode()
		{
			SceneNode.ObjectIterator itr = dummyNode.GetAttachedObjectIterator();
			while (itr.MoveNext())
			{
				sceneManager.DestroyEntity(itr.Current.Name);
			}

			dummyNode.DetachAllObjects();
		}

		/// <summary>
		/// 더미 노드에 연결된 오브젝트를 단독 노드로 이동
		/// </summary>
		/// <param name="objectName"></param>
		public void MoveDummyObjectTo(string objectName)
		{
			MovableObject mesh = dummyNode.GetAttachedObject(0);
			Vector3 dummyPosition = dummyNode.Position;
			dummyNode.DetachAllObjects();

			SceneNode newNode = sceneManager.RootSceneNode.CreateChildSceneNode(objectName, dummyPosition);
			newNode.AttachObject(mesh);
		}

		public Entity TryPick(float x, float y)
		{
			Ray ray = camera.GetCameraToViewportRay(x, y);
			CollisionTools.RaycastResult rr = collisionTools.RaycastFromPoint(ray.Origin, ray.Direction, QueryFlags.ENTITY | QueryFlags.TERRAIN | QueryFlags.REGION | QueryFlags.AXIS);
			if (rr == null)
			{
				return null;
			}
			else
			{
				return rr.Target;
			}
		}

		public Entity TryPickTerrain(Vector3 rayStart)
		{
			Ray ray = new Ray(rayStart, Vector3.NEGATIVE_UNIT_Y);
			CollisionTools.RaycastResult rr = collisionTools.RaycastFromPoint(ray.Origin, ray.Direction, QueryFlags.TERRAIN);
			if (rr == null)
			{
				return null;
			}
			else
			{
				return rr.Target;
			}
		}

		/// <summary>
		/// 오브젝트 선택
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns>선택된 오브젝트가 있으면 true, 없으면 false 리턴</returns>
		public bool PickObject(float x, float y)
		{
			if (currentSelectNode != null)
			{
				UnpickObject();
			}

			Ray ray = camera.GetCameraToViewportRay(x, y);
			CollisionTools.RaycastResult rr = collisionTools.RaycastFromPoint(ray.Origin, ray.Direction, QueryFlags.ENTITY | QueryFlags.TERRAIN);
			if (rr != null)
			{
				if (rr.Target != null)
				{
					SelectObject(rr.Target.ParentSceneNode);

					return true;
				}
			}

			return false;
		}

		public void UnpickObject()
		{
			if (currentSelectNode != null)
			{
				currentSelectNode.ShowBoundingBox = false;
				SetAxisVisible(false);
				SetSightNodeOfSelectedNodeVisible(false);

				currentSelectNode = null;
			}
		}

		private void SelectObject(SceneNode node)
		{
			UnpickObject();

			currentSelectNode = node;

			node.ShowBoundingBox = true;
			SetAxisVisible(true);
			SetSightNodeOfSelectedNodeVisible(true);
		}

		public void SelectObject(Object obj)
		{
			SelectObject((SceneNode)RootSceneNode.GetChild(obj.ObjectName));
		}

		public Vector3 GetObjectPosition(string objectName)
		{
			Node.ChildNodeIterator itr = sceneManager.RootSceneNode.GetChildIterator();
			while (itr.MoveNext())
			{
				if (itr.Current.Name == objectName)
					return itr.Current.Position;
			}

			return Vector3.ZERO;
		}

		/// <summary>
		/// meshName에 해당하는 메시로 만들어진 모든 오브젝트 삭제
		/// </summary>
		/// <param name="meshName"></param>
		//public void RemoveObjectByMeshName(string meshName)
		//{
		//    this.RemoveObjectByMeshName(sceneManager.RootSceneNode, meshName);
		//}

		//private void RemoveObjectByMeshName(SceneNode node, string meshName)
		//{
		//    SceneNode.ObjectIterator itrObject = node.GetAttachedObjectIterator();
		//    while (itrObject.MoveNext())  // node안에서 meshName을 포함하는 모든 오브젝트 제거
		//    {
		//        string name = itrObject.Current.Name;
		//        if (name.Contains('_'))
		//        {
		//            string maybeMeshName = name.Substring(0, name.LastIndexOf('_'));
		//            if (maybeMeshName == meshName)
		//                itrObject.Current.DetachFromParent();
		//        }
		//    }

		//    // 자식 노드에서도 재귀적으로 수행
		//    Node.ChildNodeIterator itrNode = node.GetChildIterator();
		//    while (itrNode.MoveNext())
		//    {
		//        this.RemoveObjectByMeshName((SceneNode)itrNode.Current, meshName);
		//    }
		//}

		public void SetAxisVisible(bool visible)
		{
			if (visible && currentSelectNode != null)
			{
				Object obj = SNFDocument.This.GetObject(currentSelectNode.Name);
				if (obj.HasSight)
				{
					axis.SetVisible(visible);
					SceneNode sightNode = GetSightNodeOfSelectedNode();
					axis.Follow(sightNode);
				}
			}
			else
			{
				axis.SetVisible(false);
			}
		}

		public void SetSightNodeVisible(Object obj, bool visible)
		{
			if (obj != null && obj.HasSight)
			{
				SceneNode objectNode = RootSceneNode.GetChild(obj.ObjectName) as SceneNode;
				SceneNode sightNode = GetSightNode(objectNode);
				sightNode.SetVisible(visible);
			}
		}

		public void SetRegionVisible(Region region, bool visible)
		{
			if (region != null)
			{
				region.SetRegionVisible(visible);
			}
		}

		public void SetSightNodeOfSelectedNodeVisible(bool visible)
		{
			if (currentSelectNode == null)
			{
				return;
			}

			Object obj = SNFDocument.This.GetObject(currentSelectNode.Name);
			if (obj.HasSight)
			{
				SceneNode sightNode = GetSightNodeOfSelectedNode();
				sightNode.SetVisible(visible);
			}
		}

		public SceneNode GetSightNodeOfSelectedNode()
		{
			return GetSightNode(currentSelectNode);
		}

		public SceneNode GetSightNode(SceneNode parent)
		{
			if (parent == null || !sceneManager.HasSceneNode("SightNode_" + parent.Name))
			{
				return null;
			}

			return parent.GetChild("SightNode_" + parent.Name) as SceneNode;
		}

		public void ScaleSightNodeOfSelectedNode(Vector2 mouseVector)
		{
			SceneNode sightNode = GetSightNodeOfSelectedNode();
			Object obj = SNFDocument.This.GetObject(currentSelectNode.Name);

			Vector3 scale;
			// 3D 화살표의 어느 방향을 잡고 드래그하는지에 따라 시야 크기 조절
			// x방향
			if (mSelectedAxisType == AxisType.X)
			{
				scale = new Vector3(mouseVector.x * OgreManager.This.TimeDelta * 100, 1, 1);
				sightNode.Scale(scale);
				obj.SightWidth += scale.x;
			}
			// y방향
			else if (mSelectedAxisType == AxisType.Y)
			{
				scale = new Vector3(1, -mouseVector.y * OgreManager.This.TimeDelta * 100, 1);
				sightNode.Scale(scale);
				obj.SightHeight += scale.y;
			}
			// z방향
			else if (mSelectedAxisType == AxisType.Z)
			{
				scale = new Vector3(1, 1, mouseVector.x * OgreManager.This.TimeDelta * 100);
				sightNode.Scale(scale);
				obj.SightRange += scale.z;
			}

			obj.UpdateSightCamera();
		}

		public void RotateSightNodeOfSelectedNode(Vector2 mouseVector, float delta)
		{
			SceneNode sightNode = GetSightNodeOfSelectedNode();

			// 3D 화살표의 어느 방향을 잡고 드래그하는지에 따라 시야 회전시킴
			// x축 기준 회전
			if (mSelectedAxisType == AxisType.X)
			{
				Quaternion q = new Quaternion(new Radian(mouseVector.y * delta), Vector3.UNIT_X);
				sightNode.Rotate(q);
			}
			// y축 기준 회전
			else if (mSelectedAxisType == AxisType.Y)
			{
				Quaternion q = new Quaternion(new Radian(mouseVector.x * delta), Vector3.UNIT_Y);
				sightNode.Rotate(q);
			}
			// z축 기준 회전
			else if (mSelectedAxisType == AxisType.Z)
			{
				Quaternion q = new Quaternion(new Radian(mouseVector.y * delta), Vector3.UNIT_Z);
				sightNode.Rotate(q);
			}

			// 3D 화살표도 시야화 함께 회전
			axis.FollowRotation(sightNode);

			// 시야 정보 변경을 자료구조에도 업데이트
			Object obj = SNFDocument.This.GetObject(currentSelectNode.Name);
			obj.SightRotation = sightNode.Orientation;
			obj.UpdateSightCamera();
		}

		public void TranslateSightNodeOfSelectedNode(Vector2 mouseVector)
		{
			SceneNode sightNode = GetSightNodeOfSelectedNode();

			// 3D 화살표의 어느 방향을 잡고 드래그하는지에 따라 시야 이동시킴
			// x방향
			if (mSelectedAxisType == AxisType.X)
			{
				sightNode.Translate(mouseVector.x * OgreManager.This.TimeDelta * 100, 0, 0);
			}
			// y방향
			else if (mSelectedAxisType == AxisType.Y)
			{
				sightNode.Translate(0, -mouseVector.y * OgreManager.This.TimeDelta * 100, 0);
			}
			// z방향
			else if (mSelectedAxisType == AxisType.Z)
			{
				sightNode.Translate(0, 0, mouseVector.x * OgreManager.This.TimeDelta * 100);
			}

			// 3D 화살표도 시야와 함께 이동시킴
			axis.FollowTranslation(sightNode);

			// 시야 정보 변경을 자료구조에도 업데이트
			Object obj = SNFDocument.This.GetObject(currentSelectNode.Name);
			obj.SightPosition = sightNode.Position;
			obj.UpdateSightCamera();

			//float mouseVectorLength = mouseVector.Length;
			//Vector2 normalizedMouseVector = mouseVector.NormalisedCopy;

			//SceneNode sightNode = OgreManager.This.GetSightNodeOfSelectedNode();

			//// 3D 화살표의 어느 방향으로 마우스가 움직이고 있는지 확인하기 위하여,
			//// 우선 현재 카메라 상에서 3D 화살표의 방향을 2D 화면에 투영시켜 얻어낸다.
			//Vector3 axisPosition = sightNode._getDerivedPosition();
			//Quaternion axisRotation = sightNode._getDerivedOrientation();
			//Vector3 localXAxis = (axisRotation * new Vector3(50, 0, 0)) + axisPosition;
			//Vector3 localYAxis = (axisRotation * new Vector3(0, 50, 0)) + axisPosition;
			//Vector3 localZAxis = (axisRotation * new Vector3(0, 0, 50)) + axisPosition;

			//Vector2 axisScreenPoint = OgreManager.This.GetScreenPoint(axisPosition);

			//Vector2 xAxisVector = (OgreManager.This.GetScreenPoint(localXAxis) - axisScreenPoint).NormalisedCopy;	// 3D 화살표의 x축 방향
			//Vector2 yAxisVector = (OgreManager.This.GetScreenPoint(localYAxis) - axisScreenPoint).NormalisedCopy;	// 3D 화살표의 y축 방향
			//Vector2 zAxisVector = (OgreManager.This.GetScreenPoint(localZAxis) - axisScreenPoint).NormalisedCopy;	// 3D 화살표의 z축 방향

			//// 3D 화살표의 어느 방향으로 마우스가 가장 가깝게 움직이고 있는지 파악
			//float[] angleBetween = new float[3];
			//angleBetween[0] = OgreHelper.CalculateAngle(mouseVector, xAxisVector);
			//angleBetween[1] = OgreHelper.CalculateAngle(mouseVector, yAxisVector);
			//angleBetween[2] = OgreHelper.CalculateAngle(mouseVector, zAxisVector);

			//// 마우스 방향 벡터와의 사이각이 가장 작은 3D 화살표 방향은?
			//float minAngle = float.MaxValue;
			//int nearestAxisIndex = -1;
			//for (int i = 0; i < 3; ++i)
			//{
			//    if (angleBetween[i] < minAngle)
			//    {
			//        minAngle = angleBetween[i];
			//        nearestAxisIndex = i;
			//    }
			//}

			//// 제일 가까운 방향을 알아냈으므로 그 방향으로 시야를 이동시키자!
			//// x방향
			//if (mSelectedAxisType == AxisType.X && nearestAxisIndex == 0)
			//{
			//    sightNode.Translate(new Vector3(mouseVectorLength * OgreManager.This.TimeDelta * 100, 0, 0));
			//}
			//// y방향
			//else if (mSelectedAxisType == AxisType.Y &&nearestAxisIndex == 1)
			//{
			//    sightNode.Translate(new Vector3(0, mouseVectorLength * OgreManager.This.TimeDelta * 100, 0));
			//}
			//// z방향
			//else if (mSelectedAxisType == AxisType.Z &&nearestAxisIndex == 2)
			//{
			//    sightNode.Translate(new Vector3(0, 0, mouseVectorLength * OgreManager.This.TimeDelta * 100));
			//}

			//// 3D 화살표도 시야와 함께 이동시킴
			//axis.SetPosition(sightNode.Position);

			// TEST:
			//SceneNode testNode01 = OgreManager.This.RootSceneNode.GetChild("TestNode01") as SceneNode;
			//SceneNode testNode02 = OgreManager.This.RootSceneNode.GetChild("TestNode02") as SceneNode;
			//SceneNode testNode03 = OgreManager.This.RootSceneNode.GetChild("TestNode03") as SceneNode;
			//testNode01.Position = localXAxis;
			//testNode02.Position = localYAxis;
			//testNode03.Position = localZAxis;
		}

		public Vector2 GetScreenPoint(Vector3 point3D)
		{
			// Is the camera facing that point? If not, return false
			Plane cameraPlane = new Plane(camera.DerivedOrientation.ZAxis, camera.DerivedPosition);
			if (cameraPlane.GetSide(point3D) != Plane.Side.NEGATIVE_SIDE)
			{
				return Vector2.ZERO;
			}
			// Transform the 3D point into screen space
			point3D = camera.ProjectionMatrix * (camera.ViewMatrix * point3D);

			// Transform from coordinate space [-1, 1] to [0, 1] and update in-value
			Vector2 result = new Vector2();
			result.x = (point3D.x / 2) + 0.5f;
			result.y = 1 - ((point3D.y / 2) + 0.5f);

			return result;
		}

		public void SetSelectedAxisType(Entity axisEntity)
		{
			mSelectedAxisType = axis.GetAxisType(axisEntity);
		}

		public Vector3 CalculateScreenPosition(Vector3 worldPosition)
		{
			Vector3 cameraToWorldVector = (worldPosition - camera.RealPosition).NormalisedCopy;
			return camera.RealPosition + cameraToWorldVector;
		}

		public void ShowSightRegion(bool show)
		{
			var objEnumerator = SNFDocument.This.GetObjectEnumerator();
			while(objEnumerator.MoveNext())
			{
				SetSightNodeVisible(objEnumerator.Current.Value, show);
			}

			var regionEnumerator = SNFDocument.This.GetRegionEnumerator();
			while (regionEnumerator.MoveNext())
			{
				SetRegionVisible(regionEnumerator.Current.Value, show);
			}
		}

		private List<SceneNode> mMovingObjectList = new List<SceneNode>();
		private Dictionary<SceneNode, Vector3[]> mPathList = new Dictionary<SceneNode, Vector3[]>();
		private Dictionary<SceneNode, int> mTargetPathIndexList = new Dictionary<SceneNode, int>();
		private Dictionary<SceneNode, float> mMoveSpeedList = new Dictionary<SceneNode, float>();
		private List<SceneNode> mMoveEndObjects = new List<SceneNode>();

		private Object GetTerrainObjectUnder(Vector3 position)
		{
			Entity terrainEntity = TryPickTerrain(position);
			if (terrainEntity == null)
			{
				return null;
			}

			SceneNode terrainSceneNode = terrainEntity.ParentSceneNode;
			Object terrainObj = SNFDocument.This.GetObject(terrainSceneNode.Name);
			if (terrainObj.ObjectType == ObjectType.Terrain)
			{
				return terrainObj;
			}

			return null;
		}

		public void MoveObject(Object obj, Vector3 endPos, float speed)
		{
			SceneNode objNode = RootSceneNode.GetChild(obj.ObjectName) as SceneNode;
			StopObjectMove(objNode);

			// 우선 자신이 밝고 있는 지형 오브젝트가 뭔지 알아냄
			Object terrainObj = GetTerrainObjectUnder(objNode.Position);
			if (terrainObj == null)
			{
				return;
			}

			List<string> messages;
			Vector3[] path = terrainObj.FindPathOnTerrain(objNode.Position, endPos, out messages);

			mMovingObjectList.Add(objNode);
			mPathList.Add(objNode, path);
			mTargetPathIndexList.Add(objNode, 0);
			mMoveSpeedList.Add(objNode, speed);
		}

		private void UpdateObjectMove()
		{
			foreach(SceneNode objectNode in mMovingObjectList)
			{
				Vector3[] path = mPathList[objectNode];
				if (path != null)
				{
					Object o = SNFDocument.This.GetObject(objectNode.Name);
					// Object에서 agentRadius로 준 값의 반 안에 들어오면
					if ((objectNode.Position - path[mTargetPathIndexList[objectNode]]).SquaredLength <= 20/2)
					{
						++mTargetPathIndexList[objectNode];
						// 최종 위치까지 도달했으면
						if (mTargetPathIndexList[objectNode] == path.Length)
						{
							mMoveEndObjects.Add(objectNode);
							break;
						}

						// 다음 지점을 향해 오브젝트를 회전시킴
						Vector3 targetPoint = path[mTargetPathIndexList[objectNode]];
						LookAtPosition(o, targetPoint);
					}

					Vector3 direction = (path[mTargetPathIndexList[objectNode]] - objectNode.Position).NormalisedCopy;
					objectNode.Position += direction * (OgreManager.This.TimeDelta > 1 ? 1 : OgreManager.This.TimeDelta) * 100 * mMoveSpeedList[objectNode];

					o.UpdateSightCamera();
				}
			}

			// 이동 완료된 오브젝트들은 이동 목록에서 제거
			if (mMoveEndObjects.Count != 0)
			{
				foreach (SceneNode obj in mMoveEndObjects)
				{
					mMovingObjectList.Remove(obj);
					mPathList.Remove(obj);
					mTargetPathIndexList.Remove(obj);
					mMoveSpeedList.Remove(obj);
				}

				mMoveEndObjects.Clear();
			}
		}

		public void StopObjectMove(SceneNode objectNode)
		{
			if (mMovingObjectList.Contains(objectNode))
			{
				mMovingObjectList.Remove(objectNode);
				mPathList.Remove(objectNode);
				mTargetPathIndexList.Remove(objectNode);
				mMoveSpeedList.Remove(objectNode);
			}
		}

		public void StopAllObjectMove()
		{
			mMovingObjectList.Clear();
			mPathList.Clear();
			mTargetPathIndexList.Clear();
			mMoveSpeedList.Clear();
		}

		public void PlayAnimation(Object obj, string animationName)
		{
			Entity entity = ((SceneNode)RootSceneNode.GetChild(obj.ObjectName)).GetAttachedObject(0) as Entity;
			if (entity.AllAnimationStates.HasAnimationState(animationName))
			{
				AnimationState animationState = entity.AllAnimationStates.GetAnimationState(animationName);
				animationState.TimePosition = 0;
				animationState.Enabled = true;
				mPlayingAnimationList.Add(animationState);
			}
		}

		public void StopAnimation(Object obj, string animationName)
		{
			Entity entity = ((SceneNode)RootSceneNode.GetChild(obj.ObjectName)).GetAttachedObject(0) as Entity;
			if (entity.AllAnimationStates.HasAnimationState(animationName))
			{
				AnimationState animationState = entity.AllAnimationStates.GetAnimationState(animationName);
				animationState.Enabled = false;
				mPlayingAnimationList.Remove(animationState);
			}
		}

		public AnimationState GetAnimationState(Object obj, string animationName)
		{
			Entity entity = ((SceneNode)RootSceneNode.GetChild(obj.ObjectName)).GetAttachedObject(0) as Entity;
			if (entity.AllAnimationStates.HasAnimationState(animationName))
			{
				return entity.AllAnimationStates.GetAnimationState(animationName);
			}
			else
			{
				return null;
			}
		}

		public void ShowObject(Object obj, bool show)
		{
			if (show)
			{
				RootSceneNode.AddChild(sceneManager.GetSceneNode(obj.ObjectName));
			}
			else
			{
				RootSceneNode.RemoveChild(sceneManager.GetSceneNode(obj.ObjectName));
			}
		}

		public void SetVisibleObject(Object obj, bool visible)
		{
			(RootSceneNode.GetChild(obj.ObjectName) as SceneNode).SetVisible(visible);
		}

		public void RestoreObject(Object obj)
		{
			SceneNode objNode = sceneManager.GetSceneNode(obj.ObjectName);
			objNode.Position = obj.ObjectPosition;	// 위치 복구
			objNode.Orientation = obj.ObjectRotation;	// 회전 복구
			if (objNode.ParentSceneNode == null)	// 사라짐 복구
			{
				ShowObject(obj, true);
			}
		}

		public void StopAllAnimation(Object obj)
		{
			foreach (var value in mPlayingAnimationList)
			{
				value.Enabled = false;
				value.TimePosition = 0;
			}

			mPlayingAnimationList.Clear();
		}

		private Vector3 mLastCameraPosition;
		private Quaternion mLastCameraOrientation;
		private float mOriginalHeight = 0;

		public void AttachCameraToObject(Object obj)
		{
			mLastCameraPosition = camera.Position;
			mLastCameraOrientation = camera.Orientation;

			SceneNode objectNode = RootSceneNode.GetChild(obj.ObjectName) as SceneNode;
			SceneNode sightNode = GetSightNode(objectNode);
			if (sightNode == null)
			{
				MessageBox.Show("플레이어 오브젝트는 반드시 시야가 있어야 합니다.", "경고", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			camera.Position = sightNode._getDerivedPosition();

			Vector3 cameraGroundPosition = CalculateGroundPosition(camera.Position);
			mOriginalHeight = camera.Position.y - cameraGroundPosition.y;
		}

		public void RestoreCamera()
		{
			camera.Position = mLastCameraPosition;
			camera.Orientation = mLastCameraOrientation;
		}

		public void PlayerMoveForward(float timeDelta)
		{
			Vector3 forward = camera.Direction;
			forward.y = 0;
			forward = forward.NormalisedCopy;
			Vector3 lookDelta = forward * timeDelta * 100;
			Vector3 targetPosition = camera.Position + lookDelta;

			SceneNode objectNode = RootSceneNode.GetChild(FSMPlayer.This.GetPlayerObject().ObjectName) as SceneNode;
			targetPosition = CalculateGroundPositionExceptMe(targetPosition, objectNode);

			Object terrainObj = GetTerrainObjectUnder(new Vector3(targetPosition[0], targetPosition[1]+10, targetPosition[2]));
			if (terrainObj != null && !terrainObj.IsOnNavMesh(targetPosition))
			{
				return;
			}

			objectNode.Position = targetPosition;

			targetPosition.y += mOriginalHeight;

			camera.Position = targetPosition;
		}

		public void PlayerMoveBack(float timeDelta)
		{
			Vector3 forward = camera.Direction;
			forward.y = 0;
			forward = forward.NormalisedCopy;
			Vector3 lookDelta = -forward * timeDelta * 100;
			Vector3 targetPosition = camera.Position + lookDelta;

			SceneNode objectNode = RootSceneNode.GetChild(FSMPlayer.This.GetPlayerObject().ObjectName) as SceneNode;
			targetPosition = CalculateGroundPositionExceptMe(targetPosition, objectNode);

			Object terrainObj = GetTerrainObjectUnder(new Vector3(targetPosition[0], targetPosition[1]+10, targetPosition[2]));
			if (terrainObj != null && !terrainObj.IsOnNavMesh(targetPosition))
			{
				return;
			}

			objectNode.Position = targetPosition;

			targetPosition.y += mOriginalHeight;

			camera.Position = targetPosition;
		}

		public void PlayerMoveLeft(float timeDelta)
		{
			Vector3 lookDelta = -camera.Right * timeDelta * 100;
			Vector3 targetPosition = camera.Position + lookDelta;

			SceneNode objectNode = RootSceneNode.GetChild(FSMPlayer.This.GetPlayerObject().ObjectName) as SceneNode;
			targetPosition = CalculateGroundPositionExceptMe(targetPosition, objectNode);

			Object terrainObj = GetTerrainObjectUnder(new Vector3(targetPosition[0], targetPosition[1]+10, targetPosition[2]));
			if (terrainObj != null && !terrainObj.IsOnNavMesh(targetPosition))
			{
				return;
			}

			objectNode.Position = targetPosition;

			targetPosition.y += mOriginalHeight;

			camera.Position = targetPosition;
		}

		public void PlayerMoveRight(float timeDelta)
		{
			Vector3 lookDelta = camera.Right * timeDelta * 100;
			Vector3 targetPosition = camera.Position + lookDelta;

			SceneNode objectNode = RootSceneNode.GetChild(FSMPlayer.This.GetPlayerObject().ObjectName) as SceneNode;
			targetPosition = CalculateGroundPositionExceptMe(targetPosition, objectNode);

			Object terrainObj = GetTerrainObjectUnder(new Vector3(targetPosition[0], targetPosition[1]+10, targetPosition[2]));
			if (terrainObj != null && !terrainObj.IsOnNavMesh(targetPosition))
			{
				return;
			}

			objectNode.Position = targetPosition;

			targetPosition.y += mOriginalHeight;

			camera.Position = targetPosition;
		}
	}
}

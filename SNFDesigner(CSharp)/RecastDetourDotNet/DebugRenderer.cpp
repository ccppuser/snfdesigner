#include "stdafx.h"

#include "DebugRenderer.h"

using namespace RecastDetourDotNet;

int DebugDraw::mInstanceCount = 0;

DebugDraw::DebugDraw()
{
	mInstanceId = mInstanceCount++;
}

DebugDraw::~DebugDraw()
{
}

void DebugDraw::depthMask(bool state)
{
	DebugRendererMiddleWare::This->GetDebugRenderer(mInstanceId)->DepthMask(state);
}

void DebugDraw::texture(bool state)
{
	DebugRendererMiddleWare::This->GetDebugRenderer(mInstanceId)->Texture(state);
}

void DebugDraw::begin(duDebugDrawPrimitives prim, float size)
{
	DebugRendererMiddleWare::This->GetDebugRenderer(mInstanceId)->Begin((DebugDrawPrimitives)prim, size);
}

void DebugDraw::vertex(const float* pos, unsigned int color)
{
	DebugRendererMiddleWare::This->GetDebugRenderer(mInstanceId)->Vertex(pos[0], pos[1], pos[2], color);
}

void DebugDraw::vertex(const float x, const float y, const float z, unsigned int color)
{
	DebugRendererMiddleWare::This->GetDebugRenderer(mInstanceId)->Vertex(x, y, z, color);
}

void DebugDraw::vertex(const float* pos, unsigned int color, const float* uv)
{
	DebugRendererMiddleWare::This->GetDebugRenderer(mInstanceId)->Vertex(pos[0], pos[1], pos[2], color, uv[0], uv[1]);
}

void DebugDraw::vertex(const float x, const float y, const float z, unsigned int color, const float u, const float v)
{
	DebugRendererMiddleWare::This->GetDebugRenderer(mInstanceId)->Vertex(x, y, z, color, u, v);
}

void DebugDraw::end()
{
	DebugRendererMiddleWare::This->GetDebugRenderer(mInstanceId)->End();
}

DebugRenderer::DebugRenderer()
{
	mDebugDraw = new DebugDraw();
	DebugRendererMiddleWare::This->AddDebugRenderer(mDebugDraw->mInstanceId, this);
}
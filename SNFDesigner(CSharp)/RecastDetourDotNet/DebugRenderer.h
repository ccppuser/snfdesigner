#pragma once

#include "DebugDraw.h"
//#include <map>

namespace RecastDetourDotNet {

	using namespace System;
	using namespace System::Collections::Generic;
	using namespace System::Runtime::InteropServices;

	class DebugDraw : public duDebugDraw
	{
	private:

		static int mInstanceCount;

	public:

		int mInstanceId;

		DebugDraw();
		virtual ~DebugDraw();
		virtual void depthMask(bool state);
		virtual void texture(bool state);
		virtual void begin(duDebugDrawPrimitives prim, float size = 1.0f);
		virtual void vertex(const float* pos, unsigned int color);
		virtual void vertex(const float x, const float y, const float z, unsigned int color);
		virtual void vertex(const float* pos, unsigned int color, const float* uv);		
		virtual void vertex(const float x, const float y, const float z, unsigned int color, const float u, const float v);
		virtual void end();
	};

	public enum class DebugDrawPrimitives
	{
		DU_DRAW_POINTS,
		DU_DRAW_LINES,
		DU_DRAW_TRIS,
		DU_DRAW_QUADS,
	};

	public ref class DebugRenderer abstract
	{
	private:

		DebugDraw *mDebugDraw;

	public:

		property DebugDraw *DD { DebugDraw *get() { return mDebugDraw; } }

	public:

		DebugRenderer();

		virtual ~DebugRenderer()
		{
			DebugRenderer::!DebugRenderer();
		}

		!DebugRenderer()
		{
			delete mDebugDraw;
		}

		virtual void DepthMask(bool state) abstract;

		virtual void Texture(bool state) abstract;

		/// Begin drawing primitives.
		///  @param prim [in] primitive type to draw, one of rcDebugDrawPrimitives.
		///  @param size [in] size of a primitive, applies to point size and line width only.
		virtual void Begin(DebugDrawPrimitives prim, float size) abstract;

		/// Submit a vertex
		///  @param pos [in] position of the verts.
		///  @param color [in] color of the verts.
		//virtual void Vertex(const float* pos, unsigned int color) abstract;

		/// Submit a vertex
		///  @param x,y,z [in] position of the verts.
		///  @param color [in] color of the verts.
		virtual void Vertex(const float x, const float y, const float z, unsigned int color) abstract;

		/// Submit a vertex
		///  @param pos [in] position of the verts.
		///  @param color [in] color of the verts.
		//virtual void Vertex(const float* pos, unsigned int color, const float* uv) abstract;
		
		/// Submit a vertex
		///  @param x,y,z [in] position of the verts.
		///  @param color [in] color of the verts.
		virtual void Vertex(const float x, const float y, const float z, unsigned int color, const float u, const float v) abstract;
		
		/// End drawing primitives.
		virtual void End() abstract;

	protected:

		void IntToColor(int i, [Out] float %r, [Out] float %g, [Out] float %b)
		{
			float color[3];
			duIntToCol(i, color);
			r = color[0];
			g = color[1];
			b = color[2];
		}
	};

	ref class DebugRendererMiddleWare
	{
	private:

		static DebugRendererMiddleWare ^mThis = nullptr;
		Dictionary<int, DebugRenderer ^> ^mDebugRendererDic;

	public:

		property static DebugRendererMiddleWare ^This { DebugRendererMiddleWare ^get() { if(mThis == nullptr) { mThis = gcnew DebugRendererMiddleWare(); } return mThis; } }

	public:

		DebugRendererMiddleWare()
		{
			mDebugRendererDic = gcnew Dictionary<int, DebugRenderer ^>();
		}

		~DebugRendererMiddleWare()
		{
			DebugRendererMiddleWare::!DebugRendererMiddleWare();
		}

		!DebugRendererMiddleWare()
		{
		}

		void AddDebugRenderer(int ddId, DebugRenderer ^dr)
		{
			mDebugRendererDic->Add(ddId, dr);
		}

		DebugRenderer ^GetDebugRenderer(int ddId)
		{
			return mDebugRendererDic[ddId];
		}
	};
}

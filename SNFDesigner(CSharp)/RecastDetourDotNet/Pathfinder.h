// RecastDetourDotNet.h

#pragma once

#include <string.h>
#include <math.h>

#include "Recast.h"
#include "DetourCommon.h"
#include "DetourNavMesh.h"
#include "DetourNavMeshBuilder.h"
#include "DetourNavMeshQuery.h"
#include "DetourDebugDraw.h"

#include "DebugRenderer.h"

namespace RecastDetourDotNet {

	using namespace System;
	using namespace System::Collections::Generic;
	using namespace System::Runtime::InteropServices;

	public ref class Pathfinder
	{
	private:

		static const int MAX_POLYS = 256;

//		static Pathfinder ^mThis = nullptr;
		rcContext *mContext;
		dtNavMesh *mNavMesh;
		dtNavMeshQuery *mNavQuery;
		dtQueryFilter *mFilter;
		float *mExtents;
		dtPolyRef *mPolys;
		float *mStraightPath;
		DebugRenderer ^mDebugRenderer;

	public:

//		property static Pathfinder ^This { Pathfinder ^get() { if(mThis == nullptr) { mThis = gcnew Pathfinder(); } return mThis; } }

	public:

		Pathfinder()
		{
			mContext = new rcContext(false);
			mNavMesh = nullptr;
			mNavQuery = dtAllocNavMeshQuery();
			mFilter = new dtQueryFilter();
			mFilter->setAreaCost(RC_WALKABLE_AREA, 1);
			mFilter->setIncludeFlags(RC_WALKABLE_AREA);
			mExtents = new float[3];
			mPolys = new dtPolyRef[MAX_POLYS];
			mStraightPath = new float[MAX_POLYS * 3];
		}

		~Pathfinder()
		{
			Pathfinder::!Pathfinder();
		}

		!Pathfinder()
		{
			Cleanup();
		}

		void Cleanup()
		{
			delete mContext;
			mContext = nullptr;
			if(mNavMesh != nullptr)
			{
				dtFreeNavMesh(mNavMesh);
				mNavMesh = nullptr;
			}
			if(mNavQuery != nullptr)
			{
				dtFreeNavMeshQuery(mNavQuery);
				mNavQuery = nullptr;
			}
			delete [] mExtents;
			mExtents = nullptr;
			delete [] mPolys;
			mPolys = nullptr;
			delete [] mStraightPath;
			mStraightPath = nullptr;
		}

		void SetDebugRenderer(DebugRenderer ^debugRenderer)
		{
			mDebugRenderer = debugRenderer;
		}

		void DebugDrawNavMesh()
		{
			duDebugDrawNavMesh(mDebugRenderer->DD, *mNavMesh, DU_DRAWNAVMESH_OFFMESHCONS | DU_DRAWNAVMESH_CLOSEDLIST | DU_DRAWNAVMESH_COLOR_TILES);
		}

		bool CreateNavigationMesh(array<float> ^vertices, array<int> ^indices,
			float cellSize, float cellHeight,
			float agentMaxSlope, float agentHeight, float agentMaxClimb, float agentRadius,
			float edgeMaxLen, float edgeMaxError,
			float regionMinSize, float regionMergeSize,
			int maxVertsPerPoly,
			float detailSampleDist, float detailSampleMaxError,
			bool monotonePartitioning,
			[Out] List<String ^> ^%messages)
		{
			messages = gcnew List<String ^>();

			dtVset(mExtents, agentRadius * 2, agentRadius * 1.5f, agentRadius * 2);

			int nverts = vertices->Length / 3;
			float *verts = new float[vertices->Length];
			for(int i = 0; i < vertices->Length; ++i)
			{
				verts[i] = vertices[i];
			}
			int ntris = indices->Length / 3;
			int *tris = new int[indices->Length];
			for(int i = 0; i < indices->Length; ++i)
			{
				tris[i] = indices[i];
			}

			//
			// Step 1. Initialize build config.
			//

			// Init build configuration from GUI
			rcConfig cfg;
			memset(&cfg, 0, sizeof(cfg));
			cfg.cs = cellSize;
			cfg.ch = cellHeight;
			cfg.walkableSlopeAngle = agentMaxSlope;
			cfg.walkableHeight = (int)ceilf(agentHeight / cfg.ch);
			cfg.walkableClimb = (int)floorf(agentMaxClimb / cfg.ch);
			cfg.walkableRadius = (int)ceilf(agentRadius / cfg.cs);
			cfg.maxEdgeLen = (int)(edgeMaxLen / cellSize);
			cfg.maxSimplificationError = edgeMaxError;
			cfg.minRegionArea = (int)rcSqr(regionMinSize);
			cfg.mergeRegionArea = (int)rcSqr(regionMergeSize);
			cfg.maxVertsPerPoly = maxVertsPerPoly;
			cfg.detailSampleDist = detailSampleDist < 0.9f ? 0 : cellSize * detailSampleDist;
			cfg.detailSampleMaxError = cellHeight * detailSampleMaxError;

			// Set the area where the navigation will be build.
			// Here the bounds of the input mesh are used, but the
			// area could be specified by an user defined box, etc.
			rcCalcBounds(verts, nverts, cfg.bmin, cfg.bmax);
			rcCalcGridSize(cfg.bmin, cfg.bmax, cfg.cs, &cfg.width, &cfg.height);

			//
			// Step 2. Rasterize input polygon soup.
			//

			// Allocate voxel heightfield where we rasterize our input data to.
			rcHeightfield *solid = rcAllocHeightfield();
			if (!solid)
			{
				messages->Add("buildNavigation: Out of memory 'solid'.");
				return false;
			}
			if(!rcCreateHeightfield(mContext, *solid, cfg.width, cfg.height, cfg.bmin, cfg.bmax, cfg.cs, cfg.ch))
			{
				messages->Add("buildNavigation: Could not create solid heightfield.");
				return false;
			}

			// Allocate array that can hold triangle area types.
			// If you have multiple meshes you need to process, allocate
			// and array which can hold the max number of triangles you need to process.
			unsigned char *triareas = new unsigned char[ntris];

			// Find triangles which are walkable based on their slope and rasterize them.
			// If your input data is multiple meshes, you can transform them here, calculate
			// the are type for each of the meshes and rasterize them.
			memset(triareas, 0, ntris * sizeof(unsigned char));
			rcMarkWalkableTriangles(mContext, cfg.walkableSlopeAngle, verts, nverts, tris, ntris, triareas);
			rcRasterizeTriangles(mContext, verts, nverts, tris, triareas, ntris, *solid, cfg.walkableClimb);

			delete [] triareas;
			triareas = nullptr;

			//
			// Step 3. Filter walkables surfaces.
			//

			// Once all geoemtry is rasterized, we do initial pass of filtering to
			// remove unwanted overhangs caused by the conservative rasterization
			// as well as filter spans where the character cannot possibly stand.
			rcFilterLowHangingWalkableObstacles(mContext, cfg.walkableClimb, *solid);
			rcFilterLedgeSpans(mContext, cfg.walkableHeight, cfg.walkableClimb, *solid);
			rcFilterWalkableLowHeightSpans(mContext, cfg.walkableHeight, *solid);


			//
			// Step 4. Partition walkable surface to simple regions.
			//

			// Compact the heightfield so that it is faster to handle from now on.
			// This will result more cache coherent data as well as the neighbours
			// between walkable cells will be calculated.
			rcCompactHeightfield *chf = rcAllocCompactHeightfield();
			if (!chf)
			{
				messages->Add("buildNavigation: Out of memory 'chf'.");
				return false;
			}
			if (!rcBuildCompactHeightfield(mContext, cfg.walkableHeight, cfg.walkableClimb, *solid, *chf))
			{
				messages->Add("buildNavigation: Could not build compact data.");
				return false;
			}

			rcFreeHeightField(solid);
			solid = nullptr;

			// Erode the walkable area by agent radius.
			if (!rcErodeWalkableArea(mContext, cfg.walkableRadius, *chf))
			{
				messages->Add("buildNavigation: Could not erode.");
				return false;
			}

			// (Optional) Mark areas.
//			const ConvexVolume* vols = m_geom->getConvexVolumes();
//			for (int i  = 0; i < m_geom->getConvexVolumeCount(); ++i)
//				rcMarkConvexPolyArea(mContext, vols[i].verts, vols[i].nverts, vols[i].hmin, vols[i].hmax, (unsigned char)vols[i].area, *chf);

			if (monotonePartitioning)
			{
				// Partition the walkable surface into simple regions without holes.
				// Monotone partitioning does not need distancefield.
				if (!rcBuildRegionsMonotone(mContext, *chf, 0, cfg.minRegionArea, cfg.mergeRegionArea))
				{
					messages->Add("buildNavigation: Could not build regions.");
					return false;
				}
			}
			else
			{
				// Prepare for region partitioning, by calculating distance field along the walkable surface.
				if (!rcBuildDistanceField(mContext, *chf))
				{
					messages->Add("buildNavigation: Could not build distance field.");
					return false;
				}

				// Partition the walkable surface into simple regions without holes.
				if (!rcBuildRegions(mContext, *chf, 0, cfg.minRegionArea, cfg.mergeRegionArea))
				{
					messages->Add("buildNavigation: Could not build regions.");
					return false;
				}
			}

			//
			// Step 5. Trace and simplify region contours.
			//

			// Create contours.
			rcContourSet *cset = rcAllocContourSet();
			if (!cset)
			{
				messages->Add("buildNavigation: Out of memory 'cset'.");
				return false;
			}
			if (!rcBuildContours(mContext, *chf, cfg.maxSimplificationError, cfg.maxEdgeLen, *cset))
			{
				messages->Add("buildNavigation: Could not create contours.");
				return false;
			}

			//
			// Step 6. Build polygons mesh from contours.
			//

			// Build polygon navmesh from the contours.
			rcPolyMesh *pmesh = rcAllocPolyMesh();
			if (!pmesh)
			{
				messages->Add("buildNavigation: Out of memory 'pmesh'.");
				return false;
			}
			if (!rcBuildPolyMesh(mContext, *cset, cfg.maxVertsPerPoly, *pmesh))
			{
				messages->Add("buildNavigation: Could not triangulate contours.");
				return false;
			}

			//
			// Step 7. Create detail mesh which allows to access approximate height on each polygon.
			//

			rcPolyMeshDetail *dmesh = rcAllocPolyMeshDetail();
			if (!dmesh)
			{
				messages->Add("buildNavigation: Out of memory 'pmdtl'.");
				return false;
			}

			if (!rcBuildPolyMeshDetail(mContext, *pmesh, *chf, cfg.detailSampleDist, cfg.detailSampleMaxError, *dmesh))
			{
				messages->Add("buildNavigation: Could not build detail mesh.");
				return false;
			}

			rcFreeCompactHeightfield(chf);
			chf = nullptr;
			rcFreeContourSet(cset);
			cset = nullptr;

			// At this point the navigation mesh data is ready, you can access it from pmesh.
			// See duDebugDrawPolyMesh or dtCreateNavMeshData as examples how to access the data.

			//
			// (Optional) Step 8. Create Detour data from Recast poly mesh.
			//

			// The GUI may allow more max points per polygon than Detour can handle.
			// Only build the detour navmesh if we do not exceed the limit.
			if (cfg.maxVertsPerPoly <= DT_VERTS_PER_POLYGON)
			{
				unsigned char* navData = nullptr;
				int navDataSize = 0;

				// Update poly flags from areas.
				for (int i = 0; i < pmesh->npolys; ++i)
				{
					/*if (pmesh->areas[i] == RC_WALKABLE_AREA)
						pmesh->areas[i] = SAMPLE_POLYAREA_GROUND;

					if (pmesh->areas[i] == SAMPLE_POLYAREA_GROUND ||
						pmesh->areas[i] == SAMPLE_POLYAREA_GRASS ||
						pmesh->areas[i] == SAMPLE_POLYAREA_ROAD)
					{
						pmesh->flags[i] = SAMPLE_POLYFLAGS_WALK;
					}
					else if (pmesh->areas[i] == SAMPLE_POLYAREA_WATER)
					{
						pmesh->flags[i] = SAMPLE_POLYFLAGS_SWIM;
					}
					else if (pmesh->areas[i] == SAMPLE_POLYAREA_DOOR)
					{
						pmesh->flags[i] = SAMPLE_POLYFLAGS_WALK | SAMPLE_POLYFLAGS_DOOR;
					}*/

					if(pmesh->areas[i] == RC_WALKABLE_AREA)
					{
						pmesh->flags[i] = RC_WALKABLE_AREA;
					}
				}


				dtNavMeshCreateParams params;
				memset(&params, 0, sizeof(params));
				params.verts = pmesh->verts;
				params.vertCount = pmesh->nverts;
				params.polys = pmesh->polys;
				params.polyAreas = pmesh->areas;
				params.polyFlags = pmesh->flags;
				params.polyCount = pmesh->npolys;
				params.nvp = pmesh->nvp;
				params.detailMeshes = dmesh->meshes;
				params.detailVerts = dmesh->verts;
				params.detailVertsCount = dmesh->nverts;
				params.detailTris = dmesh->tris;
				params.detailTriCount = dmesh->ntris;
				params.offMeshConVerts = nullptr;
				params.offMeshConRad = nullptr;
				params.offMeshConDir = nullptr;
				params.offMeshConAreas = nullptr;
				params.offMeshConFlags = nullptr;
				params.offMeshConUserID = nullptr;
				params.offMeshConCount = 0;
				params.walkableHeight = agentHeight;
				params.walkableRadius = agentRadius;
				params.walkableClimb = agentMaxClimb;
				rcVcopy(params.bmin, pmesh->bmin);
				rcVcopy(params.bmax, pmesh->bmax);
				params.cs = cfg.cs;
				params.ch = cfg.ch;
				params.buildBvTree = true;

				if (!dtCreateNavMeshData(&params, &navData, &navDataSize))
				{
					messages->Add("Could not build Detour navmesh.");
					return false;
				}

				// 만들어진 navigation mesh를 저장하자
				/*if(m_saveNavigationMesh)
				{
					FILE *file = NULL;
					if(fopen_s(&file, "SoloMesh.navmesh", "wb") == 0)
					{
						fwrite(navData, sizeof(unsigned char), navDataSize, file);
						fclose(file);
					}
				}*/

				mNavMesh = dtAllocNavMesh();
				if (!mNavMesh)
				{
					dtFree(navData);
					messages->Add("Could not create Detour navmesh");
					return false;
				}

				dtStatus status;

				status = mNavMesh->init(navData, navDataSize, DT_TILE_FREE_DATA);
				if (dtStatusFailed(status))
				{
					dtFree(navData);
					messages->Add("Could not init Detour navmesh");
					return false;
				}

				status = mNavQuery->init(mNavMesh, 2048);
				if (dtStatusFailed(status))
				{
					messages->Add("Could not init Detour navmesh query");
					return false;
				}
			}

			delete [] verts;
			delete [] tris;
			rcFreePolyMesh(pmesh);
			rcFreePolyMeshDetail(dmesh);

			return true;
		}

		array<float> ^FindPath(array<float> ^startPos, array<float> ^endPos, [Out] List<String ^> ^%messages)
		{
			messages = gcnew List<String ^>();

			if(mNavMesh == nullptr)
			{
				messages->Add("Call CreateNavigationMesh first.");
				return nullptr;
			}

			float _spos[3], spos[3];
			float _epos[3], epos[3];
			for(int i = 0; i < 3; ++i)
			{
				_spos[i] = startPos[i];
				_epos[i] = endPos[i];
			}

			dtStatus status = DT_SUCCESS;
			dtPolyRef startRef, endRef;
			status = mNavQuery->findNearestPoly(_spos, mExtents, mFilter, &startRef, spos);
			if(!dtStatusSucceed(status))
			{
				messages->Add("startRef: " + startRef);
				return nullptr;
			}
			status = mNavQuery->findNearestPoly(_epos, mExtents, mFilter, &endRef, epos);
			if(!dtStatusSucceed(status))
			{
				messages->Add("endRef: " + endRef);
				return nullptr;
			}

			messages->Add("startRef: " + startRef);
			messages->Add("endRef: " + endRef);

			//if(startRef != 0 && endRef != 0)
			{
				int npolys;
				status = mNavQuery->findPath(startRef, endRef, spos, epos, mFilter, mPolys, &npolys, MAX_POLYS);
				if(!dtStatusSucceed(status))
				{
					messages->Add("findPath failed.");
					return nullptr;
				}
				if(npolys != 0)
				{
					float clampedEpos[3];
					dtVcopy(clampedEpos, epos);
					if(mPolys[npolys-1] != endRef)
					{
						status = mNavQuery->closestPointOnPoly(mPolys[npolys-1], epos, clampedEpos);
						if(!dtStatusSucceed(status))
						{
							messages->Add("closestPointOnPoly failed.");
							return nullptr;
						}
					}

					int straightPathCount;
					status = mNavQuery->findStraightPath(spos, clampedEpos, mPolys, npolys,
						mStraightPath, nullptr, nullptr, &straightPathCount, MAX_POLYS);
					if(!dtStatusSucceed(status))
					{
						messages->Add("findStraightPath failed.");
						return nullptr;
					}

					array<float> ^path = gcnew array<float>(straightPathCount * 3);
					for(int i = 0; i < straightPathCount * 3; ++i)
					{
						path[i] = mStraightPath[i];
					}

					messages->Add("success.");
					return path;
				}
			}

			messages->Add("unknown exception.");
			return nullptr;
		}

		bool IsOnNavMesh(array<float> ^pos)
		{
			float point[3] = { pos[0], pos[1], pos[2] };
			dtStatus status;
			dtPolyRef poly;
			float nearestPoint[3];
			status = mNavQuery->findNearestPoly(point, mExtents, mFilter, &poly, nearestPoint);
			if(!dtStatusSucceed(status))
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	};
}

#pragma once
#include <OgreString.h>
#include <OgreResourceGroupManager.h>
#include <OgreDataStream.h>
#include <OgreSceneNode.h>
#include "Misc.h"

class OgreHelper
{
public:
	// 리소스 파일 이름으로 리소스 전체 경로 얻기
	static Ogre::String GetFullResourcePath(Ogre::ResourceGroupManager* rgm, const Ogre::String& resourceName);
	// 바이트 버퍼로부터 DataStreamPtr 얻기
	static Ogre::DataStreamPtr BufferToDataStream(const Bytes& buffer);
	// 하위 씬노드에서 해당 이름의 노드 존재하는지 확인
	static bool IsChildExist(Ogre::SceneNode* node, const Ogre::String& name);
};

#pragma once

struct Bytes
{
	size_t length;
	char* buffer;
};

#include "SNFDocument.h"
#include <exception>
#include <Windows.h>

SNFDocument* gSNFDocument = NULL;

Object::Object(Ogre::Vector3 position)
{
	this->scale = 1.0f;
	this->rotation = Ogre::Quaternion::ZERO;
	this->position = position;
}
Object::Object(float scale, const Ogre::Quaternion& rotation, const Ogre::Vector3& position)
{
	this->scale = scale;
	this->rotation = rotation;
	this->position = position;
}

SNFDocument::SNFDocument(void)
{
	gSNFDocument = this;

	this->Initialize();
}

SNFDocument::~SNFDocument(void)
{
	this->Cleanup();
}

void SNFDocument::Initialize()
{
}

void SNFDocument::Cleanup()
{
	for(std::map<std::string, MeshInfo*>::iterator itr = dicMeshInfo.begin(); itr != dicMeshInfo.end(); ++itr)
	{
		delete itr->second;
	}
	dicMeshInfo.clear();
	for(std::map<std::string, Bytes>::iterator itr = dicMeshData.begin(); itr != dicMeshData.end(); ++itr)
	{
		delete [] itr->second.buffer;
	}
	dicMeshData.clear();
	dicMaterialText.clear();
	for(std::map<std::string, Bytes>::iterator itr = dicTextureData.begin(); itr != dicTextureData.end(); ++itr)
	{
		delete [] itr->second.buffer;
	}
	dicTextureData.clear();
	dicObject.clear();
}

MeshInfo* SNFDocument::GetMeshInfo(const std::string& meshName)
{
	return dicMeshInfo[meshName];
}

Bytes SNFDocument::GetMeshData(const std::string& meshName)
{
	return dicMeshData[meshName];
}

std::string SNFDocument::GetMaterialText(const std::string& materialName)
{
	return dicMaterialText[materialName];
}

Bytes SNFDocument::GetTextureData(const std::string& textureName)
{
	return dicTextureData[textureName];
}

void SNFDocument::New()
{
	this->Cleanup();
	this->Initialize();
}

bool SNFDocument::Open(const std::string& fileName)
{
	this->Initialize();

	// SNF 파일로부터 정보 추출
	FILE* file = NULL;
	if(fopen_s(&file, fileName.c_str(), "rb") != 0)
		return false;

	try
	{
		// 헤더 검사
		if (!ParseHeader(file))
			return false;

		// 메시 정보 파싱
		ParseMeshInfo(file);

		// 메시 데이터 파싱
		ParseMeshData(file);

		// 재질 텍스트 파싱
		ParseMaterialText(file);

		// 텍스처 데이터 파싱
		ParseTextureData(file);

		// 오브젝트 리스트 파싱
		ParseObjectList(file);
	}
	catch (const std::exception& e)
	{
		fclose(file);
		MessageBoxA(0, e.what(), "Exception", 0);

		return false;
	}

	fclose(file);

	// TODO: 배치 정보 SNF에 저장 / SNF에서 배치 정보 로드
	// TODO: 오브젝트가 땅에 발을 딛도록 구현
	// TODO: 충돌 처리 구현

	return true;
}

bool SNFDocument::ParseHeader(FILE* file)
{
	try
	{
		char buffer[6] = {0,0,0,0,0,0};
		fread_s(buffer, 6, sizeof(char), 5, file);	// <snf> 파싱
		if (strcmp(buffer, "<snf>") != 0)	// 헤더가 <snf> 인지 검사
			return false;
	}
	catch (const std::exception&)
	{
		throw;
	}

	return true;
}

void SNFDocument::ParseMeshInfo(FILE* file)
{
	try
	{
		int totalMeshInfoSize;
		fread_s(&totalMeshInfoSize, sizeof(int), sizeof(int), 1, file);	// 총 메시 정보 크기 파싱
		int currentParsedSize = 0;
		while (currentParsedSize < totalMeshInfoSize)
		{
			char arrName[256];
			memset(arrName, 0, sizeof(char)*256);
			int meshNameLength;
			fread_s(&meshNameLength, sizeof(int), sizeof(int), 1, file);	// 메시 이름 길이 파싱
			fread_s(arrName, 256, sizeof(char), meshNameLength, file);	// 메시 이름 파싱
			std::string meshName(arrName);

			dicMeshInfo.insert(std::make_pair(meshName, new MeshInfo));	// 메시 정보 배열에 새로운 메시 정보 추가
			MeshInfo* meshInfo = dicMeshInfo[meshName];

			int totalMaterialNameLength;
			fread_s(&totalMaterialNameLength, sizeof(int), sizeof(int), 1, file);	// 총 재질 이름 길이 파싱
			int currentParsedMaterialNameLength = 0;
			while (currentParsedMaterialNameLength < totalMaterialNameLength)
			{
				char arrMaterialName[256];
				memset(arrMaterialName, 0, sizeof(char)*256);
				int materialNameLength;
				fread_s(&materialNameLength, sizeof(int), sizeof(int), 1, file);	// 재질 이름 길이 파싱
				fread_s(arrMaterialName, 256, sizeof(char), materialNameLength, file);	// 재질 이름 파싱
				std::string materialName(arrMaterialName);

				meshInfo->lstMaterialName.push_back(materialName);	// 메시 정보에 재질 이름 추가

				currentParsedMaterialNameLength += materialNameLength;
			}

			int totalTextureNameLength;
			fread_s(&totalTextureNameLength, sizeof(int), sizeof(int), 1, file);	// 총 텍스처 이름 길이 파싱
			int currentParsedTextureNameLength = 0;
			while (currentParsedTextureNameLength < totalTextureNameLength)
			{
				char arrTextureName[256];
				memset(arrTextureName, 0, sizeof(char)*256);
				int textureNameLength;
				fread_s(&textureNameLength, sizeof(int), sizeof(int), 1, file);	// 텍스처 이름 길이 파싱
				fread_s(arrTextureName, 256, sizeof(char), textureNameLength, file);	// 텍스처 이름 파싱
				std::string textureName(arrTextureName);

				meshInfo->lstTextureName.push_back(textureName);	// 메시 정보에 텍스처 이름 추가

				currentParsedTextureNameLength += textureNameLength;
			}

			currentParsedSize += meshNameLength;
			currentParsedSize += totalMaterialNameLength;
			currentParsedSize += totalTextureNameLength;
		}
	}
	catch (const std::exception&)
	{
		throw;
	}
}

void SNFDocument::ParseMeshData(FILE* file)
{
	try
	{
		int totalMeshDataSize;
		fread_s(&totalMeshDataSize, sizeof(int), sizeof(int), 1, file);	// 총 메시 데이터 크기 파싱
		int currentParsedSize = 0;
		while (currentParsedSize < totalMeshDataSize)
		{
			char arrName[256];
			memset(arrName, 0, sizeof(char)*256);
			int meshDataNameLength;
			fread_s(&meshDataNameLength, sizeof(int), sizeof(int), 1, file);	// 메시 데이터 이름 길이 파싱
			fread_s(arrName, 256, sizeof(char), meshDataNameLength, file);	// 메시 데이터 이름 파싱
			std::string meshDataName(arrName);

			Bytes arrMeshData;
			int meshDataSize;
			fread_s(&meshDataSize, sizeof(int), sizeof(int), 1, file);	// 메시 데이터 크기 파싱
			arrMeshData.length = meshDataSize;
			arrMeshData.buffer = new char[meshDataSize];
			fread_s(arrMeshData.buffer, meshDataSize, sizeof(char), meshDataSize, file);	// 메시 데이터 파싱

			dicMeshData.insert(std::make_pair(meshDataName, arrMeshData));	// dicMeshData에 메시 데이터 저장

			currentParsedSize += meshDataNameLength;
			currentParsedSize += meshDataSize;
		}
	}
	catch (const std::exception&)
	{
		throw;
	}
}

void SNFDocument::ParseMaterialText(FILE* file)
{
	try
	{
		int totalMaterialTextLength;
		fread_s(&totalMaterialTextLength, sizeof(int), sizeof(int), 1, file);	// 총 재질 텍스트 길이 파싱
		int currentParsedLength = 0;
		while (currentParsedLength < totalMaterialTextLength)
		{
			char arrName[256];
			memset(arrName, 0, sizeof(char)*256);
			int materialTextNameLength;
			fread_s(&materialTextNameLength, sizeof(int), sizeof(int), 1, file);	// 재질 텍스트 이름 길이 파싱
			fread_s(arrName, 256, sizeof(char), materialTextNameLength, file);	// 재질 텍스트 이름 파싱
			std::string materialTextName(arrName);

			char* arrMaterialText = NULL;
			int materialTextLength;
			fread_s(&materialTextLength, sizeof(int), sizeof(int), 1, file);	// 재질 텍스트 길이 파싱
			arrMaterialText = new char[materialTextLength+1];
			arrMaterialText[materialTextLength] = '\0';
			fread_s(arrMaterialText, materialTextLength+1, sizeof(char), materialTextLength, file);	// 재질 텍스트 파싱

			dicMaterialText.insert(std::make_pair(materialTextName, arrMaterialText));    // dicMaterialText에 텍스처 데이터 저장
			delete [] arrMaterialText;

			currentParsedLength += materialTextNameLength;
			currentParsedLength += materialTextLength;
		}
	}
	catch (const std::exception&)
	{
		throw;
	}
}

void SNFDocument::ParseTextureData(FILE* file)
{
	try
	{
		int totalTextureDataSize;
		fread_s(&totalTextureDataSize, sizeof(int), sizeof(int), 1, file);	// 총 텍스처 데이터 크기 파싱
		int currentParsedSize = 0;
		while (currentParsedSize < totalTextureDataSize)
		{
			char arrName[256];
			memset(arrName, 0, sizeof(char)*256);
			int textureDataNameLength;
			fread_s(&textureDataNameLength, sizeof(int), sizeof(int), 1, file);	// 텍스처 데이터 이름 길이 파싱
			fread_s(arrName, 256, sizeof(char), textureDataNameLength, file);	// 텍스처 데이터 이름 파싱
			std::string textureDataName(arrName);

			Bytes arrTextureData;
			int textureDataSize;
			fread_s(&textureDataSize, sizeof(int), sizeof(int), 1, file);	// 텍스처 데이터 크기 파싱
			arrTextureData.length = textureDataSize;
			arrTextureData.buffer = new char[textureDataSize];
			fread_s(arrTextureData.buffer, textureDataSize, sizeof(char), textureDataSize, file);	// 텍스처 데이터 파싱

			dicTextureData.insert(std::make_pair(textureDataName, arrTextureData));	// dicTextureData에 텍스처 데이터 저장

			currentParsedSize += textureDataNameLength;
			currentParsedSize += textureDataSize;
		}
	}
	catch (const std::exception&)
	{
		throw;
	}
}

void SNFDocument::ParseObjectList(FILE* file)
{
	// TODO: 오브젝트 리스트 파싱
	try
	{
		int totalObjectListSize;
		fread_s(&totalObjectListSize, sizeof(int), sizeof(int), 1, file);	// 총 오브젝트 리스트 크기 파싱
		int currentParsedSize = 0;
		while (currentParsedSize < totalObjectListSize)
		{
			int objectListSize;
			fread_s(&objectListSize, sizeof(int), sizeof(int), 1, file);	// 오브젝트 리스트 크기 파싱
			int currentParsedObjectListSize = 0;

			char arrMeshName[256];
			memset(arrMeshName, 0, sizeof(char)*256);
			int meshNameLength;
			fread_s(&meshNameLength, sizeof(int), sizeof(int), 1, file);	// 메시 이름 길이 파싱
			fread_s(arrMeshName, 256, sizeof(char), meshNameLength, file);	// 메시 이름 파싱
			std::string strMesh(arrMeshName);

			dicObject.insert(std::make_pair(strMesh, std::map<std::string, Object>()));

			currentParsedObjectListSize += meshNameLength;

			while (currentParsedObjectListSize < objectListSize)
			{
				char arrObjectName[256];
				memset(arrObjectName, 0, sizeof(char)*256);
				int objectNameLength;
				fread_s(&objectNameLength, sizeof(int), sizeof(int), 1, file);	// 오브젝트 이름 길이 파싱
				fread_s(arrObjectName, 256, sizeof(char), objectNameLength, file);	// 오브젝트 이름 파싱
				std::string strObject(arrObjectName);

				float fScale;
				fread_s(&fScale, sizeof(float), sizeof(float), 1, file);	// 오브젝트 스케일 비율 파싱

				Ogre::Quaternion qRotation;
				fread_s(&qRotation, sizeof(qRotation), sizeof(qRotation), 1, file);	// 오브젝트 회전 각도 파싱

				Ogre::Vector3 position;
				fread_s(&position, sizeof(position), sizeof(position), 1, file);	// 오브젝트 위치 파싱

				dicObject[strMesh].insert(std::make_pair(strObject, Object(fScale, qRotation, position)));

				currentParsedObjectListSize += objectNameLength;
				currentParsedObjectListSize += sizeof(float);
				currentParsedObjectListSize += sizeof(qRotation);
				currentParsedObjectListSize += sizeof(position);
			}

			currentParsedSize += currentParsedObjectListSize;
		}
	}
	catch (const std::exception&)
	{
		throw;
	}
}

//void SNFDocument::AddAllObjectToScene(OgreWindow ogreWindow)
//{
//	foreach(KeyValuePair<string, Dictionary<string, Object>> objectList in dicObject)
//	{
//		foreach (KeyValuePair<string, Object> o in objectList.Value)
//		{
//			SceneNode node = ogreWindow.CreateSceneNode(o.Key);
//			ogreWindow.AddObjectFromMeshName(node, objectList.Key);
//
//			// 배치 정보 적용
//			node.SetScale(o.Value.fScale, o.Value.fScale, o.Value.fScale);
//			node.SetOrientation(o.Value.qRotation.w, o.Value.qRotation.x, o.Value.qRotation.y, o.Value.qRotation.z);
//			node.SetPosition(o.Value.ptPosition.x, o.Value.ptPosition.y, o.Value.ptPosition.z);
//		}
//	}
//}

#pragma once

#include <vector>
#include <map>
#include <string>
#include <OgreQuaternion.h>
#include <OgreVector3.h>
#include "Misc.h"

struct MeshInfo
{
	std::vector<std::string> lstMaterialName;
	std::vector<std::string> lstTextureName;
};

struct Object
{
	float scale;
	Ogre::Quaternion rotation;
	Ogre::Vector3 position;

	Object(Ogre::Vector3 position);
	Object(float scale, const Ogre::Quaternion& rotation, const Ogre::Vector3& position);
};

class SNFDocument
{
private:
	std::map<std::string, MeshInfo*> dicMeshInfo;    // 키: 메시 이름, 값: MeshInfo
	std::map<std::string, Bytes> dicMeshData;  // 키: 메시 이름, 값: 메시 데이터
	std::map<std::string, std::string> dicMaterialText;  // 키: 재질 이름, 값: 재질 텍스트
	std::map<std::string, Bytes> dicTextureData;   // 키: 텍스처 이름, 값: 텍스처 데이터
	std::map<std::string, std::map<std::string, Object>> dicObject;   // 키: 메시 이름, 값: 해당 메시 이름으로 만들어진 오브젝트 리스트

public:
    /// <summary>
    /// 생성자(새로 만들기 용도)
    /// </summary>
	SNFDocument(void);
	~SNFDocument(void);

	void Initialize();
	void Cleanup();
	MeshInfo* GetMeshInfo(const std::string& meshName);
	Bytes GetMeshData(const std::string& meshName);
	std::string GetMaterialText(const std::string& materialName);
	Bytes GetTextureData(const std::string& textureName);
	void New();
	bool Open(const std::string& fileName);
	bool ParseHeader(FILE* file);
	void ParseMeshInfo(FILE* file);
	void ParseMeshData(FILE* file);
	void ParseMaterialText(FILE* file);
	void ParseTextureData(FILE* file);
	void ParseObjectList(FILE* file);
//	void AddAllObjectToScene(OgreWindow ogreWindow);
};

extern SNFDocument* gSNFDocument;

#include "OgreHelper.h"

// 리소스 파일 이름으로 리소스 전체 경로 얻기
Ogre::String OgreHelper::GetFullResourcePath(Ogre::ResourceGroupManager* rgm, const Ogre::String& resourceName)
{
	if (rgm == NULL)
		return "";

	// 리소스 파일이 포함된 위치 탐색
	Ogre::StringVector groupNames = rgm->getResourceGroups();
	for(Ogre::StringVector::iterator itrGroupName = groupNames.begin(); itrGroupName != groupNames.end(); ++itrGroupName)
	{
		Ogre::FileInfoListPtr fileInfoList = rgm->findResourceFileInfo(*itrGroupName, resourceName, false);
		// 리소스 파일 경로 얻음
		for(Ogre::FileInfoList::iterator itrFileInfo = fileInfoList->begin(); itrFileInfo != fileInfoList->end(); ++itrFileInfo)
		{
			// 최종 리소스 파일 경로 계산
			return itrFileInfo->archive->getName() + '/' + resourceName;
		}
	}

	return "";
}

// 바이트 버퍼로부터 DataStreamPtr 얻기
Ogre::DataStreamPtr OgreHelper::BufferToDataStream(const Bytes& buffer)
{
	void* pBuffer = &buffer.buffer[0];
	Ogre::MemoryDataStreamPtr rawStream(new Ogre::MemoryDataStream(pBuffer, buffer.length));
	if (rawStream.isNull())
	{
		Ogre::DataStreamPtr nullPtr;
		nullPtr.setNull();
		return nullPtr;
	}

	return Ogre::DataStreamPtr(rawStream);
}

// 하위 씬노드에서 해당 이름의 노드 존재하는지 확인
bool OgreHelper::IsChildExist(Ogre::SceneNode* node, const Ogre::String& name)
{
	Ogre::SceneNode::ChildNodeIterator itr = node->getChildIterator();

	while (itr.hasMoreElements())
	{
		if (itr.current()->second->getName() == name)
			return true;

		itr.moveNext();
	}

	return false;
}
